/**
 * Created by belawal on 9/23/20.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{

    var sale_id = request.session.sale_id;
    request.session.sale_id = null;

    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT booking.*,projects.project_name,parties.NAME as customer_name,plotsizes.size_name FROM booking LEFT JOIN projects on projects.project_id=booking.project_id LEFT JOIN plotsizes on plotsizes.size_id=booking.size_id LEFT JOIN parties on parties.PARTY_ID=booking.customer_id and booking.company_id=${company_id} ORDER BY booking.booking_id DESC`,(err,rows)=>{
        if(err) response.send(err);

        response.render('./booking/list.twig',{bookings:rows,'messages': request.flash('user')});
    });



});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y' and type='Builder'`;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND status='Y' and registration_type='Builder'`;
    var plotsizes = `select * from plotsizes where company_id=${company_id} and active_flag='Y'`;
    var plotfeatures = `select * from plotfeatures where company_id=${company_id} and active_flag='Y'`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotsizes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotsizes = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotfeatures, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotfeatres = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);
//        res.send(return_data.projects);
        res.render('./booking/add.twig',{'projects':return_data.projects,'customers':return_data.customers , 'plotsizes':return_data.plotsizes, 'plotfeatures':return_data.plotfeatres,company_id:company_id,banks:return_data.banks});
    });
});


router.get('/plot_by_size/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from plot_master where COMPANY_ID=${company_id} AND size_id=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});
router.get('/plot_detail/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from plot_master where  plot_id=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

router.get('/plot_features/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT plot_child.*,plotfeatures.description FROM plot_child LEFT JOIN plotfeatures ON plotfeatures.feature_id=plot_child.feature_id WHERE plot_child.plot_id=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

router.get('/get_schedule/:project_id?/:type?/:size_id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from payment_schedules where project_id=${request.params.project_id} and plot_type="${request.params.type}" and size_id=${request.params.size_id} and company_id=${company_id}`,(err,rows)=>{
        if(!err){
            if(rows.length>0){
                var master_id=rows[0].schedule_id
                mysqlconnection.query(`select payment_schedules_child.*,payment_modes.name as mode_name from payment_schedules_child LEFT JOIN payment_modes on payment_modes.mode_id=payment_schedules_child.mode_id where payment_schedules_child.payment_schedule_master_id=${master_id}`,(err,rows)=>{
                    response.json(rows);
                });
            }
            else{
                response.json('NO')
            }

        }
        else{
            response.send(err);
        }
    });
});

router.get('/get_total/:project_id?/:type?/:size_id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from payment_schedules where project_id=${request.params.project_id} and plot_type="${request.params.type}" and size_id=${request.params.size_id} and company_id=${company_id}`,(err,rows)=>{
        if(!err){
            if(rows.length>0){
                var total=rows[0].grand_total;
                response.json(total);
            }
            else{
                response.json('NO')
            }

        }
        else{
            response.send(err);
        }
    });
});


router.post('/store',(request,response)=>{
    company_id=request.session.COMPANY_ID;
    customer_type=request.body.customer_type;
    customer_id="";
    if(customer_type=='new'){
        var data={
            NAME:request.body.name,
            father_name:request.body.father_name,
            Email:request.body.email,
            occupation:request.body.occupation,
            age:request.body.age,
            nationality:request.body.nationality,
            cnic:request.body.cnic,
            TYPE:'C',
            CONTACT_NO:request.body.contact_number,
            phone_number:request.body.phone_number,
            residence_number:request.body.residence_number,
            ADDRESS:request.body.address,
            permanent_address:request.body.permanent_address,
            CITY:request.body.city,
            city_id:request.body.city_id,
            DESCRIPTION:request.body.description,
            name_of_nominee:request.body.name_of_nominee,
            address_of_nominee:request.body.address_of_nominee,
            relation:request.body.relation,
            OPENING_BALANCE:request.body.opening_balance,
            COMPANY_ID:request.session.COMPANY_ID
        }
        mysqlconnection.query('insert into parties set ?' , data,(err,rows)=>{
            if(err) console.log(err);
                customer_id=rows.insertId;


            var data={
                customer_id:customer_id,
                booking_number:request.body.booking_number,
                project_id:request.body.project_id,
                plot_type:request.body.plot_type,
                size_id:request.body.size_id,
                plot_id:request.body.plot_id,
                total:request.body.grand_total,
                no_of_instalments:request.body.no_of_instalments,
                plan_date:request.body.plan_date,
                gap:request.body.gap,
                advance:request.body.advance,
                discount:request.body.discount,
                remaining:request.body.remaining,
                feature_amount:request.body.feature_amount,
                bank_id:request.body.account_id,
                cheque_number:request.body.cheque_number,
                company_id:request.session.COMPANY_ID
            }

            mysqlconnection.query('insert into booking set ?',data,(err,rows)=>{
                if(!err){
                    var booking_id=rows.insertId;

                    var once_mode_name=request.body.once_mode_name;
                    var once_mode_id=request.body.once_mode_id;
                    var once_amount=request.body.once_amount;
                    var once_total=request.body.once_total;
                    var once_type=request.body.once_type;

                    for(var i=0;i<once_mode_id.length;i++){
                        var data={
                            booking_id:booking_id,
                            mode_id:once_mode_id[i],
                            amount:once_amount[i],
                            total:once_total[i],
                            type:once_type[i],
                            payment_status:'N'
                        }
                        mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                        });
                    }


                    var monthly_instalment_no=request.body.monthly_instalment_no;
                    var monthly_mode_id=request.body.monthly_mode_id;
                    var monthly_amount=request.body.monthly_amount;
                    var monthly_total=request.body.monthly_total;
                    var monthly_type=request.body.monthly_type;
                    var monthly_date=request.body.monthly_date;
                    for(var i=0;i<monthly_mode_id.length;i++){
                        var data={
                            booking_id:booking_id,
                            mode_id:monthly_mode_id[i],
                            amount:monthly_amount[i],
                            total:monthly_total[i],
                            type:monthly_type[i],
                            instalment_number:monthly_instalment_no[i],
                            date:monthly_date[i],
                            payment_status:'N'
                        }
                        mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                        });
                    }

                    var yearly_instalment_no=request.body.yearly_instalment_no;
                    var yearly_mode_id=request.body.yearly_mode_id;
                    var yearly_amount=request.body.yearly_amount;
                    var yearly_total=request.body.yearly_total;
                    var yearly_type=request.body.yearly_type;
                    var yearly_date=request.body.yearly_date;
                    for(var i=0;i<yearly_mode_id.length;i++){
                        var data={
                            booking_id:booking_id,
                            mode_id:yearly_mode_id[i],
                            amount:yearly_amount[i],
                            total:yearly_total[i],
                            type:yearly_type[i],
                            instalment_number:yearly_instalment_no[i],
                            date:yearly_date[i],
                            payment_status:'N'
                        }
                        mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                        });
                    }


                    var data={
                        voucher_no:request.body.booking_number,
                        voucher_date:request.body.plan_date,
                        type:"BOOKING",
                        party_id:customer_id,
                        credit:0,
                        COMPANY_ID:company_id,
                        debit:request.body.grand_total,
                        remarks:'BOOKING ENTRY'
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });


                    if(request.body.advance==0){

                    }else{
                        var data={
                            voucher_no:request.body.booking_number,
                            voucher_date:request.body.plan_date,
                            type:"BOOKING",
                            party_id:customer_id,
                            credit:request.body.advance,
                            COMPANY_ID:company_id,
                            debit:0,
                            remarks:'BOOKING ADVANCE ENTRY'
                        }
                        mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });
                    }


                    if(request.body.payment_type=='cash'){
                        var data={
                            voucher_no:request.body.booking_number,
                            voucher_date:request.body.plan_date,
                            type:"BOOKING",
                            credit:0,
                            COMPANY_ID:company_id,
                            debit:request.body.advance,
                            remarks:'Booking Advance Entry'
                        }
                        mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });

                    }


                    if(request.body.payment_type=='bank'){
                        var data={
                            voucher_no:request.body.booking_number,
                            voucher_date:request.body.plan_date,
                            deposit:request.body.advance,
                            withdraw:0,
                            COMPANY_ID:company_id,
                            account_id:request.body.account_id,
                            remarks:'Booking Advance Entry'
                        }

                        mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });

                    }




                }
            });
        });
    }
    else{
        customer_id=request.body.customer_id;

        var data={
            customer_id:request.body.customer_id,
            booking_number:request.body.booking_number,
            project_id:request.body.project_id,
            plot_type:request.body.plot_type,
            size_id:request.body.size_id,
            plot_id:request.body.plot_id,
            total:request.body.grand_total,
            no_of_instalments:request.body.no_of_instalments,
            plan_date:request.body.plan_date,
            gap:request.body.gap,
            advance:request.body.advance,
            discount:request.body.discount,
            remaining:request.body.remaining,
            feature_amount:request.body.feature_amount,
            bank_id:request.body.account_id,
            cheque_number:request.body.cheque_number,
            company_id:request.session.COMPANY_ID
        }

        mysqlconnection.query('insert into booking set ?',data,(err,rows)=>{
            if(!err){
                var booking_id=rows.insertId;

                var once_mode_name=request.body.once_mode_name;
                var once_mode_id=request.body.once_mode_id;
                var once_amount=request.body.once_amount;
                var once_total=request.body.once_total;
                var once_type=request.body.once_type;

                for(var i=0;i<once_mode_id.length;i++){
                    var data={
                        booking_id:booking_id,
                        mode_id:once_mode_id[i],
                        amount:once_amount[i],
                        total:once_total[i],
                        type:once_type[i],
                        payment_status:'N'
                    }
                    mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                    });
                }


                var monthly_instalment_no=request.body.monthly_instalment_no;
                var monthly_mode_id=request.body.monthly_mode_id;
                var monthly_amount=request.body.monthly_amount;
                var monthly_total=request.body.monthly_total;
                var monthly_type=request.body.monthly_type;
                var monthly_date=request.body.monthly_date;
                for(var i=0;i<monthly_mode_id.length;i++){
                    var data={
                        booking_id:booking_id,
                        mode_id:monthly_mode_id[i],
                        amount:monthly_amount[i],
                        total:monthly_total[i],
                        type:monthly_type[i],
                        instalment_number:monthly_instalment_no[i],
                        date:monthly_date[i],
                        payment_status:'N'
                    }
                    mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                    });
                }

                var yearly_instalment_no=request.body.yearly_instalment_no;
                var yearly_mode_id=request.body.yearly_mode_id;
                var yearly_amount=request.body.yearly_amount;
                var yearly_total=request.body.yearly_total;
                var yearly_type=request.body.yearly_type;
                var yearly_date=request.body.yearly_date;
                for(var i=0;i<yearly_mode_id.length;i++){
                    var data={
                        booking_id:booking_id,
                        mode_id:yearly_mode_id[i],
                        amount:yearly_amount[i],
                        total:yearly_total[i],
                        type:yearly_type[i],
                        instalment_number:yearly_instalment_no[i],
                        date:yearly_date[i],
                        payment_status:'N'
                    }
                    mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                    });
                }


                var data={
                    voucher_no:request.body.booking_number,
                    voucher_date:request.body.plan_date,
                    type:"BOOKING",
                    party_id:customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.grand_total,
                    remarks:'BOOKING ENTRY'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });


                if(request.body.advance==0){

                }else{
                    var data={
                        voucher_no:request.body.booking_number,
                        voucher_date:request.body.plan_date,
                        type:"BOOKING",
                        party_id:customer_id,
                        credit:request.body.advance,
                        COMPANY_ID:company_id,
                        debit:0,
                        remarks:'BOOKING ADVANCE ENTRY'
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                }


                if(request.body.payment_type=='cash'){
                    var data={
                        voucher_no:request.body.booking_number,
                        voucher_date:request.body.plan_date,
                        type:"BOOKING",
                        credit:0,
                        COMPANY_ID:company_id,
                        debit:request.body.advance,
                        remarks:'Booking Advance Entry'
                    }
                    mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });

                }


                if(request.body.payment_type=='bank'){
                    var data={
                        voucher_no:request.body.booking_number,
                        voucher_date:request.body.plan_date,
                        deposit:request.body.advance,
                        withdraw:0,
                        COMPANY_ID:company_id,
                        account_id:request.body.account_id,
                        remarks:'Booking Advance Entry'
                    }

                    mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });

                }




            }
        });
    }


    request.flash('user', 'Data Successfully Inserted');
    response.redirect('/booking/list')






});


router.post('/store2',(request,response)=>{

    customer_type=request.body.customer_type;
    customer_id="";
    if(customer_type=='new'){
        var data={
            NAME:request.body.name,
            father_name:request.body.father_name,
            Email:request.body.email,
            occupation:request.body.occupation,
            age:request.body.age,
            nationality:request.body.nationality,
            cnic:request.body.cnic,
            TYPE:'C',
            CONTACT_NO:request.body.contact_number,
            phone_number:request.body.phone_number,
            residence_number:request.body.residence_number,
            ADDRESS:request.body.address,
            permanent_address:request.body.permanent_address,
            CITY:request.body.city,
            city_id:request.body.city_id,
            DESCRIPTION:request.body.description,
            name_of_nominee:request.body.name_of_nominee,
            address_of_nominee:request.body.address_of_nominee,
            relation:request.body.relation,
            OPENING_BALANCE:request.body.opening_balance,
            COMPANY_ID:request.session.COMPANY_ID
        }
        mysqlconnection.query('insert into parties set ?' , data,(err,rows)=>{
            if(err) console.log(err);
            if(!err){


                customer_id=rows.insertId;
                console.log("I am in new customer The customer_id is" + customer_id);

            }

        });
    }
    else{
        customer_id=request.body.customer_id;
        console.log("I am in old customer The customer_id is" + customer_id);

    }




    console.log("At Last The customer_id is" + customer_id);

});


router.get('/print/:id?',(request,response)=>{

    mysqlconnection.query(`SELECT booking.*,parties.*,projects.project_logo,projects.project_name,plotsizes.size_name,accounts.NAME as bank_name, plot_master.name as plot_number,plot_master.street_no,plot_master.block,plot_master.length_width FROM booking LEFT JOIN projects on projects.project_id=booking.project_id LEFT JOIN parties ON parties.PARTY_ID=booking.customer_id LEFT JOIN plot_master on plot_master.plot_id=booking.plot_id LEFT JOIN plotsizes on plotsizes.size_id=booking.size_id LEFT JOIN accounts on accounts.ACCOUNT_ID=booking.bank_id WHERE booking.booking_id=${request.params.id}`,(err,rows)=>{
        if(err) response.send(err);
//          response.send(rows);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//        response.send(rows);
        response.render('./booking/print.twig',{bookings:rows,session:session_values});
    });




});


router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    var company_id=req.session.COMPANY_ID;
    var bookingmaster = `select * from booking where company_id=${company_id}  and booking_id=${id}`;
    var bookingchild = `select booking_detail.*,payment_modes.name as mode_name from booking_detail LEFT JOIN payment_modes on payment_modes.mode_id=booking_detail.mode_id  where  booking_detail.booking_id=${id} Order By booking_detail.date ASC`;
    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y'`;
    var plotsizes = `select * from plotsizes where company_id=${company_id} and active_flag='Y'`;
    var plotfeatures = `select * from plotfeatures where company_id=${company_id} and active_flag='Y'`;
    var plots = `select * from plot_master where COMPANY_ID=${company_id}`;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND status='Y'`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(bookingmaster, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.bookingmaster = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(bookingchild, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.bookingchild = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotsizes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotsizes = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotfeatures, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotfeatres = results;
                parallel_done();
            });
        },    function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        }, function(parallel_done) {
            mysqlconnection.query(plots, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plots = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
//        res.send(return_data.bookingchild);
        res.render('./booking/edit.twig',{'projects':return_data.projects,'customers':return_data.customers , 'plotsizes':return_data.plotsizes, 'plotfeatures':return_data.plotfeatres,company_id:company_id,banks:return_data.banks,masters:return_data.bookingmaster,childs:return_data.bookingchild,plots:return_data.plots});
    });


});


router.post('/update',(request,response)=>{

    var booking_id=request.body.booking_id;
    var company_id=request.session.COMPANY_ID;
    customer_id=request.body.customer_id;

//    console.log('The type is is ' + request.body.payment_type);
    var data={
        customer_id:request.body.customer_id,
        booking_number:request.body.booking_number,
        project_id:request.body.project_id,
        plot_type:request.body.plot_type,
        size_id:request.body.size_id,
        plot_id:request.body.plot_id,
        total:request.body.grand_total,
        no_of_instalments:request.body.no_of_instalments,
        plan_date:request.body.plan_date,
        gap:request.body.gap,
        advance:request.body.advance,
        discount:request.body.discount,
        remaining:request.body.remaining,
        feature_amount:request.body.feature_amount,
        bank_id:request.body.account_id,
        cheque_number:request.body.cheque_number,
        company_id:request.session.COMPANY_ID
    }
    mysqlconnection.query(`update booking  set ? where booking_id=${booking_id}`,data,(err,rows)=>{

        var plot_master_id=booking_id;
        mysqlconnection.query(`delete from booking_detail where  booking_id=${booking_id}`,()=>{

            var once_mode_name=request.body.once_mode_name;
            var once_mode_id=request.body.once_mode_id;
            var once_amount=request.body.once_amount;
            var once_total=request.body.once_total;
            var once_type=request.body.once_type;

            for(var i=0;i<once_mode_id.length;i++){
                var data={
                    booking_id:booking_id,
                    mode_id:once_mode_id[i],
                    amount:once_amount[i],
                    total:once_total[i],
                    type:once_type[i],
                    payment_status:'N'
                }
                mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                });
            }


            var monthly_instalment_no=request.body.monthly_instalment_no;
            var monthly_mode_id=request.body.monthly_mode_id;
            var monthly_amount=request.body.monthly_amount;
            var monthly_total=request.body.monthly_total;
            var monthly_type=request.body.monthly_type;
            var monthly_date=request.body.monthly_date;
            for(var i=0;i<monthly_mode_id.length;i++){
                var data={
                    booking_id:booking_id,
                    mode_id:monthly_mode_id[i],
                    amount:monthly_amount[i],
                    total:monthly_total[i],
                    type:monthly_type[i],
                    instalment_number:monthly_instalment_no[i],
                    date:monthly_date[i],
                    payment_status:'N'
                }
                mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                });
            }

            var yearly_instalment_no=request.body.yearly_instalment_no;
            var yearly_mode_id=request.body.yearly_mode_id;
            var yearly_amount=request.body.yearly_amount;
            var yearly_total=request.body.yearly_total;
            var yearly_type=request.body.yearly_type;
            var yearly_date=request.body.yearly_date;
            for(var i=0;i<yearly_mode_id.length;i++){
                var data={
                    booking_id:booking_id,
                    mode_id:yearly_mode_id[i],
                    amount:yearly_amount[i],
                    total:yearly_total[i],
                    type:yearly_type[i],
                    instalment_number:yearly_instalment_no[i],
                    date:yearly_date[i],
                    payment_status:'N'
                }
                mysqlconnection.query('insert into booking_detail set ?',data,(err,rows)=>{

                });
            }

        });

        var voucher_no=request.body.booking_number;
        mysqlconnection.query(`delete from party_ledger where  voucher_no='${voucher_no}' AND type='BOOKING'`,(err)=>{
            console.log(err);
            var data={
                voucher_no:request.body.booking_number,
                voucher_date:request.body.plan_date,
                type:"BOOKING",
                party_id:customer_id,
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.grand_total,
                remarks:'BOOKING ENTRY'
            }
            mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });


            if(request.body.advance==0){

            }else{
                var data={
                    voucher_no:request.body.booking_number,
                    voucher_date:request.body.plan_date,
                    type:"BOOKING",
                    party_id:customer_id,
                    credit:request.body.advance,
                    COMPANY_ID:company_id,
                    debit:0,
                    remarks:'BOOKING ADVANCE ENTRY'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }
        });

        mysqlconnection.query(`delete from cash_ledger where  voucher_no='${voucher_no}' AND type='BOOKING'`,(err)=>{

            if(request.body.payment_type=='cash'){
                var data={
                    voucher_no:request.body.booking_number,
                    voucher_date:request.body.plan_date,
                    type:"BOOKING",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.advance,
                    remarks:'Booking Advance Entry'
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

            }

        });

        mysqlconnection.query(`delete from bank_ledger where  voucher_no='${voucher_no}' AND COMPANY_ID='${company_id}'`,(err)=>{
            if(request.body.payment_type=='bank'){
                var data={
                    voucher_no:request.body.booking_number,
                    voucher_date:request.body.plan_date,
                    deposit:request.body.advance,
                    withdraw:0,
                    COMPANY_ID:company_id,
                    account_id:request.body.account_id,
                    remarks:'Booking Advance Entry'
                }

                mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

            }
        });


        request.flash('user', 'Data Successfully Updated');
        response.redirect('/booking/list')

    });
});


module.exports=router;
