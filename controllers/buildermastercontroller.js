/**
 * Created by belawal on 8/27/20.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT plot_master.*,projects.project_name,floor.floor_name,plotsizes.size_name FROM plot_master LEFT JOIN projects on projects.project_id=plot_master.project_id LEFT JOIN floor on floor.floor_id=plot_master.floor_id LEFT JOIN plotsizes on plotsizes.size_id=plot_master.size_id WHERE plot_master.company_id=${company_id} and plot_master.status='B' and plot_master.active_flag='Y' ORDER BY plot_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./buildermaster/list.twig',{'messages': request.flash('user'),buildermasters:rows});
    });




});


router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y' AND type='Builder'`;
    var plotsizes = `select * from plotsizes where company_id=${company_id} and active_flag='Y'`;
    var floors = `select * from floor where COMPANY_ID=${company_id}`;
    var plotfeatures = `select * from plotfeatures where company_id=${company_id} and active_flag='Y'`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotsizes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotsizes = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(floors, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.floors = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotfeatures, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotfeatres = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);
//        res.send(return_data.floors);
        res.render('./buildermaster/add.twig',{'projects':return_data.projects , 'plotsizes':return_data.plotsizes, 'plotfeatures':return_data.plotfeatres,company_id:company_id,'floors':return_data.floors});
    });
});


router.post('/store',(request,response,cb)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        project_id:request.body.project_id,
        name:request.body.name,
        floor_id:request.body.floor_id,
        street_no:request.body.street_no,
        block:request.body.block,
        type:request.body.type,
        status:'B',
        flat_type:request.body.flat_type,
        number_of_bedrooms:request.body.number_of_bedrooms,
        squarefeets:request.body.squarefeets,
        rate:request.body.rate,
        company_id:company_id
    }
    mysqlconnection.query('insert into plot_master set ?',data,(err,rows)=>{
        if(!err){
            var plot_master_id=rows.insertId;
            var feature_id=request.body.feature_id;
            var percentage=request.body.percentage;

            for(var i=0;i<feature_id.length;i++){

                var data={
                    plot_id:plot_master_id,
                    feature_id:feature_id[i],
                    percentage:percentage[i]
                }
                mysqlconnection.query('insert into plot_child set ?',data,(err,rows)=>{

                });

            }

            request.flash('user', 'Data Successfully Inserted');
            response.redirect('/buildermaster/list')
        }
        else{
            response.send(err)
        }
    });
});


router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    var company_id=req.session.COMPANY_ID;
    var plotmaster = `select * from plot_master where company_id=${company_id} and active_flag='Y' and status='B' and plot_id=${id}`;
    var plotchild = `select * from plot_child where  plot_id=${id}`;
    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y' AND type='Builder'`;
    var plotsizes = `select * from plotsizes where company_id=${company_id} and active_flag='Y'`;
    var plotfeatures = `select * from plotfeatures where company_id=${company_id} and active_flag='Y'`;
    var floors = `select * from floor where COMPANY_ID=${company_id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotmaster, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotmasters = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotchild, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotchild = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(floors, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.floors = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotsizes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotsizes = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotfeatures, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotfeatres = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);
//        res.send(return_data.plotchild);
        res.render('./buildermaster/edit.twig',{'projects':return_data.projects , 'plotsizes':return_data.plotsizes, 'plotfeatures':return_data.plotfeatres,'plotmasters':return_data.plotmasters,'plotchilds':return_data.plotchild,company_id:company_id,'floors':return_data.floors});
    });


});

router.post('/update',(request,response)=>{

    var id=request.body.plot_id;
    var company_id=request.session.COMPANY_ID;
    var data={
        project_id:request.body.project_id,
        name:request.body.name,
        floor_id:request.body.floor_id,
        street_no:request.body.street_no,
        block:request.body.block,
        type:request.body.type,
        status:'B',
        flat_type:request.body.flat_type,
        number_of_bedrooms:request.body.number_of_bedrooms,
        squarefeets:request.body.squarefeets,
        rate:request.body.rate,
        company_id:company_id
    }
    mysqlconnection.query(`update plot_master set ? where plot_id=${id}`,data,(err,rows)=>{

        var plot_master_id=id;
        var feature_id=request.body.feature_id;
        var percentage=request.body.percentage;


        mysqlconnection.query(`delete from plot_child where  plot_id=${id}`,()=>{
            for(var i=0;i<feature_id.length;i++){

                var data={
                    plot_id:plot_master_id,
                    feature_id:feature_id[i],
                    percentage:percentage[i]
                }
                mysqlconnection.query('insert into plot_child set ?',data,(err,rows)=>{

                });

            }

            request.flash('user', 'Data Successfully Updated');
            response.redirect('/buildermaster/list')
        });
    });
});

router.get('/delete/:id?',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var invoice_number;
    mysqlconnection.query(`UPDATE plot_master SET active_flag='N' where plot_id=${id} And company_id=${company_id}`);
    mysqlconnection.query(`DELETE FROM plot_child where plot_id=${id}`);

    request.flash('user', 'Data Successfully Deleted');
    response.redirect('/buildermaster/list')
});

router.post('/dublicate',(req,res)=>{
    
    var company_id=req.session.COMPANY_ID;
    var floor =req.body.floor;
    var pro =req.body.project;
    var name =req.body.name;
    //  res.json(pro + name +floor);
    
  var sql = 'SELECT * FROM plot_master WHERE project_id = ? AND COMPANY_ID = ? AND name =? AND floor_id =?';
        mysqlconnection.query(sql, [pro, company_id,name,floor], function (err, result) {
          if(err) throw err;
          
          res.json(result);
      
        });
           
});
module.exports=router;

