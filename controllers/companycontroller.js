/**
 * Created by belawal on 1/24/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var http = require("http");

var multer  = require('multer');
var upload = multer({ dest: 'public/img/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');


var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};
router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

var nodemailer = require('nodemailer');

router.get("/add",(request,res)=>{
    console.log(request.session.EMAIL);
    mysqlconnection.query('select * from types',(err,rows)=>{
        if(!err){
            res.render('./company/addcompany.twig',{types:rows});
        }
        else{

        }
    });

});

router.get("/messages",(request,res)=>{

    mysqlconnection.query(`SELECT contact_us.*,types.name as type_name From contact_us LEFT JOIN types on types.id=contact_us.type_id where contact_us.approve_status='V' ORDER BY contact_us.id DESC `,(err,rows)=>{
//res.send(rows);
        res.render('./company/contact_us.twig',{messages:rows})
    });

});

router.post('/registercompany', upload.single('company_logo'), function(request, response) {
    var logo=request.file.originalname;
    var file = 'public/img' + '/' + request.file.originalname;
    fs.rename(request.file.path, file, function(err) {
         console.log('success')
    });
    var company_id;
   var data={
        'NAME':request.body.name,
        'SHORT_NAME':request.body.short_name,
        'DESCRIPTION':request.body.description,
        'CONTACT':request.body.contact,
        'EMAIL':request.body.email,
        'PASSWORD':request.body.password,
        'lat':request.body.lat,
       'lang':request.body.lang,
       'address':request.body.address,
        'LOGO_LARGE':logo,
        'LOGO_SMALL':logo,
       'type':request.body.type
   }
    mysqlconnection.query('insert INTO company_setup set ? ' ,data,(err,rows)=>{
        if (err) {
            console.log(err);
        }else{
           company_id=rows.insertId;
            var data_party={
                'NAME':request.body.name,
                'DESCRIPTION':request.body.description,
                'CONTACT_NO':request.body.contact,
                'EMAIL':request.body.email,
                'TYPE':'E',
                'role':'Admin',
                'OPENING_BALANCE':0,
                'PASSWORD':request.body.password,
                'COMPANY_ID':company_id
            }

            mysqlconnection.query('insert INTO parties SET ?',data_party,(err,rows)=>{
                if(err){
                    console.log(err)
                }
                else{
                    console.log('success insertion into parties')
                }
            })
        }
        var categories=request.body.check_list;

        for(var i=0;i<categories.length;i++){
            console.log(categories[i])
            var data={
                'name':categories[i],
                'COMPANY_ID':company_id
            }
            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                if(err) console.log(err)
            });

        }
    });



    var company=request.body.short_name;
    var mycompany = encodeURI(company);
    var options = {
        host: 'www.hajanaone.com',
        port: 80,
        path: `/api/sendsms.php?apikey=05469878cef2757da06a744036e9adbd&phone=923136258447&sender=SmartSMS&message=${mycompany}+has+been+registered`,
        method: 'GET'
    };

    var req = http.request(options, function(res) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('BODY: ' + chunk);
        });
    });

    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

// write data to request body
    req.write('data\n');
    req.write('data\n');


    var company=request.body.short_name;
    var password=request.body.password;
    var email=request.body.email;
    var contact_no=request.body.contact
    var msg=`Hello your company ${company} has been registered with hissab. Comany Code is ${company}, Your Email is ${email}  Your password is ${password}`;
    var mymsg = encodeURI(msg);
    var options = {
        host: 'www.hajanaone.com',
        port: 80,
        path: `/api/sendsms.php?apikey=05469878cef2757da06a744036e9adbd&phone=${contact_no}&sender=SmartSMS&message=${mymsg}`,
        method: 'GET'
    };

    var req = http.request(options, function(res) {
        console.log('STATUS: ' + res.statusCode);
        console.log('HEADERS: ' + JSON.stringify(res.headers));
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            console.log('BODY: ' + chunk);
        });
    });

    req.on('error', function(e) {
        console.log('problem with request: ' + e.message);
    });

// write data to request body
    req.write('data\n');
    req.write('data\n');

    response.redirect('/company/list');

});

router.get('/list',(request,res)=>{
   mysqlconnection.query('SELECT * From company_setup ORDER BY COMPANY_ID DESC ',(err,rows)=>{
      res.render('./company/companylist.twig',{companies:rows})
    
   });
});


router.get('/test',(request,res)=>{
//    mysqlconnection.query('SELECT * From company_setup',(err,rows)=>{
//        res.render('./company/test.twig')
//    });
    console.log('hello bilawal the companpy id is:' + request.session.COMPANY_ID);
});

router.get('/head',(request,res)=>{
    res.render('./login/head2.twig')
})

router.post('/test',(request,res)=>{
    var name=request.body.name.length;
    for(var i=0;i<name;i++){
        console.log(request.body.name[i]);
    }
});


router.get('/getcompanies',(request,response)=>{

    mysqlconnection.query(`select * from company_setup order By COMPANY_ID DESC LIMIT 1`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});

router.get('/getcompany/:id?',(request,response)=>{

    mysqlconnection.query(`select * from company_setup where COMPANY_ID=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});


router.get('/add_new_company/:id?',(request,response)=>{

    console.log('i am here');
    mysqlconnection.query(`SELECT contact_us.*,types.name as type_name,types.AMOUNT,types.PER_MONTH,types.EXPIRY FROM contact_us LEFT JOIN types ON types.id=contact_us.type_id where contact_us.id=${request.params.id}`,(err,rows)=>{
        if(!err){

            var type=rows[0].type;
            var type_name=rows[0].type_name;
            var amount=rows[0].AMOUNT;
            var expiry=rows[0].EXPIRY;
            var per_month=rows[0].PER_MONTH;
            var name=rows[0].name;
            var short_name=rows[0].company_code;
            var contact=rows[0].number;
            var  email=rows[0].email;
            var  password='admin';
            var company_id;
            var date = new Date();

            date.setDate(date.getDate() + expiry);

            var data={
                'NAME':name,
                'SHORT_NAME':short_name,
                'CONTACT':contact,
                'EMAIL':email,
                'PASSWORD':password,
                'type':type,
                'amount':amount,
                'per_month':per_month,
                 'LOGO_LARGE':'hissablogo.png',
                 'LOGO_SMALL':'hissablogo.png',
                'expiry':date
            }

            mysqlconnection.query('insert INTO company_setup set ? ' ,data,(err,rows)=>{
                if (err) {
                    console.log(err);
                }else{
                    company_id=rows.insertId;
                    var data_party={
                        'NAME':name,
                        'CONTACT_NO':contact,
                        'EMAIL':email,
                        'TYPE':'E',
                        'role':'Admin',
                        'OPENING_BALANCE':0,
                        'PASSWORD':'admin',
                        'COMPANY_ID':company_id
                    }

                    mysqlconnection.query('insert INTO parties SET ?',data_party,(err,rows)=>{
                        if(err){
                            console.log(err)
                        }
                        else{
                            console.log('success insertion into parties')
                        }
                    });
                    
                    
                    
                    var data={
                        NAME:'CASH_IN_HAND',
                        DESCRIPTION:"",
                        OPENING_BALANCE:0,
                        HEAD_ID:'1',
                        COMPANY_ID:company_id
                    }
                    mysqlconnection.query('INSERT INTO accounts set ?',data,(err,rows)=>{
                        if(!err){
                        }
                        else{
                            console.log(err);
                        }
                    });


                    if(type_name=='POS'){
                        console.log('it is point of sale');
                        var menus = ["account_head","expenses","banks","cash","income","union_sale_invoice","union_purchase_invoice","vouchers","damages","voucher_expense","voucher_income","voucher_cash_payment","voucher_cash_receive","voucher_bank_withdraw","voucher_bank_deposit","reports","users","vendors","customers","categories","products","manage_account","sale_return","purchase_return","bookers","remainder","city","sale","purchase"];
                        var arrayLength = menus.length;
                        for (var i = 0; i < arrayLength; i++) {
                            var data={
                                'name':menus[i],
                                'COMPANY_ID':company_id
                            }
                            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                                if(err) console.log(err)
                            });
                        }


                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                             auth: {
                            user: 'admin@hissab.pk',
                            pass: '@bilawal12345'
                            },
                          tls: {
                         rejectUnauthorized: false
                        }
                        });

                        var mailOptions = {
                            from: 'admin@hissab.pk',
                             to: `${email}`,
                            subject: 'Welcome To Hissab',
                            html: `<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3>Email:${email}</h3><h3>Password:Admin</h3></div><p>Hissab integrate and automate your retail and other operations – from the Point of Sale to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a point of sale (POS) solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of POS systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3>Use Hissab as 30 days for free trial</h3></button></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5><b>This is what you get with premium access:</b></h5> <ul type=""><li>Unlimited connection time</li><li>Unlock all streaming services</li><li>Available on all platform <strong>(app for every device!)</strong></li><li>Connection up to 12 devices at the same time</li> <li>+40 server locations to connect</li> </ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>`
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                    }






                    if(type_name=='CURRENCY EXCHANGE'){
                        console.log('it is point of sale');
                        var menus = ["manage_account","vouchers","users","destinatinos","expenses","account_head","currencies","fakhar_customers","hawala","hawala_receive","hawala_purchase","hawala_cashreceive","hawala_cashpayment","hawala_voucher_expense","hawala_reports","hawala_cashreceive","hawala_cash_receive","hawala_cash_payment","hawala_exchange_menu","hawala_menu","hawala_cash_closing"];
                        var arrayLength = menus.length;
                        for (var i = 0; i < arrayLength; i++) {
                            var data={
                                'name':menus[i],
                                'COMPANY_ID':company_id
                            }
                            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                                if(err) console.log(err)
                            });
                        }


                         var transporter = nodemailer.createTransport({
                            service: 'gmail',
                             auth: {
                            user: 'admin@hissab.pk',
                            pass: '@bilawal12345'
                            },
                          tls: {
                         rejectUnauthorized: false
                        }
                        });

                        var mailOptions = {
                            from: 'admin@hissab.pk',
                             to: `${email}`,
                            subject: 'Welcome To Hissab',
                            html: `<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3>Email:${email}</h3><h3>Password:Admin</h3></div><p>Hissab integrate and automate your retail and other operations – from the Point of Sale to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a point of sale (POS) solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of POS systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3>Use Hissab as 30 days for free trial</h3></button></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5><b>This is what you get with premium access:</b></h5> <ul type=""><li>Unlimited connection time</li><li>Unlock all streaming services</li><li>Available on all platform <strong>(app for every device!)</strong></li><li>Connection up to 12 devices at the same time</li> <li>+40 server locations to connect</li> </ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>`
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                    }


                    if(type_name=='TRANSPORT'){
                        console.log('it is point of sale');
                        var menus = ["banks","manage_account","cash","income","sale","trip","sale_dollars","sale_form_i","purchase","purchase_dollars","purchase_form_i","damages","vouchers","voucher_expense","voucher_income","voucher_cash_payment","voucher_cash_receive","voucher_bank_deposit","reports","voucher_bank_withdraw","users","products","categories","vendors","customers","destinatinos","expenses","account_head","destinations","city"];
                        var arrayLength = menus.length;
                        for (var i = 0; i < arrayLength; i++) {
                            var data={
                                'name':menus[i],
                                'COMPANY_ID':company_id
                            }
                            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                                if(err) console.log(err)
                            });
                        }


                       var transporter = nodemailer.createTransport({
                            service: 'gmail',
                             auth: {
                            user: 'admin@hissab.pk',
                            pass: '@bilawal12345'
                            },
                          tls: {
                         rejectUnauthorized: false
                        }
                        });

                        var mailOptions = {
                            from: 'admin@hissab.pk',
                             to: `${email}`,
                            subject: 'Welcome To Hissab',
                            html: `<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3>Email:${email}</h3><h3>Password:Admin</h3></div><p>Hissab integrate and automate your retail and other operations – from the Point of Sale to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a point of sale (POS) solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of POS systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3>Use Hissab as 30 days for free trial</h3></button></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5><b>This is what you get with premium access:</b></h5> <ul type=""><li>Unlimited connection time</li><li>Unlock all streaming services</li><li>Available on all platform <strong>(app for every device!)</strong></li><li>Connection up to 12 devices at the same time</li> <li>+40 server locations to connect</li> </ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>`
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                    }

                    var data={
                        'approve_status':'Y'
                    }
                    mysqlconnection.query(`update contact_us set ? Where id=${request.params.id}`, data,(err,rows)=>{

                    });

                }
            });


            response.redirect('/company/messages')



        }
        else{
            response.send(err);
        }
    });
});
// company expriy date update section
router.get('/date_update',(request,res)=>{
  mysqlconnection.query('SELECT * From company_setup ORDER BY COMPANY_ID DESC ',(err,rows)=>{
      res.render('./company_expiry_date/company_date_update_list.twig',{companies:rows});
  });
});

router.get('/edit_date_update/:id?',(req,res)=>{
    var id =req.params.id;
   mysqlconnection.query(`select * from company_setup where COMPANY_ID =${id}`,(err,rows)=>{
	if(err) throw err;
	else{
		d =rows;
		res.render('./company_expiry_date/edit.twig',{data:d});
	}
});

});

router.post('/update_date', function(req, res){
     id =req.body.id;
    var data={
        expiry:req.body.new_date
    }
    
 mysqlconnection.query(`UPDATE company_setup set ? where COMPANY_ID=${id}`,data,(err,rows)=>{
        if(err) throw err;
        
        else{
           res.redirect('/company/date_update');
        }
    });
	});


module.exports=router;