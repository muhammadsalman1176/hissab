/**
 * Created by belawal on 2/18/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select damages_master.*,parties.name as p_name from damages_master LEFT JOIN parties on damages_master.customer_id=parties.PARTY_ID where  damages_master.company_id=${company_id} ORDER BY damages_master.ID DESC`,(err,rows)=>{
        if(err) response.send(err);
        response.render('./damages/list.twig',{damages:rows});

    });



});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var damages_master=`select * from damages_master ORDER BY ID DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(damages_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.invoice = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        if(return_data.invoice.length>0){
            var invoice_number=return_data.invoice[0].invoice_number+1;
        }
        else{
            var invoice_number=1;
        }
        res.render('./damages/add.twig',{'customers':return_data.customers , 'categories':return_data.categories,'invoice_number':invoice_number,sale_id:sale_id});
    });
});


router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        invoice_number:request.body.invoice_number,
        invoice_date:request.body.invoice_date,
        payment_type:request.body.payment_type,
        customer_id:request.body.customer_id,
        net_amount:request.body.net_amount,
        internal_notes:request.body.internal_notes,
        external_notes:request.body.external_notes,
        company_id:company_id
    }
    mysqlconnection.query('insert into damages_master set ?',data,(err,rows)=>{
        if(!err){
            var damages_master_id=rows.insertId;
            var categories=request.body.category_id;
            var product_id=request.body.product_id;
            var quantity=request.body.quantity;
            var price=request.body.price;
            var total=request.body.total;
            var remarks=request.body.remarks;
            var cost_price=request.body.cost_price;
            for(var i=0;i<categories.length;i++){
                var data={
                    damages_master_id:damages_master_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:quantity[i],
                    total:total[i],
                   cost_price:cost_price[i],
                    remarks:remarks[i]
                }
                mysqlconnection.query('insert into damages_child set ?',data,(err,rows)=>{
                 if(!err){

                 }
                    else{
                     console.log(err);
                 }
                });
            }

            for(var i=0;i<categories.length;i++){
                var data={
                    transection_id:request.body.invoice_number,
                    transection_date:request.body.invoice_date,
                    company_id:company_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:-(quantity[i]),
                    cost_price:cost_price[i],
                    type:'DEMAGE'
                }
                mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                    if(err) console.log(err);
                });
            }


            console.log("the master id is " +rows.insertId);
            request.session.sale_id=rows.insertId;
            response.redirect('/damages/add');
        }
        else{
            response.send(err)
        }
    });
});


router.get('/cat_product/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from products where COMPANY_ID=${company_id} AND CATEGORY_ID=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});


module.exports=router;


