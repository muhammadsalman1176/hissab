

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list', function(req, res){
	var company_id=req.session.COMPANY_ID;
	mysqlconnection.query(`select * from floor where COMPANY_ID=${company_id} ORDER BY floor_id DESC`,(err, rows)=>{
		var c = rows
    res.render('./floor/list.twig',{data:c});
	});
   
});

router.get('/add', function(req, res){
	res.render('./floor/add.twig');
});

router.post('/insert', function(req, res){

    var company_id=req.session.COMPANY_ID;

    var data={
        floor_name:req.body.floor,
        COMPANY_ID:company_id
    }
    // res.send(data);
	mysqlconnection.query('insert INTO floor set ?',data, function(error, result, fields){
		if(error) throw error;
		else{
			res.redirect('/floor/list');
		}
	});
	});

router.get('/edit/:id?', function(req, res){
	var id = req.params.id;
	// res.send('edit');
mysqlconnection.query(`select * from floor where floor_id =${id}`,(err,rows)=>{
	if(err) throw err;
	else{
		d =rows;
		res.render('./floor/edit.twig',{data:d});
	}
});
});

router.post('/update/:id?', function(req, res){
	var id = req.params.id;
	// res.send('update' + id);
	mysqlconnection.query(`UPDATE floor set ? where floor_id=${id}`,req.body,(err,result,fields)=>{
		if(err) throw err;
		else{
			res.redirect('/floor/list');
		}
	});
});

router.get('/delete/:id?', function(req, res){
var id = req.params.id;
mysqlconnection.query(`DELETE from floor where floor_id=${id}`, (err,result)=>{
	if(err) throw err;
	else{
		res.redirect('/floor/list');
	}
});
});
module.exports=router;