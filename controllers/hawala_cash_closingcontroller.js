/**
 * Created by belawal on 8/31/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    mysqlconnection.query(`select * from cash_closing_master ORDER BY cash_closing_master_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./hawala_cash_closing/list.twig',{hawalas:rows,sale_id:sale_id,'messages': request.flash('user')});
    });


});

router.get('/add',(request,response)=>{


    var company_id=request.session.COMPANY_ID;
    var cash_in_hand = `SELECT IFNULL(opening_balance,0)+IFNULL(view_cash_open.debate,0)-IFNULL(view_cash_open.credit,0) as BALANCE,currency_name,currency_id FROM currency LEFT JOIN view_cash_open ON view_cash_open.currency_id=currency.currencyid WHERE currency.COMPANY_ID=${company_id}`;
    var receivables=`SELECT IFNULL(party_opening.opening_balance,0) as open, IFNULL(SUM(party_ledger.credit),0) as cr, IFNULL(SUM(party_ledger.debit),0) as dr, IFNULL(SUM(party_opening.opening_balance),0) - IFNULL(SUM(party_ledger.credit),0) + IFNULL(SUM(party_ledger.debit),0) as balance, currency.currency_name, party_opening.currency_id FROM party_opening LEFT JOIN party_ledger on party_ledger.party_id=party_opening.party_id AND party_ledger.currency_id=party_opening.currency_id and party_ledger.company_id=party_opening.company_id LEFT JOIN currency ON currency.currencyid=party_opening.currency_id WHERE party_opening.COMPANY_ID=${company_id} GROUP BY party_opening.currency_id,currency.currency_name`;
    var closing_master=`select * from cash_closing_master ORDER BY cash_closing_master_id DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(cash_in_hand, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.cash_in_hand = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(receivables, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.receivables= results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(closing_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.closing_master = results;
                parallel_done();
            });
        },

    ], function(err) {
           if (err) console.log(err);
//           response.send(return_data.receivables)

        var invoice_number="";

        if(return_data.closing_master.length>0){

            var invoice_number=return_data.closing_master[0].invoice_number+1;

        }
        else{

            var invoice_number=1;
        }


        response.render('./hawala_cash_closing/add.twig',{'cash_in_hand':return_data.cash_in_hand,'invoice_number':invoice_number ,'receivables':return_data.receivables ,company_id:company_id});
    });
});


router.post('/store',(request,response,cb)=>{
    var company_id=request.session.COMPANY_ID;


    var data={

        date:request.body.date,
        net_grand_total:request.body.net_grand_total,
        invoice_number:request.body.invoice_number,
        company_id:company_id
    }
    mysqlconnection.query('insert into cash_closing_master set ?',data,(err,rows)=>{
        if(!err){
            var cash_closing_master_id=rows.insertId;
            var cash_in_hand_currency_id=request.body.cash_in_hand_currency_id;
            var cash_in_hand_balance=request.body.cash_in_hand_balance;
            var cash_in_hand_exchange_rate=request.body.cash_in_hand_exchange_rate;
            var cash_in_hand_symbol=request.body.cash_in_hand_symbol;
            var cash_in_hand_total=request.body.cash_in_hand_total;

            for(var i=0;i<cash_in_hand_currency_id.length;i++){

                var data={
                    cash_closing_master_id:cash_closing_master_id,
                    currency_id:cash_in_hand_currency_id[i],
                    balance:cash_in_hand_balance[i],
                    exchange_rate:cash_in_hand_exchange_rate[i],
                    symbol:cash_in_hand_symbol[i],
                    total:cash_in_hand_total[i],
                    type:'CASH'
                }
                mysqlconnection.query('insert into cash_closing_child set ?',data,(err,rows)=>{

                });

            }


            request.session.sale_id=rows.insertId;

            var receivable_currency_id=request.body.receivable_currency_id;
            var receivable_balance=request.body.receivable_balance;
            var receivable_exchange_rate=request.body.receivable_exchange_rate;
            var receivable_symbol=request.body.receivable_symbol;
            var receivable_total=request.body.receivable_total;

            for(var i=0;i<receivable_currency_id.length;i++){

                var data={
                    cash_closing_master_id:cash_closing_master_id,
                    currency_id:receivable_currency_id[i],
                    balance:receivable_balance[i],
                    exchange_rate:receivable_exchange_rate[i],
                    symbol:receivable_symbol[i],
                    total:receivable_total[i],
                    type:'RECEIVABLE'
                }
                mysqlconnection.query('insert into cash_closing_child set ?',data,(err,rows)=>{

                });

            }


            var payable_currency_id=request.body.payable_currency_id;
            var payable_balance=request.body.payable_balance;
            var payable_exchange_rate=request.body.payable_exchange_rate;
            var payable_symbol=request.body.payable_symbol;
            var payable_total=request.body.payable_total;

            for(var i=0;i<payable_currency_id.length;i++){

                var data={
                    cash_closing_master_id:cash_closing_master_id,
                    currency_id:payable_currency_id[i],
                    balance:payable_balance[i],
                    exchange_rate:payable_exchange_rate[i],
                    symbol:payable_symbol[i],
                    total:payable_total[i],
                    type:'PAYABLE'
                }
                mysqlconnection.query('insert into cash_closing_child set ?',data,(err,rows)=>{

                });

            }







//         response.send('done');

            response.redirect('/hawala_cash_closing/list');
        }
        else{
            response.send(err)
        }
    });
});


router.get('/print/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var cash_closing_master=`select * from cash_closing_master where company_id=${company_id} and cash_closing_master_id=${id}`;
    var cash_closing_child=`SELECT cash_closing_child.*,currency.currency_name FROM cash_closing_child LEFT JOIN currency on currency.currencyid=cash_closing_child.currency_id  where  cash_closing_child.cash_closing_master_id=${id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(cash_closing_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.cash_closing_master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(cash_closing_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.cash_closing_child = results;
                parallel_done();
            });
        },

    ], function(err) {
        if (err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE,
            tax_number:request.session.tax_number
        }
//        console.log(return_data.cash_closing_child);

        response.render('./hawala_cash_closing/print.twig',{'masters':return_data.cash_closing_master , 'childs':return_data.cash_closing_child,session:session_values});
    });


});

router.get('/delete/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;



        mysqlconnection.query(`delete from cash_closing_master where cash_closing_master_id=${id} and COMPANY_ID=${company_id}`);
        mysqlconnection.query(`delete from cash_closing_child where cash_closing_master_id=${id}`);



    request.flash('user', 'Data Successfully Deleted');
    response.redirect('/hawala_cash_closing/list');
});

module.exports=router;