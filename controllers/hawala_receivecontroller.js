/**
 * Created by belawal on 6/18/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var sale_id = request.session.sale_id;
    request.session.sale_id = null;

    mysqlconnection.query(`SELECT hawala.*,p1.name as from_customer_name,p2.name as to_customer_name,currency.currency_name FROM hawala LEFT JOIN parties p1 on hawala.from_customer=p1.PARTY_ID LEFT JOIN parties p2 on hawala.to_customer=p2.PARTY_ID LEFT JOIN currency on hawala.currency_id=currency.currencyid where hawala.TYPE='RECEIVE' AND hawala.COMPANY_ID=${company_id} ORDER BY hawala.hawala_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./hawala_receive/list.twig',{'messages': request.flash('user'),hawalas:rows,sale_id:sale_id});
    });



});


router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;
    var hawala_master=`select * from hawala ORDER BY hawala_id DESC limit 1`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(hawala_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.hawalas = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);

        var invoice_number="";

        if(return_data.hawalas.length>0){
            var invoice_number=return_data.hawalas[0].hawala_number+1;
        }
        else{
            var invoice_number=1;
        }
        console.log(invoice_number);


        response.render('./hawala_receive/add.twig',{'customers':return_data.customers ,'currencies':return_data.currencies ,company_id:company_id,hawala_number:invoice_number});
    });
});


router.post('/store',upload.single('product_pic'),(request,response)=>{
    var filename="";
    if(!request.file){

    }
    else{
        var file = 'public/cnics' + '/' + request.file.originalname;
        fs.rename(request.file.path, file, function(err) {
//            response.send('done')
        });
        filename=request.file.originalname;
    }

    var company_id=request.session.COMPANY_ID;
    var data={
        hawala_number:request.body.hawala_number,
        hawala_date:request.body.hawala_date,
        hawala_time:request.body.hawala_time,
        TYPE:'RECEIVE',
        customer_type:request.body.customer_type,
        from_customer:request.body.from_customer,
        to_customer:request.body.to_customer,
        currency_id:request.body.currency_id,
        hawala_amount:request.body.hawala_amount,
        hawala_commission:request.body.hawala_commission,
        commission_currency_id:request.body.commission_currency_id,
        remarks:request.body.remarks,
        conversion_currency_id:request.body.conversion_currency_id,
        exchange_rate:request.body.exchange_rate,
        total_amount:request.body.total_amount,
        cnic:filename,
        status:request.body.status,
        from_name:request.body.from_name,
        to_name:request.body.to_name,
        COMPANY_ID:request.session.COMPANY_ID
    }

    mysqlconnection.query('insert into hawala set ?' , data,(err,rows)=>{
        if(!err){
            var status=request.body.status;
            if(status=='clear'){

                if(request.body.customer_type=='permanent'){

                }
                else{
                    var data={
                        voucher_no:request.body.hawala_number,
                        voucher_date:request.body.hawala_date,
                        type:"HAWALA_RECEIVE",
                        credit:request.body.hawala_amount,
                        COMPANY_ID:company_id,
                        debit:0,
                        currency_id:request.body.currency_id,
                        remarks:'Hawala Receive  on Hawala number'+request.body.hawala_number
                    }

                    mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                }




                request.session.sale_id=rows.insertId;

                var data={
                    voucher_no:request.body.hawala_number,
                    voucher_date:request.body.hawala_date,
                    type:"COMMISSION_RECEIVE",
                    credit:request.body.hawala_commission,
                    COMPANY_ID:company_id,
                    debit:0,
                    currency_id:request.body.commission_currency_id,
                    remarks:'Hawala Commission Receive  on Hawala number'+request.body.hawala_number
                }

                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });


                if(request.body.to_customer==0 || request.body.to_customer==undefined){

                }
                else{
                    var data={
                        voucher_no:request.body.hawala_number,
                        voucher_date:request.body.hawala_date,
                        type:"HAWALA_RECEIVE",
                        party_id:request.body.to_customer,
                        credit:0,
                        COMPANY_ID:company_id,
                        debit:request.body.hawala_amount,
                        currency_id:request.body.currency_id,
                        remarks:'HAWALA_RECEIVE'
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                }


                if(request.body.exchange_rate==""){
                    var data={
                        voucher_no:request.body.hawala_number,
                        voucher_date:request.body.hawala_date,
                        type:"HAWALA_RECEIVE",
                        party_id:request.body.from_customer,
                        credit:0,
                        COMPANY_ID:company_id,
                        debit:request.body.hawala_amount,
                        currency_id:request.body.currency_id,
                        remarks:'HAWALA_RECEIVE'
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                }
                else{
                    var data={
                        voucher_no:request.body.hawala_number,
                        voucher_date:request.body.hawala_date,
                        type:"HAWALA_RECEIVE",
                        party_id:request.body.from_customer,
                        credit:0,
                        COMPANY_ID:company_id,
                        exchange_rate:request.body.exchange_rate,
                        debit:request.body.total_amount,
                        currency_id:request.body.conversion_currency_id,
                        remarks:'HAWALA_RECEIVE'
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                }




                request.flash('user', 'Data Successfully Inserted');
                response.redirect('/hawala_receive/list');
            }
            else{
                request.session.sale_id=rows.insertId;
                request.flash('user', 'Data Successfully Inserted');
                response.redirect('/hawala_receive/list');
            }

        }
        else{
            console.log('error:'. err)
        }
    });



});

router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;

    var hawalas=`select * from hawala where hawala_id=${request.params.id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(hawalas, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.hawalas = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
//      response.send(return_data.hawalas)

        response.render('./hawala_receive/edit.twig',{'customers':return_data.customers ,'currencies':return_data.currencies,'hawalas':return_data.hawalas ,company_id:company_id});
    });

});


router.post('/update',upload.single('product_pic'),(request,response)=>{
    var filename="";
    if(!request.file){
        filename=request.body.old_product_pic;
    }
    else{
        var file = 'public/cnics' + '/' + request.file.originalname;
        fs.rename(request.file.path, file, function(err) {
//            response.send('done')
        });
        filename=request.file.originalname;
    }



    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`delete from cash_ledger where voucher_no=${request.body.hawala_number} and COMPANY_ID=${company_id} AND (type = 'HAWALA_RECEIVE' OR type='COMMISSION_RECEIVE')`,(err,rows)=>{
        mysqlconnection.query(`delete from party_ledger where voucher_no=${request.body.hawala_number} and COMPANY_ID=${company_id}  AND type='HAWALA_RECEIVE'`,(err,rows)=>{
        var data={
            hawala_number:request.body.hawala_number,
            hawala_date:request.body.hawala_date,
            TYPE:'RECEIVE',
            customer_type:request.body.customer_type,
            from_customer:request.body.from_customer,
            to_customer:request.body.to_customer,
            currency_id:request.body.currency_id,
            hawala_amount:request.body.hawala_amount,
            hawala_commission:request.body.hawala_commission,
            commission_currency_id:request.body.commission_currency_id,
            remarks:request.body.remarks,
            conversion_currency_id:request.body.conversion_currency_id,
            exchange_rate:request.body.exchange_rate,
            total_amount:request.body.total_amount,
            cnic:filename,
            from_name:request.body.from_name,
            to_name:request.body.to_name,
            status:request.body.status,
            COMPANY_ID:request.session.COMPANY_ID
        }

        mysqlconnection.query(`update hawala set ? where hawala_id=${request.body.id}`,data,(err,rows)=>{

            if(!err){
                var status=request.body.status;
                if(status=='clear'){

                    if(request.body.customer_type=='permanent'){

                    }
                    else{
                        var data={
                            voucher_no:request.body.hawala_number,
                            voucher_date:request.body.hawala_date,
                            type:"HAWALA_RECEIVE",
                            credit:request.body.hawala_amount,
                            COMPANY_ID:company_id,
                            debit:0,
                            currency_id:request.body.currency_id,
                            remarks:'Hawala Receive  on Hawala number'+request.body.hawala_number
                        }

                        mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });
                    }



                    var data={
                        voucher_no:request.body.hawala_number,
                        voucher_date:request.body.hawala_date,
                        type:"COMMISSION_RECEIVE",
                        credit:request.body.hawala_commission,
                        COMPANY_ID:company_id,
                        debit:0,
                        currency_id:request.body.commission_currency_id,
                        remarks:'Hawala Commission Receive  on Hawala number'+request.body.hawala_number
                    }

                    mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });


                    if(request.body.to_customer==0 || request.body.to_customer==undefined){

                    }
                    else{
                        var data={
                            voucher_no:request.body.hawala_number,
                            voucher_date:request.body.hawala_date,
                            type:"HAWALA_RECEIVE",
                            party_id:request.body.to_customer,
                            credit:0,
                            COMPANY_ID:company_id,
                            debit:request.body.hawala_amount,
                            currency_id:request.body.currency_id,
                            remarks:'HAWALA_RECEIVE'
                        }
                        mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });
                    }


                    if(request.body.exchange_rate==""){
                        var data={
                            voucher_no:request.body.hawala_number,
                            voucher_date:request.body.hawala_date,
                            type:"HAWALA_RECEIVE",
                            party_id:request.body.from_customer,
                            credit:0,
                            COMPANY_ID:company_id,
                            debit:request.body.hawala_amount,
                            currency_id:request.body.currency_id,
                            remarks:'HAWALA_RECEIVE'
                        }
                        mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });
                    }
                    else{
                        var data={
                            voucher_no:request.body.hawala_number,
                            voucher_date:request.body.hawala_date,
                            type:"HAWALA_RECEIVE",
                            party_id:request.body.from_customer,
                            credit:0,
                            COMPANY_ID:company_id,
                            exchange_rate:request.body.exchange_rate,
                            debit:request.body.total_amount,
                            currency_id:request.body.conversion_currency_id,
                            remarks:'HAWALA_RECEIVE'
                        }
                        mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });
                    }

                    request.flash('user', 'Data Successfully Inserted');
                    response.redirect('/hawala_receive/list');
                }
                else{
                    request.flash('user', 'Data Successfully Inserted');
                    response.redirect('/hawala_receive/list');
                }
            }
            else{
                console.log('error:'. err)
            }
        });
    });
    });
});


router.get('/print/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    mysqlconnection.query(`SELECT hawala.*,p1.name as from_customer_name,p2.name as to_customer_name,p2.ADDRESS,currency.currency_name,currency_commision.currency_name as currency_commision,conversion_currency_commision.currency_name as conversion_currency_commision FROM hawala LEFT JOIN parties p1 on hawala.from_customer=p1.PARTY_ID LEFT JOIN parties p2 on hawala.to_customer=p2.PARTY_ID LEFT JOIN currency on hawala.currency_id=currency.currencyid LEFT JOIN currency currency_commision on hawala.currency_id=currency_commision.currencyid LEFT JOIN currency conversion_currency_commision on hawala.currency_id=conversion_currency_commision.currencyid where hawala.TYPE='RECEIVE' AND hawala.COMPANY_ID=${company_id} AND hawala.hawala_id=${id} ORDER BY hawala.hawala_id DESC`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE,
            tax_number:request.session.tax_number
        }
        response.render('./hawala_receive/print.twig',{'results':rows,session:session_values });
    });

});

router.get('/delete/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var invoice_number;
    mysqlconnection.query(`select * from hawala where hawala_id=${id}`,(err,rows)=>{
        invoice_number=rows[0].hawala_number;
        mysqlconnection.query(`delete  from hawala where hawala_id=${id}`);
        mysqlconnection.query(`delete from cash_ledger where voucher_no=${invoice_number} and COMPANY_ID=${company_id} AND (type = 'HAWALA_RECEIVE' OR type='COMMISSION_RECEIVE')`);
        mysqlconnection.query(`delete from party_ledger where voucher_no=${invoice_number} and COMPANY_ID=${company_id} AND type='HAWALA_RECEIVE'`);


    })
    request.flash('user', 'Data Successfully Deleted');
    response.redirect('/hawala_receive/list');
});

module.exports=router;
