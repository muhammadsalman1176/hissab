/**
 * Created by belawal on 7/22/20.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
         var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT currency_transaction.*,c1.currency_name as from_currency,c2.currency_name as to_currency FROM currency_transaction LEFT JOIN currency c1 on c1.currencyid=currency_transaction.from_currency_id LEFT JOIN currency c2 on c2.currencyid=currency_transaction.to_currency_id where currency_transaction.TYPE='CURRENCY_SALE' AND currency_transaction.COMPANY_ID=${company_id} ORDER BY currency_transaction.transaction_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./hawala_sale/list.twig',{'messages': request.flash('user'),hawalas:rows});
    });



});

router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;
    var hawala_master=`select * from currency_transaction ORDER BY transaction_id DESC limit 1`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(hawala_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.hawalas = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);


        var invoice_number="";

        if(return_data.hawalas.length>0){

            var invoice_number=return_data.hawalas[0].invoice_number+1;

        }
        else{

            var invoice_number=1;
        }
        console.log(invoice_number);



        response.render('./hawala_sale/add.twig',{'customers':return_data.customers ,'currencies':return_data.currencies ,company_id:company_id,hawala_number:invoice_number});
    });
});




router.post('/store',upload.single('product_pic'),(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        invoice_number:request.body.invoice_number,
        date:request.body.date,
        time:request.body.time,
        TYPE:'CURRENCY_SALE',
        from_currency_id:request.body.from_currency,
        to_currency_id:request.body.to_currency,
        remarks:request.body.remarks,
        exchange_rate:request.body.exchange_rate,
        amount:request.body.amount,
        total_amount:request.body.total_amount,
        COMPANY_ID:request.session.COMPANY_ID
    }

    mysqlconnection.query('insert into currency_transaction set ?' , data,(err,rows)=>{
        if(!err){
            var data={
                voucher_no:request.body.invoice_number,
                voucher_date:request.body.date,
                type:"CURRENCY_SALE",
                credit:request.body.total_amount,
                COMPANY_ID:company_id,
                debit:0,
                currency_id:request.body.to_currency,
                exchange_rate:request.body.exchange_rate,
                remarks:'Hawala Sale on Invoice Number'+request.body.invoice_number
            }

            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });

            var data={
                voucher_no:request.body.invoice_number,
                voucher_date:request.body.date,
                type:"CURRENCY_SALE",
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.amount,
                currency_id:request.body.from_currency,
                exchange_rate:request.body.exchange_rate,
                remarks:'Hawala Sale on Invoice Number'+request.body.invoice_number
            }

            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });


            request.flash('user', 'Data Successfully Inserted');
            response.redirect('/hawala_sale/list');
        }
        else{
            console.log('error:'. err)
        }
    });

//         for(var i=0; i<rows.length;i++){
//             console.log("Opening is  : "+request.opening_balance[0]);
//         }



});

router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;

    var hawalas=`select * from currency_transaction where transaction_id=${request.params.id}`;

    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(hawalas, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.hawalas = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);


        response.render('./hawala_sale/edit.twig',{'currencies':return_data.currencies,'hawalas':return_data.hawalas ,company_id:company_id});
    });

});

router.post('/update',upload.single('product_pic'),(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    console.log(request.body.invoice_number)
    mysqlconnection.query(`delete from cash_ledger where voucher_no=${request.body.invoice_number} and COMPANY_ID=${company_id} and type='CURRENCY_SALE'`,(err,rows)=>{
        var data={
            invoice_number:request.body.invoice_number,
            date:request.body.date,
            time:request.body.time,
            TYPE:'CURRENCY_SALE',
            from_currency_id:request.body.from_currency,
            to_currency_id:request.body.to_currency,
            remarks:request.body.remarks,
            exchange_rate:request.body.exchange_rate,
            amount:request.body.amount,
            total_amount:request.body.total_amount,
            COMPANY_ID:request.session.COMPANY_ID
        }
        mysqlconnection.query(`update currency_transaction set ? where transaction_id=${request.body.id}`,data,(err,rows)=>{

            var data={
                voucher_no:request.body.invoice_number,
                voucher_date:request.body.date,
                type:"CURRENCY_SALE",
                credit:request.body.total_amount,
                COMPANY_ID:company_id,
                debit:0,
                currency_id:request.body.to_currency,
                exchange_rate:request.body.exchange_rate,
                remarks:'Hawala Sale on Invoice Number'+request.body.invoice_number
            }

            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });

            var data={
                voucher_no:request.body.invoice_number,
                voucher_date:request.body.date,
                type:"CURRENCY_SALE",
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.amount,
                currency_id:request.body.from_currency,
                exchange_rate:request.body.exchange_rate,
                remarks:'Hawala Sale on Invoice Number'+request.body.invoice_number
            }

            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });


            request.flash('user', 'Data Successfully Update');
            response.redirect('/hawala_sale/list');

        });
    });
});

router.get('/delete/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var invoice_number;
    mysqlconnection.query(`select * from currency_transaction where transaction_id=${id}`,(err,rows)=>{
        invoice_number=rows[0].invoice_number;
        mysqlconnection.query(`delete from currency_transaction where transaction_id=${id} and COMPANY_ID=${company_id}`);
        mysqlconnection.query(`delete from cash_ledger where voucher_no=${invoice_number} and COMPANY_ID=${company_id} and type='CURRENCY_SALE'`);

    })
    request.flash('user', 'Data Successfully Deleted');
    response.redirect('/hawala_sale/list');
});



module.exports=router;