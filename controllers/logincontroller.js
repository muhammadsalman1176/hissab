/**
 * Created by belawal on 1/15/19.
 */

const express=require('express');
var cookieParser = require('cookie-parser');

const app=express();

var router=express.Router();

const bodyparser=require('body-parser');
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());


app.use(cookieParser());

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

var mysqlconnection=require('../connection.js');

var nodemailer = require('nodemailer');

app.use(function (req, res, next) {
    res.locals = {
        site: {
            title: 'ExpressBootstrapEJS',
            description: 'A boilerplate for a simple web application with a Node.JS and Express backend, with an EJS template with using Twitter Bootstrap.'
        },
        author: {
            name: 'Cory Gross',
            contact: 'CoryG89@gmail.com'
        }
    };
    next();
});

app.set('views','./views');
app.set('view engine','twig');


router.get("/malikice",(request,res)=>{
    request.session.name='Malik ICE';
    request.session.COMPANY_ID=31;
    request.session.CONTACT= '03369189596';
    request.session.EMAIL='icetry@gmail.com';
    request.session.SHORT_NAME='Malik_Ice';
    request.session.LOGO_LARGE='4-MIF.png';
    res.render('./login/userlogin2.twig',{image: request.session.LOGO_LARGE,session: request.session})

//    res.render('./login/companycode.twig')
});

router.get("/",(request,res)=>{
//    request.session.name='Malik ICE';
//    request.session.CONTACT=03369189596;
//    request.session.EMAIL='icetry@gmail.com';
//    request.session.SHORT_NAME='Malik_Ice';

//    res.render('./login/companycode.twig')
//    res.send('i am here bilawal');

    var types = `select * from types`;



    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(types, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.types = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);

        res.render('./general/main.twig',{types:return_data.types})
//        response.render('./hawala_purchase/add.twig',{'customers':return_data.customers ,'currencies':return_data.currencies ,company_id:company_id,hawala_number:invoice_number});
    });

//    res.render('./general/main.twig')
});

router.get("/login",(request,res)=>{
//    request.session.name='Malik ICE';
//    request.session.CONTACT=03369189596;
//    request.session.EMAIL='icetry@gmail.com';
//    request.session.SHORT_NAME='Malik_Ice';

    res.render('./login/companycode.twig')
//    res.render('./general/main.twig')
});

router.get("/adminlogin",(request,res)=>{
    res.render('./login/adminlogin.twig')
});

router.post('/adminlogin',(request,response)=>{
    var email=request.body.email;
    var password=request.body.password;

    mysqlconnection.query('select * from admins where EMAIL=? and password=?',[email,password],(err,results)=>{
        if(results.length>0){
            request.session.COMPANY_ID=1;
            request.session.admin_email=results[0].email;
            response.redirect('company/add');
        }
        else{

            response.render('./login/adminlogin.twig',{msg:'Email or Password is wrong'})
        }
    });
});



router.post('/companycode',(request,response)=>{
   var shortname=request.body.code;
   mysqlconnection.query('SELECT * FROM company_setup WHERE SHORT_NAME=?',[shortname],(err,result)=>{
      if(result.length>0){
          request.session.name=result[0].NAME;
          request.session.COMPANY_ID=result[0].COMPANY_ID;
          request.session.CONTACT=result[0].CONTACT;
          request.session.EMAIL=result[0].EMAIL;
          request.session.SHORT_NAME=result[0].SHORT_NAME;
          request.session.LOGO_LARGE=result[0].LOGO_LARGE;
          request.session.tax_number=result[0].tax_number;

          var date = new Date();
          console.log("Today Date : " +date);
          var startDate = Date.parse(date);
          console.log("Expiry Date : " +result[0].expiry);
          var endDate = Date.parse(result[0].expiry);
          var timeDiff = endDate - startDate;
          daysDiff = Math.floor(timeDiff / (1000 * 60 * 60 * 24));

          if(daysDiff>0){
              return response.render('./login/userlogin2.twig',{image: request.session.LOGO_LARGE,session: request.session,difference:daysDiff})
          }else{
              response.render('./login/companycode.twig',{msg:'Please Contact With Admin for your account'})
          }

//
      }
      else{
          response.render('./login/companycode.twig',{msg:'Company Code is wrong'})
      }
   })
});


router.post('/userlogin',(request,response)=>{
   var email=request.body.email;
   var password=request.body.password;
   var company_id=request.session.COMPANY_ID;
   var role=request.body.role;
  console.log(company_id);

// response.send(company_id);

  mysqlconnection.query('select * from parties where EMAIL=? and password=? and COMPANY_ID=?  and role=? limit 1',[email,password,company_id,role],(err,results)=>{
     if(results.length>0){
         request.session.role=results[0].role;
         request.session.party_id=results[0].PARTY_ID;
        //  response.render('./login/head2.twig');
          response.redirect('/clientdashboard');
     }
      else{

         response.render('./login/userlogin2.twig',{msg:'Email or Password is wrong',image: request.session.LOGO_LARGE})
     }
  });
});

router.get("/clientdashboard",(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var today_sale = `SELECT IFNULL(sum(net_amount),0) AS TODAY_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND invoice_date=CURRENT_DATE`;
    var today_credit_sale = `SELECT IFNULL(sum(net_amount),0) AS TODAY_CREDIT_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='credit' AND invoice_date=CURRENT_DATE`;
    var today_cash_sale = `SELECT IFNULL(sum(net_amount),0) AS TODAY_CASH_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='cash' AND invoice_date=CURRENT_DATE`;

    var monthly_sale=`SELECT ROUND(COALESCE(sum(net_amount),0),2) AS Monthly_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND MONTH(invoice_date) = MONTH(CURRENT_DATE())`;
    var monthly_credit_sale=`SELECT ROUND(COALESCE(sum(net_amount),0),2) AS Monthly_credit_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='credit'  AND MONTH(invoice_date) = MONTH(CURRENT_DATE())`;
    var monthly_cash_sale=`SELECT ROUND(COALESCE(sum(net_amount),0),2) AS Monthly_cash_SALE from sale_master WHERE company_id=${company_id} AND type='S' AND payment_type='cash'  AND MONTH(invoice_date) = MONTH(CURRENT_DATE())`;


    var total_sale=`SELECT ROUND(sum(net_amount),2) AS TODAY_SALE from sale_master WHERE company_id=${company_id} AND type='S'`;
    var total_credit_sale=`SELECT ROUND(sum(net_amount),2) AS TODAY_credit_SALE from sale_master WHERE company_id=${company_id} AND payment_type='credit'  AND type='S'`;
    var total_cash_sale=`SELECT ROUND(sum(net_amount),2) AS TODAY_cash_SALE from sale_master WHERE company_id=${company_id} AND payment_type='cash'  AND type='S'`;


   var monthly_expense=`SELECT COALESCE(sum(AMOUNT),0) as AMOUNT FROM vouchers WHERE COMPANY_ID=${company_id} and TYPE='EX' AND MONTH(VOUCHER_DATE) = MONTH(CURRENT_DATE())`;
   var total_expense=`SELECT COALESCE(sum(AMOUNT),0) as AMOUNT FROM vouchers WHERE COMPANY_ID=${company_id} and TYPE='EX'`;

   var payables=``;
   
     var remainders=`select remainder.*,parties.name as p_name from remainder LEFT JOIN parties on remainder.PARTY_ID=parties.PARTY_ID WHERE remainder.company_id=${company_id}  AND date=CURRENT_DATE ORDER BY remainder.id DESC`
    var previous_remainders=`select remainder.*,parties.name as p_name from remainder LEFT JOIN parties on remainder.PARTY_ID=parties.PARTY_ID WHERE remainder.company_id=${company_id} AND remainder.status='N' AND date<CURRENT_DATE ORDER BY remainder.id DESC`;


    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(today_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.today_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(today_credit_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.today_credit_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(today_cash_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.today_cash_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(monthly_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(monthly_credit_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_credit_sale = results;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(monthly_cash_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_cash_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_credit_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_credit_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_cash_sale, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_cash_sale = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(monthly_expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.monthly_expense = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(total_expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.total_expense = results;
                parallel_done();
            });
        },
         function(parallel_done) {
            mysqlconnection.query(remainders, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.remainders = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(previous_remainders, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.previouse_remainders = results;
                parallel_done();
            });
        },
    ], function(err) {
        if (err) console.log(err);
//           response.send(return_data.total_expense);
            response.render('./login/clientdashboard.twig',{today_sale:return_data.today_sale , 'today_credit_sale':return_data.today_credit_sale,'today_cash_sale':return_data.today_cash_sale,'monthly_sale':return_data.monthly_sale,'monthly_credit_sale':return_data.monthly_credit_sale,'monthly_cash_sale':return_data.monthly_cash_sale,'total_sale':return_data.total_sale,'total_credit_sale':return_data.total_credit_sale,'total_cash_sale':return_data.total_cash_sale,'monthly_expense':return_data.monthly_expense,'total_expense':return_data.total_expense,'remainders':return_data.remainders,'previous_remainders':return_data.previouse_remainders});
    });


});


router.post('/insert_contact_us',(request,response)=>{
    
      console.log('i am here and i am working');
      var c_code="";
      mysqlconnection.query(`select * from company_setup where SHORT_NAME='${request.body.company_code}' OR EMAIL='${request.body.email}'`,(err,rows)=>{
      if(err) console.log(err);
      if(rows.length>0){
        response.json(1)
      }else{
          const c = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        const s = [...Array(15)].map(_ => c[~~(Math.random()*c.length)]).join('');
        console.log("random", s);
        var data={
        name:request.body.name,
        email:request.body.email,
        company_code:request.body.company_code,
        number:request.body.number,
        type_id:request.body.type,
        message:request.body.message,
        token:s
    }
       
    mysqlconnection.query('insert into contact_us set ?' , data,(err,rows)=>{
      if(err) console.log(err)

         mysqlconnection.query(`select * from types where id=${request.body.type}`,(err,rows)=>{
        type_name=rows[0].name;
       
        var email=request.body.email;

        var transporter = nodemailer.createTransport({
            service: 'gmail',
             auth: {
            user: 'admin@hissab.pk',
            pass: '@bilawal12345'
        },
         tls: {
          rejectUnauthorized: false
          }
        });

        var mailOptions = {
            from: 'admin@hissab.pk',
            to: `${email}`,
            subject: 'Email Verification',
         
             html: `<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3></h3></div><p>Hissab integrate and automate your retail and other operations – from the ${type_name} to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a point of sale (${type_name}) solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of ${type_name} systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><a ><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3><p> <a href="http://hissab.pk/verify_email/${s}">Click here to Activate Your Account</a> </p></h3></button></a></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5></h5> <ul type=""></ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>`
        };

        transporter.sendMail(mailOptions, function(error, info){
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
            }
        });
        response.json(0)
        
         });
    });
      }
      
      });
    //   var emal ="";
    //   mysqlconnection.query(`select * from company_setup where EMAIL='${request.body.email}'`,(err,rows)=>{
    //   if(err) console.log(err);
    //   if(rows.length>0){
    //         emal =1;
    //   }else{
    //         emal =0;
    //   }
    //   });
    // //   khurm section end
    
    
    //   const c = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    //     const s = [...Array(15)].map(_ => c[~~(Math.random()*c.length)]).join('');
    //     console.log("random", s);
    //     var data={
    //     name:request.body.name,
    //     email:request.body.email,
    //     number:request.body.number,
    //     type_id:request.body.type,
    //     message:request.body.message,
    //     token:s
    // }
       
    // mysqlconnection.query('insert into contact_us set ?' , data,(err,rows)=>{
    //   if(err) console.log(err)

    //     var email=request.body.email;

    //     var transporter = nodemailer.createTransport({
    //         service: 'gmail',
    //          auth: {
    //         user: 'admin@hissab.pk',
    //         pass: '@bilawal12345'
    //     },
    //      tls: {
    //       rejectUnauthorized: false
    //       }
    //     });

    //     var mailOptions = {
    //         from: 'admin@hissab.pk',
    //         to: `${email}`,
    //         subject: 'Email Verification',
    //         html: '<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3></h3></div><p>Hissab integrate and automate your retail and other operations – from the Point of Sale to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a point of sale (POS) solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of POS systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><a ><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3><p> <a href="http://hissab.pk/verify_email/' + s +'">Click here to Activate Your Account</a> </p></h3></button></a></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5><b>This is what you get with premium access:</b></h5> <ul type=""><li>Unlimited connection time</li><li>Unlock all streaming services</li><li>Available on all platform <strong>(app for every device!)</strong></li><li>Connection up to 12 devices at the same time</li> <li>+40 server locations to connect</li> </ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>'
    //     };

    //     transporter.sendMail(mailOptions, function(error, info){
    //         if (error) {
    //             console.log(error);
    //         } else {
    //             console.log('Email sent: ' + info.response);
    //         }
    //     });
    //     response.json(1)
    // });
});


router.get("/verify_email/:token?",(request,res)=>{
    
    
    mysqlconnection.query(`select * from contact_us where token='${request.params.token}' And approve_status="N"`,(err,rows)=>{
      
      if(err) console.log(err);
      var contact_us_id=rows[0].id;
      
      if(rows.length>0){
          var data={
              approve_status:'V'
          }
          mysqlconnection.query(`update contact_us set ? where token='${request.params.token}'`,data,(err,rows)=>{
              
              
                  console.log('i am here');
    mysqlconnection.query(`SELECT contact_us.*,types.name as type_name,types.AMOUNT,types.PER_MONTH,types.EXPIRY FROM contact_us LEFT JOIN types ON types.id=contact_us.type_id where contact_us.id=${contact_us_id}`,(err,rows)=>{
        if(!err){

            var type=rows[0].type;
            var type_name=rows[0].type_name;
            var amount=rows[0].AMOUNT;
            var expiry=rows[0].EXPIRY;
            var per_month=rows[0].PER_MONTH;
            var name=rows[0].name;
            var short_name=rows[0].company_code;
            var contact=rows[0].number;
            var  email=rows[0].email;
            var  password='admin';
            var company_id;
            var date = new Date();

            date.setDate(date.getDate() + expiry);

            var data={
                'NAME':name,
                'SHORT_NAME':short_name,
                'CONTACT':contact,
                'EMAIL':email,
                'PASSWORD':password,
                'type':type,
                'amount':amount,
                'per_month':per_month,
                 'LOGO_LARGE':'hissablogo.png',
                 'LOGO_SMALL':'hissablogo.png',
                'expiry':date
            }

            mysqlconnection.query('insert INTO company_setup set ? ' ,data,(err,rows)=>{
                if (err) {
                    console.log(err);
                }else{
                    company_id=rows.insertId;
                    var data_party={
                        'NAME':name,
                        'CONTACT_NO':contact,
                        'EMAIL':email,
                        'TYPE':'E',
                        'role':'Admin',
                        'OPENING_BALANCE':0,
                        'PASSWORD':'admin',
                        'COMPANY_ID':company_id
                    }

                    mysqlconnection.query('insert INTO parties SET ?',data_party,(err,rows)=>{
                        if(err){
                            console.log(err)
                        }
                        else{
                            console.log('success insertion into parties')
                        }
                    });
                    
                    
                    
                    var data={
                        NAME:'CASH_IN_HAND',
                        DESCRIPTION:"",
                        OPENING_BALANCE:0,
                        HEAD_ID:'1',
                        COMPANY_ID:company_id
                    }
                    mysqlconnection.query('INSERT INTO accounts set ?',data,(err,rows)=>{
                        if(!err){
                        }
                        else{
                            console.log(err);
                        }
                    });


                    if(type_name=='POS'){
                        console.log('it is point of sale');
                        var menus = ["account_head","expenses","banks","cash","income","union_sale_invoice","union_purchase_invoice","vouchers","damages","voucher_expense","voucher_income","voucher_cash_payment","voucher_cash_receive","voucher_bank_withdraw","voucher_bank_deposit","reports","users","vendors","customers","categories","products","manage_account","sale_return","purchase_return","bookers","remainder","city","sale","purchase"];
                        var arrayLength = menus.length;
                        for (var i = 0; i < arrayLength; i++) {
                            var data={
                                'name':menus[i],
                                'COMPANY_ID':company_id
                            }
                            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                                if(err) console.log(err)
                            });
                        }


                        var transporter = nodemailer.createTransport({
                            service: 'gmail',
                             auth: {
                            user: 'admin@hissab.pk',
                            pass: '@bilawal12345'
                            },
                          tls: {
                         rejectUnauthorized: false
                        }
                        });

                        var mailOptions = {
                            from: 'admin@hissab.pk',
                             to: `${email}`,
                            subject: 'Welcome To Hissab',
                             html: `<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3>Email:${email}</h3><h3>Password:Admin</h3><h3>Link : <a href="http://hissab.pk/login">Login</a></h3></div><p>Hissab integrate and automate your retail and other operations – from the ${type_name} to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a ${type_name} solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of ${type_name} systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3>Use Hissab upto ${expiry} days for free trial</h3></button></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5><b>After Free Trials Your Account Charges Will be As Follow:</b></h5> <ul type=""><li>Registration Fees: <b>${amount}</b></li><li>Per Month Charges : <b>${per_month}</b></li></ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>`
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                    }






                    if(type_name=='CURRENCY EXCHANGE'){
                        console.log('it is point of sale');
                        var menus = ["manage_account","vouchers","users","destinatinos","expenses","account_head","currencies","fakhar_customers","hawala","hawala_receive","hawala_purchase","hawala_cashreceive","hawala_cashpayment","hawala_voucher_expense","hawala_reports","hawala_cashreceive","hawala_cash_receive","hawala_cash_payment","hawala_exchange_menu","hawala_menu","hawala_cash_closing"];
                        var arrayLength = menus.length;
                        for (var i = 0; i < arrayLength; i++) {
                            var data={
                                'name':menus[i],
                                'COMPANY_ID':company_id
                            }
                            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                                if(err) console.log(err)
                            });
                        }


                         var transporter = nodemailer.createTransport({
                            service: 'gmail',
                             auth: {
                            user: 'admin@hissab.pk',
                            pass: '@bilawal12345'
                            },
                          tls: {
                         rejectUnauthorized: false
                        }
                        });

                        var mailOptions = {
                            from: 'admin@hissab.pk',
                             to: `${email}`,
                            subject: 'Welcome To Hissab',
                            html: `<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3>Email:${email}</h3><h3>Password:Admin</h3><h3>Link : <a href="http://hissab.pk/login">Login</a></h3></div><p>Hissab integrate and automate your retail and other operations – from the ${type_name} to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a ${type_name} solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of ${type_name} systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3>Use Hissab upto ${expiry} days for free trial</h3></button></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5><b>After Free Trials Your Account Charges Will be As Follow:</b></h5> <ul type=""><li>Registration Fees: <b>${amount}</b></li><li>Per Month Charges : <b>${per_month}</b></li></ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>`
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                    }


                    if(type_name=='TRANSPORT'){
                        console.log('it is point of sale');
                        var menus = ["banks","manage_account","cash","income","sale","trip","sale_dollars","sale_form_i","purchase","purchase_dollars","purchase_form_i","damages","vouchers","voucher_expense","voucher_income","voucher_cash_payment","voucher_cash_receive","voucher_bank_deposit","reports","voucher_bank_withdraw","users","products","categories","vendors","customers","destinatinos","expenses","account_head","destinations","city"];
                        var arrayLength = menus.length;
                        for (var i = 0; i < arrayLength; i++) {
                            var data={
                                'name':menus[i],
                                'COMPANY_ID':company_id
                            }
                            mysqlconnection.query('insert into assigned_menus SET ?',data,(err,rows)=>{
                                if(err) console.log(err)
                            });
                        }


                       var transporter = nodemailer.createTransport({
                            service: 'gmail',
                             auth: {
                            user: 'admin@hissab.pk',
                            pass: '@bilawal12345'
                            },
                          tls: {
                         rejectUnauthorized: false
                        }
                        });

                        var mailOptions = {
                            from: 'admin@hissab.pk',
                             to: `${email}`,
                            subject: 'Welcome To Hissab',
                             html: `<body><div class="container" style="background-color: azure; margin-left: 20%; margin-right: 20%;"><section style="margin-left: 30px"><div align="center"><h2><strong style="font-size: 20px">Welcome To</strong> Hissab.pk</h2><h3>Email:${email}</h3><h3>Password:Admin</h3><h3>Link : <a href="http://hissab.pk/login">Login</a></h3></div><p>Hissab integrate and automate your retail and other operations – from the ${type_name} to the back office and up and down your supply chain, we turns your business into an efficient, smoothly running operation that can recognize and adapt quickly to change.When selecting a ${type_name} solution, users have a choice between stand-alone solutions and integrated solutions. They should first evaluate core and non-core components of ${type_name} systems, and assess the strengths and weaknesses of best-of-breed and integrated approaches</p></section> <div align="center"><button type="button" class="btn-group btn-lg btn-group-justified btn-success" style="border-radius: 3px; background: lightgreen; height: 60px; width: 100%; "><h3>Use Hissab upto ${expiry} days for free trial</h3></button></div><p style="text-align: center; font-size:12px"><i>Activate your account by directly contact with admin.</i></p><section style="margin-left: 30px"><h5><b>After Free Trials Your Account Charges Will be As Follow:</b></h5> <ul type=""><li>Registration Fees: <b>${amount}</b></li><li>Per Month Charges : <b>${per_month}</b></li></ul><p>If you have any questions or need help, simply respond to this email or get in touch at any point via our 24/7 live chat.</p>Kind regards<br><b>Hissab.pk Support</b><br></section> </div> </body>`
                        };

                        transporter.sendMail(mailOptions, function(error, info){
                            if (error) {
                                console.log(error);
                            } else {
                                console.log('Email sent: ' + info.response);
                            }
                        });

                    }

                    var data={
                        'approve_status':'Y'
                    }
                    mysqlconnection.query(`update contact_us set ? Where id=${request.params.id}`, data,(err,rows)=>{

                    });

                }
            });


         



        }
        else{
            response.send(err);
        }
    });
              
              res.render('./login/companycode.twig',{msg:'Email Verified. Check Email Again for Credentials'}) 
          })
      }
        else{
           res.render('./login/companycode.twig',{msg:'Email Already Verified'})
      }
    });
    
});


module.exports=router;

