/**
 * Created by belawal on 9/8/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');
router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT payment_modes.*,projects.project_name FROM payment_modes LEFT JOIN projects on projects.project_id=payment_modes.project_id  WHERE payment_modes.company_id=${company_id} and payment_modes.active_flag='Y' ORDER BY mode_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./payment_modes/list.twig',{'messages': request.flash('user'),modes:rows});
    });




});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var projects = `select * from projects where company_id=${company_id} And type='Builder' AND active_flag='Y'`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        }


    ], function(err) {
        if (err) console.log(err);
//        res.send(return_data.projects);
        res.render('./payment_modes/add.twig',{'projects':return_data.projects ,company_id:company_id});
    });
});

router.post('/store',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var data={
        company_id:company_id,
        project_id:request.body.project_id,
        name:request.body.name,
        type:request.body.type
    }

    mysqlconnection.query('insert into payment_modes set ?' , data,(err,rows)=>{
        if(err) console.log(err)

        request.flash('user', 'Data Successfully Inserted');
        response.redirect('/payment_modes/list')
    });
});

router.get('/edit/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var payment_modes=`select * from payment_modes where company_id=${company_id} and mode_id=${req.params.id}`;
    var projects=`select * from projects where company_id = ${company_id} and active_flag='Y'`;
    var return_data = {};
    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(payment_modes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.payment_modes = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        }
    ], function(err) {

        res.render('./payment_modes/edit.twig',{record:return_data.payment_modes,projects:return_data.projects});
    });
});

router.post('/update',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var data={
        company_id:company_id,
        project_id:request.body.project_id,
        name:request.body.name,
        type:request.body.type
    }


    mysqlconnection.query(`update payment_modes set ? where mode_id=${request.body.mode_id}`,data,(err,rows)=>{
        request.flash('user', 'Data Successfully Updated');
        response.redirect('/payment_modes/list');
    });


});

router.get('/delete/:id?',(request,response)=>{
    var id=request.params.id;
    mysqlconnection.query(`UPDATE payment_modes SET active_flag='N' where mode_id=${id}`,(err,rows)=>{
        if(!err){
            request.flash('user', 'Data Successfully Deleted');
            response.redirect('/payment_modes/list');
        }
        else{
            console.log('error'+err)
        }
    });
});


module.exports=router;
