/**
 * Created by belawal on 9/8/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT payment_schedules.*,projects.project_name,plotsizes.size_name,payment_modes.name as mode_name FROM payment_schedules LEFT JOIN projects on projects.project_id=payment_schedules.project_id LEFT JOIN plotsizes on plotsizes.size_id=payment_schedules.size_id LEFT JOIN payment_modes on payment_modes.mode_id=payment_schedules.mode_id WHERE payment_schedules.company_id=${company_id}  and payment_schedules.active_flag='Y' ORDER BY schedule_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./payment_schedule/list.twig',{'messages': request.flash('user'),schedules:rows});
    });




});

router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y' AND type='Developer'`;
    var plotsizes = `select * from plotsizes where company_id=${company_id} and active_flag='Y'`;

    var modes = `select * from payment_modes where company_id=${company_id} and active_flag='Y'`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotsizes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotsizes = results;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(modes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.modes = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);
//        res.send(return_data.projects);
        res.render('./payment_schedule/add.twig',{'projects':return_data.projects , 'plotsizes':return_data.plotsizes, 'modes':return_data.modes,company_id:company_id});
    });
});

router.post('/store',(request,response,cb)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        project_id:request.body.project_id,
        size_id:request.body.size_id,
        plot_type:request.body.plot_type,
        grand_total:request.body.grand_total,

        company_id:company_id
    }

    mysqlconnection.query('insert into payment_schedules set ?',data,(err,rows)=>{
        if(!err){
            var payment_schedule_master_id=rows.insertId;
            var mode_id=request.body.mode_id;
            var amount=request.body.amount;
            var unit=request.body.unit;
            var type=request.body.type;
            var total=request.body.total;

            for(var i=0;i<mode_id.length;i++){

                var data={
                    payment_schedule_master_id:payment_schedule_master_id,
                    mode_id:mode_id[i],
                    amount:amount[i],
                    unit:unit[i],
                    total:total[i],
                    type:type[i]
                }
                mysqlconnection.query('insert into payment_schedules_child set ?',data,(err,rows)=>{

                });

            }

            request.flash('user', 'Data Successfully Inserted');
            response.redirect('/payment_schedule/list')
        }
        else{
            response.send(err)
        }
    });
});


router.get('/print',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var payment_modes=`select * from payment_modes where company_id=${company_id} and active_flag='Y'`;
    var plotsizes = `select * from plotsizes where company_id=${company_id} and active_flag='Y'`;
    var schedules = `select * from payment_schedules where company_id=${company_id} and active_flag='Y'`;
    var return_data = {};
    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(payment_modes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.modes = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotsizes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotsizes = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(schedules, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.schedules = results;
                parallel_done();
            });
        }
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:req.session.SHORT_NAME,
            email:req.session.EMAIL,
            contact:req.session.CONTACT,
            logo:req.session.LOGO_LARGE
        }
//        res.send(return_data.modes);
        res.render('./payment_schedule/print.twig',{modes:return_data.modes,sizes:return_data.plotsizes,'session':session_values,schedules:return_data.schedules});
    });
});


router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var master = `select * from payment_schedules where company_id=${company_id} And schedule_id=${request.params.id} AND active_flag='Y'`;
    var childs=`SELECT payment_schedules_child.*,payment_modes.name as mode_name FROM payment_schedules_child LEFT JOIN payment_modes on payment_modes.mode_id=payment_schedules_child.mode_id where payment_schedules_child.payment_schedule_master_id = ${request.params.id}`;
    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y' AND type='Developer'`;
    var plotsizes = `select * from plotsizes where company_id=${company_id} and active_flag='Y'`;

    var modes = `select * from payment_modes where company_id=${company_id} and active_flag='Y'`;


    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.masters = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(childs, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.childs = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(plotsizes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotsizes = results;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(modes, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.modes = results;
                parallel_done();
            });
        }

    ], function(err) {
        if (err) console.log(err);
//      response.send(return_data.hawalas)

        response.render('./payment_schedule/edit.twig',{'projects':return_data.projects , 'plotsizes':return_data.plotsizes, 'modes':return_data.modes,'masters':return_data.masters ,'childs':return_data.childs,company_id:company_id});
    });

});

router.post('/update',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`delete from payment_schedules_child where payment_schedule_master_id=${request.body.schedule_id}`,(err,rows)=>{
        var data={
            project_id:request.body.project_id,
            size_id:request.body.size_id,
            plot_type:request.body.plot_type,
            grand_total:request.body.grand_total,
            company_id:company_id
        }

        mysqlconnection.query(`update payment_schedules set ? where schedule_id=${request.body.schedule_id}`,data,(err,rows)=>{

            if(!err){
                var payment_schedule_master_id=request.body.schedule_id;
                var mode_id=request.body.mode_id;
                var amount=request.body.amount;
                var unit=request.body.unit;
                var total=request.body.total;

                for(var i=0;i<mode_id.length;i++){

                    var data={
                        payment_schedule_master_id:payment_schedule_master_id,
                        mode_id:mode_id[i],
                        amount:amount[i],
                        unit:unit[i],
                        total:total[i]

                    }
                    mysqlconnection.query('insert into payment_schedules_child set ?',data,(err,rows)=>{

                    });

                }

                request.flash('user', 'Data Successfully Inserted');
                response.redirect('/payment_schedule/list')
            }

        });
    });
});

router.get('/delete/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
        mysqlconnection.query(`delete from payment_schedules where schedule_id=${id} and COMPANY_ID=${company_id}`);

        mysqlconnection.query(`delete from payment_schedules_child where payment_schedule_master_id=${id}`);


    request.flash('user', 'Data Successfully Deleted');
    response.redirect('/payment_schedule/list')
});

module.exports=router;
