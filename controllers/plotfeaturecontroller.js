/**
 * Created by belawal on 8/27/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select plotfeatures.*,projects.project_name from plotfeatures LEFT JOIN projects on projects.project_id=plotfeatures.project_id where plotfeatures.company_id=${company_id} and plotfeatures.active_flag='Y' ORDER BY plotfeatures.feature_id DESC`  ,(err,rows)=>{
        if(!err){

            response.render('./plotfeatures/list.twig',{'messages': request.flash('user'),plots:rows})
        }
        else{
            response.send(err);
        }
    })
//    response.render('./product/list.twig')
});


router.get('/add',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from projects where company_id= ${company_id} And type='Builder' And active_flag='Y'`,(err,rows)=>{
//        response.send(rows);
        response.render('./plotfeatures/add.twig',{projects:rows});
    })
});

router.post('/store',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var data={
        COMPANY_ID:company_id,
        project_id:request.body.project_id,
        description:request.body.description,
        percentage:request.body.percentage
    }

    mysqlconnection.query('insert into plotfeatures set ?' , data,(err,rows)=>{
        if(err) console.log(err)

        request.flash('user', 'Data Successfully Inserted');
        response.redirect('/plotfeatures/list')
    });
});


router.get('/edit/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var plotfeatres=`select * from plotfeatures where company_id=${company_id} and feature_id=${req.params.id}`;
    var projects=`select * from projects where company_id = ${company_id} And type='Builder' and active_flag='Y'`;
    var return_data = {};
    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(plotfeatres, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.plotfeatres = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        }
    ], function(err) {

        res.render('./plotfeatures/edit.twig',{record:return_data.plotfeatres,projects:return_data.projects});
    });
});


router.post('/update',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var data={
        COMPANY_ID:company_id,
        project_id:request.body.project_id,
        description:request.body.description,
        percentage:request.body.percentage
    }


    mysqlconnection.query(`update plotfeatures set ? where feature_id=${request.body.feature_id}`,data,(err,rows)=>{
        request.flash('user', 'Data Successfully Updated');
        response.redirect('/plotfeatures/list');
    });


});

router.get('/delete/:id?',(request,response)=>{
    var id=request.params.id;
    mysqlconnection.query(`UPDATE plotfeatures SET active_flag='N' where feature_id=${id}`,(err,rows)=>{
        if(!err){
            request.flash('user', 'Data Successfully Deleted');
            response.redirect('/plotfeatures/list');
        }
        else{
            console.log('error'+err)
        }
    });
});

module.exports=router;