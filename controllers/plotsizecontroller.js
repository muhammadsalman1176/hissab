/**
 * Created by belawal on 8/27/20.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from plotsizes where active_flag='Y' And company_id=${company_id} ORDER BY plotsizes.size_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./plotsizes/list.twig',{'messages': request.flash('user'),plotsizes:rows});
    });

});


router.get('/add',(request,response)=>{

    response.render('./plotsizes/add.twig');

});

router.post('/store',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var data={
        COMPANY_ID:company_id,
        size_name:request.body.size_name,
        length:request.body.length,
        width:request.body.width
    }

    mysqlconnection.query('insert into plotsizes set ?' , data,(err,rows)=>{
        if(err) console.log(err)

        request.flash('user', 'Data Successfully Inserted');
        response.redirect('/plotsizes/list')
    });
});

router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`SELECT * From plotsizes where size_id=${id}`,(err,rows2)=>{
        if(!err){

            res.render('./plotsizes/edit.twig',{plotsizes:rows2})

        }
        else{
            console.log('error'+err)
        }
    });
});


router.post('/update',(request,response)=>{



    var company_id=request.session.COMPANY_ID;
    var data={
        COMPANY_ID:company_id,
        size_name:request.body.size_name,
        length:request.body.length,
        width:request.body.width
    }

    mysqlconnection.query(`update plotsizes set ? where size_id=${request.body.size_id}`,data,(err,rows)=>{
        request.flash('user', 'Data Successfully Updated');
        response.redirect('/plotsizes/list');
    });


});

router.get('/delete/:id?',(request,response)=>{
    var id=request.params.id;
    mysqlconnection.query(`UPDATE plotsizes SET active_flag='N' where size_id=${id}`,(err,rows)=>{
        if(!err){
            request.flash('user', 'Data Successfully Deleted');
            response.redirect('/plotsizes/list');
        }
        else{
            console.log('error'+err)
        }
    });
});

module.exports=router;