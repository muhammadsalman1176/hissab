/**
 * Created by belawal on 8/25/20.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
// var upload = multer({ dest: '/tmp/'});
var storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null,'public/projects/')
  },
  filename:function(req, file, cb){
    cb(null,file.originalname)
  }
});
var upload = multer({storage: storage});



const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');



router.get('/list/:type?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var type=request.params.type;

    mysqlconnection.query(`select * from projects where active_flag='Y' And company_id=${company_id} AND type='${type}' ORDER BY projects.project_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./projects/list.twig',{'messages': request.flash('user'),projects:rows,'type':type});
    });

});

router.get('/add/:type?',(request,response)=>{
    var type=request.params.type;
    response.render('./projects/add.twig',{'type':type});

});

router.post('/store',upload.single('product_pic'),(request,response,next)=>{
    var filename="";
    if(!request.file){

    }
    else{
//         response.send('working');
//         var file = 'public/projects' + '/' + request.file.originalname;
//         fs.rename(request.file.path, file, function(err) {
// //            response.send('done')
//         });
        filename=request.file.originalname;
    }

    var company_id=request.session.COMPANY_ID;
    var data={
        COMPANY_ID:company_id,
        project_name:request.body.project_name,
        project_code:request.body.project_code,
        start_date:request.body.start_date,
        end_date:request.body.end_date,
        project_location:request.body.project_location,
        project_logo:filename,
        project_terms:request.body.project_terms,
        type:request.body.project_type
    }

    mysqlconnection.query('insert into projects set ?' , data,(err,rows)=>{
      if(err) console.log(err)

        request.flash('user', 'Data Successfully Inserted');
        if(request.body.project_type=='Builder'){
            response.redirect('/projects/list/Builder')
        }
        else{
            response.redirect('/projects/list/Developer')
        }

    });



});

router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`SELECT * From projects where project_id=${id}`,(err,rows2)=>{
        if(!err){


                res.render('./projects/edit.twig',{projects:rows2})


        }
        else{
            console.log('error'+err)
        }
    });
});



router.post('/update',upload.single('product_pic'),(request,response)=>{
    var filename="";
    if(!request.file){
        filename=request.body.old_product_pic;
    }
    else{
        var file = '/public/projects' + '/' + request.file.originalname;
        fs.rename(request.file.path, file, function(err) {
//            response.send('done')
        });
        filename=request.file.originalname;
    }



    var company_id=request.session.COMPANY_ID;
    var data={
        COMPANY_ID:company_id,
        project_name:request.body.project_name,
        project_code:request.body.project_code,
        start_date:request.body.start_date,
        end_date:request.body.end_date,
        project_location:request.body.project_location,
        project_logo:filename,
        project_terms:request.body.project_terms,
        type:request.body.project_type
    }
            mysqlconnection.query(`update projects set ? where project_id=${request.body.project_id}`,data,(err,rows)=>{
                request.flash('user', 'Data Successfully Updated');
                if(request.body.project_type=='Builder'){
                    response.redirect('/projects/list/Builder')
                }
                else{
                    response.redirect('/projects/list/Developer')
                }
            });


});

router.get('/delete/:id?/:type?',(request,response)=>{
    var id=request.params.id;
    var type=request.params.type;
    mysqlconnection.query(`UPDATE projects SET active_flag='N' where project_id=${id}`,(err,rows)=>{
        if(!err){
            request.flash('user', 'Data Successfully Deleted');
            if(type=='Builder'){
                response.redirect('/projects/list/Builder')
            }
            else{
                response.redirect('/projects/list/Developer')
            }
        }
        else{
            console.log('error'+err)
        }
    });
});

module.exports=router;