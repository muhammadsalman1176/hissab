/**
 * Created by belawal on 9/18/20.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
// var upload = multer({ dest: '/tmp/'});
var storage = multer.diskStorage({
  destination: function(req, file, cb){
    cb(null,'public/realestateCustomer/');
  },
  filename:function(req, file, cb){
    cb(null,file.originalname);
  }
});
var upload = multer({storage: storage});

const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }));


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages');
app.use(flash());
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');


router.get('/list/:type?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var type=request.params.type;
    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y' AND registration_type='${type}' ORDER BY parties.PARTY_ID DESC`,(err,rows)=>{

        if(!err){
            response.render('./realestate_customers/list.twig',{customers:rows,'type':type});
        }
        else{
            console.log(err);
        }
    });


});

router.get('/add/:type?',(request,response)=>{
    var type=request.params.type;
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from city where COMPANY_ID=${company_id}`,(err, rows)=>{
        response.render('./realestate_customers/add.twig',{cities:rows,'type':type});
    });

});

router.post('/store',upload.fields([{
           name: 'customer_image'
         }, {
           name: 'customer_cnic_image'
         },
         {
            name: 'customer_sig_image'
         },
          {
            name: 'nominee_cnic_image'
         },
         {
            name: 'nominee_image'
         },
          {
            name: 'nominee_sig_image'
         }]),(request,response,next)=>{
    
    
        // filename=request.file.originalname;
        //c stand for customer in this method
        c_img =request.files.customer_image[0];
        c_cnic_img = request.files.customer_cnic_image[0];
        c_sig_img = request.files.customer_sig_image[0];
        
        //nominee get name from add form
        n_img =request.files.nominee_image[0];
        n_cnic_img = request.files.nominee_cnic_image[0];
        n_sig_img = request.files.nominee_sig_image[0];
        
        //original name section 
        customer_img = c_img.originalname;
        customer_cnic_img = c_cnic_img.originalname;
        customer_sig_img = c_sig_img.originalname;
        
        nominee_img = n_img.originalname;
        nominee_cnic_img = n_cnic_img.originalname;
        nominee_sig_img = n_sig_img.originalname;
        
        
        
    
    var data={
        reg_no:request.body.reg_no,
        NAME:request.body.name,
        father_name:request.body.father_name,
        Email:request.body.email,
        occupation:request.body.occupation,
        age:request.body.age,
        nationality:request.body.nationality,
        cnic:request.body.cnic,
        TYPE:'C',
        CONTACT_NO:request.body.contact_number,
        registration_type:request.body.registration_type,
        phone_number:request.body.phone_number,
        residence_number:request.body.residence_number,
        ADDRESS:request.body.address,
        permanent_address:request.body.permanent_address,
        CITY:request.body.city,
        city_id:request.body.city_id,
        DESCRIPTION:request.body.description,
        name_of_nominee:request.body.name_of_nominee,
        address_of_nominee:request.body.address_of_nominee,
        relation:request.body.relation,
        OPENING_BALANCE:request.body.opening_balance,
        COMPANY_ID:request.session.COMPANY_ID,
        customer_img:customer_img,
        customer_cnic_img:customer_cnic_img,
        customer_sig_img:customer_sig_img,
        nominee_img:nominee_img,
        nominee_cnic_img:nominee_cnic_img,
        nominee_sig_img:nominee_sig_img
    };
    mysqlconnection.query('insert into parties set ?' , data,(err,rows)=>{
         if(err) console.log(err);
        if(!err){

            if(request.body.registration_type=='Builder'){
                response.redirect('/realestate_customers/list/Builder')
            }
            else{
                response.redirect('/realestate_customers/list/Developer')
            }


        }
        else{

        }
    });
});

router.get('/detail/:id?',(req,res)=>{
    var id=req.params.id;
    // res.send(id);
    mysqlconnection.query(`SELECT * From parties where PARTY_ID=${id}`,(err,rows2)=>{
        if(!err){
            
                res.render('./realestate_customers/detail.twig',{customers:rows2});
        }
        else{
            console.log('error'+err);
        }
    });
});

router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`SELECT * From parties where PARTY_ID=${id}`,(err,rows2)=>{
        if(!err){

            var company_id=req.session.COMPANY_ID;
            mysqlconnection.query(`select * from city where COMPANY_ID=${company_id}`,(err, rows)=>{

                res.render('./realestate_customers/editcustomer.twig',{customers:rows2,cities:rows});
            });

        }
        else{
            console.log('error'+err);
        }
    });
});

router.post('/update',upload.fields([{
           name: 'customer_image'
         }, {
           name: 'customer_cnic_image'
         },
         {
            name: 'customer_sig_image'
         },
          {
            name: 'nominee_cnic_image'
         },
         {
            name: 'nominee_image'
         },
          {
            name: 'nominee_sig_image'
         }]),(request,response,next)=>{
    
     var party_id=request.body.party_id;
     
     var customer_img="";
     var customer_sig_img="";
     var customer_cnic_img="";
     var nominee_img="";
     var nominee_sig_img="";
     var nominee_cnic_img="";
     
     
    if(!request.files.customer_image)
    {
      
         customer_img  =request.body.customer_image_old; 
    }else{
        var c_img = request.files.customer_image[0];
        customer_img =c_img.originalname;
    }
    
    if(!request.files.customer_cnic_image)
    {
       customer_cnic_img = request.body.customer_cnic_old; 
    }else{
        var c_cnic = request.files.customer_cnic_image[0];
        customer_cnic_img =c_cnic.originalname;
    }
    if(!request.files.customer_sig_image)
    {
      customer_sig_img =request.body.customer_sig_old; 
    }
    else
    {
        var c_sig = request.files.customer_cnic_image[0];
        customer_sig_img =c_sig.originalname;
    }
    //nominee img section 
    if(!request.files.nominee_cnic_image)
    {
      nominee_cnic_img = request.body.nominee_cnic_old; 
    }else{
        var n_cnic=request.files.nominee_cnic_image[0];
        nominee_cnic_img =n_cnic.originalname;
    }
     if(!request.files.nominee_sig_image)
    {
      nominee_sig_img = request.body.nominee_sig_old; 
    }else{
        var n_sig=request.files.nominee_sig_image[0];
        nominee_sig_img =n_sig.originalname;
    }
     if(!request.files.nominee_image)
    {
      nominee_img = request.body.nominee_old; 
    }else{
        var n_img=request.files.nominee_image[0];
        nominee_img =n_img.originalname;
    }
   
    var data={
        reg_no:request.body.reg_no,
        NAME:request.body.name,
        father_name:request.body.father_name,
        Email:request.body.email,
        occupation:request.body.occupation,
        age:request.body.age,
        nationality:request.body.nationality,
        cnic:request.body.cnic,
        TYPE:'C',
        CONTACT_NO:request.body.contact_number,
        phone_number:request.body.phone_number,
        residence_number:request.body.residence_number,
        ADDRESS:request.body.address,
        permanent_address:request.body.permanent_address,
        CITY:request.body.city,
        city_id:request.body.city_id,
        DESCRIPTION:request.body.description,
        name_of_nominee:request.body.name_of_nominee,
        address_of_nominee:request.body.address_of_nominee,
        relation:request.body.relation,
        OPENING_BALANCE:request.body.opening_balance,
        COMPANY_ID:request.session.COMPANY_ID,
            customer_img:customer_img,
            customer_cnic_img:customer_cnic_img,
            customer_sig_img:customer_sig_img,
            nominee_img:nominee_img,
            nominee_cnic_img:nominee_cnic_img,
            nominee_sig_img:nominee_sig_img
    }
    mysqlconnection.query(`UPDATE parties set ? where PARTY_ID=${party_id}`,data,(err,rows)=>{
        if(!err){
           
            if(request.body.registration_type=='Builder'){
                response.redirect('/realestate_customers/list/Builder')
            }
            else{
                response.redirect('/realestate_customers/list/Developer')
            }
        }
        else{
            response.send(err);
        }
    });
});

router.get('/delete/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`UPDATE parties SET status='N' WHERE party_id=${id}`,(err,rows)=>{
        if(!err){
            res.redirect('/realestate_customers/list');
        }
        else{
            console.log('error'+err);
        }
    });
});



module.exports=router;
