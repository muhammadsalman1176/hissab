/**
 * Created by belawal on 12/18/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var return_data = {};
    var company_id=request.session.COMPANY_ID;
    var banks=`SELECT * FROM accounts WHERE HEAD_ID='2' and COMPANY_ID=${company_id}`;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND status='Y' and registration_type='Developer'`;
    var products=`select * from products where COMPANY_ID=${company_id}`;
    var floors=`select * from floor where COMPANY_ID=${company_id}`;
    var categories=`SELECT * FROM category WHERE COMPANY_ID=${company_id}`;

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(floors, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.floors = results;
                parallel_done();
            });
        }
    ], function(err) {
        if(err) console.log(err);
//         response.send(return_data.floors);
        response.render('./reports/realestate_reports.twig',{'banks':return_data.banks,'customers':return_data.customers,'products':return_data.products,'expenses':return_data.expenses,'categories':return_data.categories,'cities':return_data.cities,'floors':return_data.floors});
    });


})

module.exports=router;