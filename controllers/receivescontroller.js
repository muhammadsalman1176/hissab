/**
 * Created by belawal on 2/6/19.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer = require('multer');
var upload = multer({
    dest: '/tmp/'
});


const fileUpload = require('express-fileupload');

app.use(fileUpload({
    safeFileNames: true,
    preserveExtension: true
}))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router = express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function (req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection = require('../connection.js');

app.set('views', './views');
app.set('view engine', 'twig');

router.get('/list', (request, response) => {
    mysqlconnection.query(`select * from receives_master`, (err, rows) => {
        if (err) response.send(err);
        response.render('./receives/list.twig', {
            receives: rows
        });
    });

});

router.get('/add', (request, response) => {
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id = request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y' ORDER By receive_id DESC`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var products = `select * from products where COMPANY_ID=${company_id}`;
    var receives = `select * from receives_master ORDER BY receive_id DESC limit 1`;
    var return_data = {};

    async.parallel([
        function (parallel_done) {
            mysqlconnection.query(customers, {}, function (err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function (parallel_done) {
            mysqlconnection.query(categories, {}, function (err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function (parallel_done) {
            mysqlconnection.query(receives, {}, function (err, results) {
                if (err) return parallel_done(err);
                return_data.receives = results;
                parallel_done();
            });
        },
        function (parallel_done) {
            mysqlconnection.query(products, {}, function (err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        }
    ], function (err) {
        if (err) console.log(err);
        if (return_data.receives[0]) {
            var receive_number = return_data.receives[0].receive_number + 1;
        } else {
            var receive_number = 1;
        }

        response.render('./receives/add.twig', {
            'customers': return_data.customers,
            'categories': return_data.categories,
            'receive_number': receive_number,
            sale_id: sale_id,
            products: return_data.products,
            company_id: company_id
        });
    });


});

router.get('/cat_product/:id?', (request, response) => {
    var company_id = request.session.COMPANY_ID;
    mysqlconnection.query(`select * from products where COMPANY_ID=${company_id} AND CATEGORY_ID=${request.params.id}`, (err, rows) => {
        if (!err) {
            response.json(rows);
        } else {
            response.send(err);
        }
    });
});


router.post('/store', (request, response, cb) => {
    var company_id = request.session.COMPANY_ID;

    var data = {
        receive_number: request.body.receive_number,
        date: request.body.receive_date,
        time: request.body.time,
        receive_total: request.body.net_amount,
        company_id: company_id
    }
    mysqlconnection.query('insert into receives_master set ?', data, (err, rows) => {
        if (!err) {
            var receive_master_id = rows.insertId;
            var categories = request.body.category_id;
            var product_id = request.body.product_id;
            var unit = request.body.unit;
            var quantity = request.body.quantity;
            var price = request.body.price;
            var totalprice = request.body.total;
            var cost_price = request.body.cost_price;
            for (var i = 0; i < categories.length; i++) {

                var data = {
                    receive_master_id: receive_master_id,
                    category_id: categories[i],
                    product_id: product_id[i],
                    unit: unit[i],
                    quantity: quantity[i],
                    price: price[i],
                    totalprice: totalprice[i],
                    price: cost_price[i],
                }
                mysqlconnection.query('insert into receives_child set ?', data, (err, rows) => {
                    if (err) {
                        console.log(err);
                    } else {

                    }
                });

            }
            
                for(var i=0;i<categories.length;i++){
                var data={
                    transection_id:request.body.receive_number,
                    transection_date:request.body.receive_date,
                    company_id:company_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:(quantity[i]),
                    type:'RECEIVE'
                }
                mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                    if(err) console.log(err);
                });
            }
            
            
            

            response.redirect('/receives/list');
        } else {
            response.send(err)
        }
    });
});

router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var products = `select * from products where COMPANY_ID=${company_id}`;
    var receives_master=`select * from receives_master where receive_id=${request.params.id}`;
    var receives_child=`select * from receives_child WHERE receive_master_id=${request.params.id}`;
    var bookers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='B' AND status='Y'`;
    var return_data = {};


    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(receives_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.receives_master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(receives_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.receives_child = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(bookers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.bookers = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);

//        var invoice_number=return_data.invoice[0].invoice_number+1;
//             response.send(return_data.sale_child);
        response.render('./receives/edit.twig',{'customers':return_data.customers , 'categories':return_data.categories,'receives_master':return_data.receives_master,receives_child:return_data.receives_child, products:return_data.products,bookers:return_data.bookers});
    });

});


router.get('/print/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var sale_master=`select sale_master.*,parties.NAME as party_name,parties.CONTACT_NO,parties.EMAIL,parties.ADDRESS from sale_master LEFT JOIN parties on parties.PARTY_ID=sale_master.customer_id where sale_master.company_id=${company_id} And id=${id}`;
    var sale_child=`select sale_child.*,category.NAME as category_name,products.NAME as product_name from sale_child LEFT JOIN category on category.CATEGORY_ID=sale_child.category_id LEFT JOIN products on products.PRODUCT_ID=sale_child.product_id   where  sale_child.sale_master_id=${id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.child = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE,
            tax_number:request.session.tax_number
        }
        console.log(return_data.master);
             var party_id=return_data.master[0].customer_id;
        var voucher_number=return_data.master[0].invoice_number;
        console.log(party_id);
        if(party_id==""){
            response.render('./sale_invoice/print4.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values});
        }
        else{
//            console.log(return_data.master);
            console.log(voucher_number);
            mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND Party_ID=${party_id}`,(err,rows)=>{
                if(!err){
                    var opening_balance=rows[0].OPENING_BALANCE;
                    mysqlconnection.query(`SELECT SUM(credit) as credit_sum, SUM(debit) as debit_sum FROM party_ledger WHERE party_id=${party_id} and voucher_no != ${voucher_number} `,(err,rows)=>{
                 if(err) response.send(err);
//                 response.send(rows);
                        var credit=rows[0].credit_sum;
                        var debit=rows[0].debit_sum;
                        var last_balance=(opening_balance)-(credit)+(debit);
//                        response.json(last_balance);
                        response.render('./sale_invoice/print4.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values,last_balance:last_balance});
                    });
                }
                else{
                    response.send(err);
                }
            });
        }
        
        // response.render('./sale_invoice/print4.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values});
    });


});

router.post('/update2',(request,response)=>{

    mysqlconnection.query(`delete from receives_child where receive_master_id=${request.body.id}`,(err,rows)=>{
         if (err) {
                        console.log(err);
                    } else {
        var company_id=request.session.COMPANY_ID;
        var id=request.body.id;

        var data = {
        receive_number: request.body.receive_number,
        date: request.body.receive_date,
        time: request.body.time,
        receive_total: request.body.net_amount,
        company_id: company_id
    }
        mysqlconnection.query(`update receives_master set ? where receive_id=${request.body.id}`,data,(err,rows)=>{
             if (err) {
                        console.log(err);
                    } else {
             var receive_master_id = request.body.id;
            var categories = request.body.category_id;
            var product_id = request.body.product_id;
            var unit = request.body.unit;
            var quantity = request.body.quantity;
            var price = request.body.price;
            var totalprice = request.body.total;
            var cost_price = request.body.cost_price;
            for (var i = 0; i < categories.length; i++) {

                var data = {
                    receive_master_id: receive_master_id,
                    category_id: categories[i],
                    product_id: product_id[i],
                    unit: unit[i],
                    quantity: quantity[i],
                    price: price[i],
                    totalprice: totalprice[i],
                    price: cost_price[i],
                }
                mysqlconnection.query('insert into receives_child set ?',data,(err,rows)=>{
                     if (err) {
                        console.log(err);
                    } else {
                    }

                });

            }


 mysqlconnection.query(`delete from product_ledger where type='RECEIVE' AND transection_id=${request.body.receive_number}`,()=>{
                     for(var i=0;i<categories.length;i++){
                         var data={
                             transection_id:request.body.receive_number,
                             transection_date:request.body.receive_date,
                             company_id:company_id,
                             category_id:categories[i],
                             product_id:product_id[i],
                             quantity:(quantity[i]),
                             type:'RECEIVE'
                         }
                         mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                             if(err) console.log(err);
                         });
                     }
                 });
            response.redirect('/receives/list');
        }
        });
    }
    });

});

router.get('/delete/:id?',(request,response)=>{
   var id=request.params.id;
   mysqlconnection.query(`select * from receives_master where receive_id=${id}`,(err,rows)=>{
       
       mysqlconnection.query(`DELETE FROM receives_master where receive_id=${id}`,(err,rows)=>{
                     if (err) {
                        console.log(err);
                    } else {
                          mysqlconnection.query(`DELETE FROM receives_child where receive_master_id=${id}`,(err,rows)=>{
                     if (err) {
                        console.log(err);
                    } else {
                        response.redirect('/receives/list');
                    }

                });
                        
                    }

                });
     
   
   });
});



module.exports = router;
