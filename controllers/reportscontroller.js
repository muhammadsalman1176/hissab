/**
 * Created by belawal on 3/25/19.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/all',(request,response)=>{
    var return_data = {};
    var company_id=request.session.COMPANY_ID;
    var banks=`SELECT * FROM accounts WHERE HEAD_ID='2' and COMPANY_ID=${company_id}`;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V' OR type='B') AND status='Y'`;
    var products=`select * from products where COMPANY_ID=${company_id}`;
    var expenses=`SELECT * FROM accounts WHERE HEAD_ID='3' and COMPANY_ID=${company_id}`
    var cities=`SELECT * FROM city WHERE  COMPANY_ID=${company_id}`
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;
    var categories=`SELECT * FROM category WHERE COMPANY_ID=${company_id}`
    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(cities, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.cities = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(expenses, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.expenses = results;
                parallel_done();
            });
        }
    ], function(err) {
       if(err) console.log(err);
//         response.send(return_data.categories);
        response.render('./reports/all.twig',{'banks':return_data.banks,'customers':return_data.customers,'products':return_data.products,'expenses':return_data.expenses,'categories':return_data.categories,'cities':return_data.cities,'currencies':return_data.currencies});
    });


})




router.post('/cash_leger_report',(request,response)=>{

    var to_date=request.body.to_date;
    var currency_id=request.body.currency_id;
    var from_date=request.body.from_date;

    let opening_balance = `SELECT OPENING_CASH_CURRENCY(${currency_id},'${from_date}') as opening`;
    let closing_balance = `SELECT CLOSING_CASH_CURRENCY(${currency_id},'${to_date}') as closing`;
    let complete_reports=`SELECT * FROM cash_ledger WHERE voucher_date>='${from_date}' AND voucher_date<='${to_date}'  AND currency_id=${currency_id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(opening_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].opening;
                return_data.opening_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(closing_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].closing;
                return_data.closing_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(complete_reports, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.complete_reports = results;
                parallel_done();
            });
        },
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

//         console.log(return_data.opening_balance);
        response.render('./reports/cash_ledger_report.twig',{'complete_reports':return_data.complete_reports,'from_date':request.body.from_date,'session':session_values,'to_date':request.body.to_date,'closing_balance':return_data.closing_balance,'opening_balance':return_data.opening_balance});
    });
});


router.post('/currency_party_leger_report',(request,response)=>{
    var party_id=request.body.party_id;
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;
    var currency_id=request.body.currency_id;

    console.log("party id :" + party_id + " from:"+ from_date + "  TO:" + to_date + " Currency:" + currency_id);

    let closing_balance = `SELECT F_CLOSING_BALANCE_CURRENCY(${party_id},'${to_date}',${currency_id}) as closing`;
    let opening_balance=`SELECT F_OPENING_BALANCE_CURRENCY(${party_id},'${from_date}',${currency_id}) as opening`;
    let complete_reports=`SELECT party_ledger.*,currency.currency_name FROM party_ledger LEFT JOIN currency on currency.currencyid=party_ledger.currency_id  WHERE party_ledger.voucher_date>='${from_date}' AND party_ledger.voucher_date<='${to_date}' AND party_ledger.party_id=${party_id} AND party_ledger.currency_id=${currency_id}`;
    let parties=`SELECT * FROM parties  WHERE PARTY_ID=${party_id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(closing_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].closing;
                return_data.closing_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(opening_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].opening;
                return_data.opening_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(complete_reports, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.complete_reports = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

//         console.log(return_data.complete_reports);
        response.render('./reports/currency_party_ledger_report.twig',{'complete_reports':return_data.complete_reports,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'closing_balance':return_data.closing_balance,'opening_balance':return_data.opening_balance,'parties':return_data.parties});
    });
});



//for Pending Hawalas//
router.get('/pending_hawalas',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT hawala.*,p1.name as from_customer_name,p2.name as to_customer_name,currency.currency_name FROM hawala LEFT JOIN parties p1 on hawala.from_customer=p1.PARTY_ID LEFT JOIN parties p2 on hawala.to_customer=p2.PARTY_ID LEFT JOIN currency on hawala.currency_id=currency.currencyid where hawala.TYPE='RECEIVE' AND hawala.COMPANY_ID=${company_id} And hawala.status='pending' ORDER BY hawala.hawala_id DESC`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/pending_hawalas.twig',{'hawalas':rows,'session':session_values});

    });
});
//end of Pending Hawalas//

//start of demage report

router.post('/demage_report',(request,response)=>{
    var party_id=request.body.party_id;
    var product_id=request.body.product_id;
    var company_id=request.session.COMPANY_ID;

    var query=`select damages_master.*,products.NAME as product_name,damages_child.id as child_id  from damages_master  LEFT JOIN damages_child on damages_child.damages_master_id=damages_master.id LEFT JOIN products on products.PRODUCT_ID=damages_child.product_id  where  damages_master.company_id=${company_id} AND damages_master.invoice_date >= '${request.body.from_date}' AND damages_master.invoice_date <= '${request.body.to_date}' `;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


//        response.send(rows);
        response.render('./reports/damages.twig',{'damages':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });

});
//end of demage report





router.post('/cash_leger_report_simple',(request,response)=>{

    var to_date=request.body.to_date;
    var currency_id=request.body.currency_id;
    var from_date=request.body.from_date;
    var company_id=request.session.COMPANY_ID;

    let opening_balance = `select * from accounts where HEAD_ID=1 AND COMPANY_ID=${company_id}`;
    let cr_db = `SELECT IFNULL(SUM(credit),0) as credit_sum, IFNULL(SUM(debit),0) as debit_sum FROM cash_ledger WHERE COMPANY_ID=${company_id} AND voucher_date < '${from_date}'`;
    let complete_reports=`SELECT * FROM cash_ledger WHERE voucher_date>='${from_date}' AND voucher_date<='${to_date}' AND COMPANY_ID=${company_id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(opening_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].OPENING_BALANCE;
                return_data.opening_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(cr_db, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.cr_db = results;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(complete_reports, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.complete_reports = results;
                parallel_done();
            });
        },
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
        var opening=return_data.opening_balance;
        var credit=return_data.cr_db[0].credit_sum;
        var debit=return_data.cr_db[0].debit_sum;
        var cash_in_hand=(opening)-(credit)+(debit);


        response.render('./reports/cash_ledger_report_simple.twig',{'complete_reports':return_data.complete_reports,'from_date':request.body.from_date,'session':session_values,'to_date':request.body.to_date,'cash_in_hand':cash_in_hand});
    });
});


router.post('/bank_deposit',(request,response)=>{
       var company_id=request.session.COMPANY_ID;
       mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id} AND vouchers.ACCOUNT_ID=${request.body.account_id} AND vouchers.type='BDV'`,(err,rows)=>{
          if(err) console.log(err);
           var session_values={
               name:request.session.SHORT_NAME,
               email:request.session.EMAIL,
               contact:request.session.CONTACT,
               logo:request.session.LOGO_LARGE
           }

           response.render('./reports/bank_deposit.twig',{'bank_deposits':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
       });
});


router.post('/bank_withdraw',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id} AND vouchers.ACCOUNT_ID=${request.body.account_id} AND vouchers.type='BWV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

        response.render('./reports/bank_withdraw.twig',{'bank_withdraws':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/production_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select receives_master.*, receives_child.receive_child_id as child_id, products.NAME as product_name,GROUP_CONCAT(products.NAME,  ' ( ', receives_child.quantity, ' ) ' '<br>') as product_details from receives_master LEFT JOIN receives_child on receives_child.receive_master_id=receives_master.receive_id LEFT JOIN products on products.PRODUCT_ID=receives_child.product_id where receives_master.company_id=${company_id} AND receives_master.date >= '${request.body.from_date}' AND receives_master.date <= '${request.body.to_date}' GROUP by receives_master.receive_id`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/production_report.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


//for categorywise sale report
//for categorywise sale report
router.post('/categorywise_sale_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var party_id=request.body.party_id;
    var category_id=request.body.category_id;

    var query=`Select ifnull(sum(quantity),0) as Total_Qty,sale_child.price,sale_child.total, products.NAME,parties.NAME as party_name from sale_child Left join sale_master on sale_master.id=sale_child.sale_master_id  Left join parties on parties.PARTY_ID=sale_master.booker_id Left join products on products.PRODUCT_ID=sale_child.product_id Where sale_master.invoice_date between '${request.body.from_date}' AND '${request.body.to_date}' And sale_child.category_id=${request.body.category_id} `;

    if(party_id!=0){
        var query2=` AND sale_master.booker_id=${request.body.party_id}`
        query=query+query2;
    }

    query2=` Group by sale_child.product_id `;
    query=query+query2;

//    response.send(query);

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/categorywise_sale_report.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'party_id':party_id});
    });
});
//end of categorywise sale report


router.post('/cash_receive',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='CRV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/cash_receive.twig',{'cash_receives':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/cash_payment',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='CPV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/cash_payment.twig',{'cash_payments':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/income_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='INC'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/income_report.twig',{'income_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/price_list_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var category_id=request.body.category_id;
    var query=`select * from products where COMPANY_ID=${company_id} `;
    if(category_id!=0){
        var query2=` AND CATEGORY_ID=${request.body.category_id}`
        query=query+query2;
    }
    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);

        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
        response.render('./reports/price_list_report.twig',{'results':rows,'session':session_values});
    });
});

router.get('/customer_list_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select * from parties where COMPANY_ID=${company_id} AND type='C' AND status='Y'`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

      response.render('./reports/customer_list_report.twig',{'results':rows,'session':session_values});

    })
});

router.post('/expense_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='EX'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/expense_report.twig',{'expense_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});

router.post('/expense_headwise',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var account_id=request.body.account_id;

    var query=`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='EX' AND vouchers.ACCOUNT_ID=${account_id}`;
    var headname=`select NAME FROM accounts where ACCOUNT_ID=${account_id}`;



    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(query, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.results = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(headname, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].NAME;

                return_data.headname = x;
                parallel_done();
            });
        },

    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


        response.render('./reports/expense_headwise.twig',{'complete_reports':return_data.results,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'headname':return_data.headname});
    });



});

router.post('/purchase_return_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select purchase_master.*,parties.name as p_name from purchase_master LEFT JOIN parties on purchase_master.customer_id=parties.PARTY_ID where purchase_master.type='R' AND purchase_master.company_id=${company_id} AND purchase_master.invoice_date >= '${request.body.from_date}' AND purchase_master.invoice_date <= '${request.body.to_date}'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/purchase_return_report.twig',{'returns':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/purchase_invoice_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select purchase_master.*,parties.name as p_name,GROUP_CONCAT(products.NAME, ' ' , purchase_child.cost_price , ' * ', purchase_child.quantity, ' = ', purchase_child.total separator '<br>') as product_details from purchase_master LEFT JOIN purchase_child on purchase_child.purchase_master_id=purchase_master.id LEFT JOIN products on products.PRODUCT_ID=purchase_child.product_id LEFT JOIN parties on purchase_master.customer_id=parties.PARTY_ID where purchase_master.type='P' AND purchase_master.company_id=${company_id} AND purchase_master.invoice_date >= '${request.body.from_date}' AND purchase_master.invoice_date <= '${request.body.to_date}' GROUP by purchase_master.id ORDER BY  purchase_master.invoice_date ASC`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/purchase_invoice_report.twig',{'purchase_invoices':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/sale_return_report',(request,response)=>{
    var party_id=request.body.party_id;
    var product_id=request.body.product_id;
    var company_id=request.session.COMPANY_ID;

    var query=`select sale_master.*,parties.name as p_name, sale_child.id as child_id, products.NAME as product_name,GROUP_CONCAT(products.NAME, ' ' , sale_child.price separator '\n') as product_details  from sale_master LEFT JOIN parties on sale_master.customer_id=parties.PARTY_ID LEFT JOIN sale_child on sale_child.sale_master_id=sale_master.id LEFT JOIN products on products.PRODUCT_ID=sale_child.product_id  where sale_master.type='R' AND sale_master.company_id=${company_id} AND sale_master.invoice_date >= '${request.body.from_date}' AND sale_master.invoice_date <= '${request.body.to_date}' `;
    if(party_id!=0){
        var query2=` AND sale_master.customer_id=${request.body.party_id}`
        query=query+query2;
    }
    if(product_id!=0){
        var query2=` AND sale_child.product_id=${request.body.product_id}`
        query=query+query2;
    }
    query2=` GROUP by sale_master.id`;
    query=query+query2;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/sale_return_report.twig',{'sale_returns':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/monthly_sale_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT IFNULL(SUM(net_amount),0) as net_amount,invoice_number,invoice_date from sale_master WHERE sale_master.type='S' AND sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}' AND company_id=${company_id} GROUP BY invoice_number,invoice_date`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/monthly_sale_report.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});

router.post('/sale_invoice_report',(request,response)=>{
    var party_id=request.body.party_id;
    var product_id=request.body.product_id;
    var company_id=request.session.COMPANY_ID;

    var query=`select sale_master.*,parties.name as p_name, sale_child.id as child_id, products.NAME as product_name,GROUP_CONCAT(products.NAME, ' ' , sale_child.price, ' * ', sale_child.quantity, ' = ',  sale_child.total   separator '<br>') as product_details  from sale_master LEFT JOIN parties on sale_master.customer_id=parties.PARTY_ID LEFT JOIN sale_child on sale_child.sale_master_id=sale_master.id LEFT JOIN products on products.PRODUCT_ID=sale_child.product_id  where sale_master.type='S' AND sale_master.company_id=${company_id} AND sale_master.invoice_date >= '${request.body.from_date}' AND sale_master.invoice_date <= '${request.body.to_date}' `;
    if(party_id!=0){
        var query2=` AND sale_master.customer_id=${request.body.party_id}`
        query=query+query2;
    }
    if(product_id!=0){
        var query2=` AND sale_child.product_id=${request.body.product_id}`
        query=query+query2;
    }
    query2=` GROUP by sale_master.id ORDER BY  sale_master.invoice_date ASC`;
    query=query+query2;


    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


//        response.send(rows);
        response.render('./reports/sale_invoice_report.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



router.post('/expense_complete_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT vouchers.*,sum(AMOUNT) as ammount,accounts.Name as account_name FROM vouchers LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id} AND vouchers.type='EX' GROUP BY ACCOUNT_ID`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/expense_complete_report.twig',{'expense_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});



//for receivable report//
router.get('/receivable_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(OPENING_BALANCE,0)+IFNULL(view_rec2.debate,0)-IFNULL(view_rec2.credit,0) as BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=${company_id} AND parties.TYPE='C'`,(err,rows)=>{
      var session_values={
          name:request.session.SHORT_NAME,
          email:request.session.EMAIL,
          contact:request.session.CONTACT,
          logo:request.session.LOGO_LARGE
      }
//      response.send(rows)
      response.render('./reports/receivable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

  });
});


router.get('/hawala_receivable_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(party_opening.opening_balance,0) as open, IFNULL(SUM(party_ledger.credit),0) as cr, IFNULL(SUM(party_ledger.debit),0) as dr, IFNULL(SUM(party_opening.opening_balance),0) - IFNULL(SUM(party_ledger.credit),0) + IFNULL(SUM(party_ledger.debit),0) as balance, currency.currency_name, party_opening.currency_id FROM party_opening LEFT JOIN party_ledger on party_ledger.party_id=party_opening.party_id AND party_ledger.currency_id=party_opening.currency_id and party_ledger.company_id=party_opening.company_id LEFT JOIN currency ON currency.currencyid=party_opening.currency_id WHERE party_opening.COMPANY_ID=${company_id} GROUP BY party_opening.currency_id,currency.currency_name`,(err,rows)=>{
       if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/hawala_receivable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });
});


router.get('/hawala_payable_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(party_opening.opening_balance,0) as open, IFNULL(SUM(party_ledger.credit),0) as cr, IFNULL(SUM(party_ledger.debit),0) as dr, IFNULL(SUM(party_opening.opening_balance),0) - IFNULL(SUM(party_ledger.credit),0) + IFNULL(SUM(party_ledger.debit),0) as balance, currency.currency_name, party_opening.currency_id FROM party_opening LEFT JOIN party_ledger on party_ledger.party_id=party_opening.party_id AND party_ledger.currency_id=party_opening.currency_id and party_ledger.company_id=party_opening.company_id LEFT JOIN currency ON currency.currencyid=party_opening.currency_id WHERE party_opening.COMPANY_ID=${company_id} GROUP BY party_opening.currency_id,currency.currency_name`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/hawala_payable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });
});

//end of reveabile//

router.get('/hawala_cash_in_hand',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(opening_balance,0)+IFNULL(view_cash_open.debate,0)-
IFNULL(view_cash_open.credit,0) as BALANCE,
currency_name FROM currency LEFT
JOIN view_cash_open ON view_cash_open.currency_id=currency.currencyid WHERE
currency.COMPANY_ID=${company_id} And currency.status='Y'`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/hawala_cash_in_hand.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });
});
//end of reveabile//



router.post('/city_receivable',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(OPENING_BALANCE,0)+IFNULL(view_rec2.debate,0)-IFNULL(view_rec2.credit,0) as BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=${company_id} AND parties.TYPE='C' and parties.city_id=${request.body.city_id}`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/receivable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });
});



//for stock report//
router.get('/stock_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT DISTINCT pro.product_id,pro.COST_PRICE,category.CATEGORY_ID,category.NAME as cat_name,pro.NAME,pro.weight,IFNULL(pro.OPENING_BALANCE,0) as OPEN,

(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE' AND product_id=pro.product_id AND company_id=${company_id}) as SALE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as SALE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='DEMAGE' AND product_id=pro.product_id AND company_id=${company_id}) as DEMAGE,
(SELECT IFNULL(SUM(quantity),0)+ IFNULL(OPEN,0) FROM product_ledger WHERE  product_id=pro.product_id AND company_id=${company_id} AND type <> 'OPENING')  as BALANCE
from products as pro
LEFT JOIN category ON
category.CATEGORY_ID=pro.CATEGORY_ID
WHERE pro.company_id=${company_id} ORDER BY category.CATEGORY_ID`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/stock_report.twig',{'stocks':rows,'session':session_values});

    });
});

router.get('/mehndi_stock_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT DISTINCT pro.product_id,pro.COST_PRICE,category.CATEGORY_ID,category.NAME as cat_name,pro.NAME,pro.weight,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='OPENING' AND product_id=pro.product_id AND company_id=${company_id}) as OPEN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE' AND product_id=pro.product_id AND company_id=${company_id}) as SALE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as SALE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='DEMAGE' AND product_id=pro.product_id AND company_id=${company_id}) as DEMAGE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='RECEIVE' AND product_id=pro.product_id AND company_id=${company_id}) as RECEIVE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='ISSUE' AND product_id=pro.product_id AND company_id=${company_id}) as ISSUE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE  product_id=pro.product_id AND company_id=${company_id}) as BALANCE
from products as pro
LEFT JOIN category ON
category.CATEGORY_ID=pro.CATEGORY_ID
WHERE pro.company_id=${company_id} ORDER BY category.CATEGORY_ID`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/mehndi_stock_report.twig',{'stocks':rows,'session':session_values});

    });
});


router.get('/sale_missing_invoice_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT sale_master.*,parties.NAME as p_name FROM sale_master LEFT JOIN parties ON parties.PARTY_ID=sale_master.customer_id WHERE sale_master.company_id=${company_id} AND sale_master.invoice_date >= '2020-02-01' AND sale_master.payment_type='credit' AND sale_master.type='S' and sale_master.invoice_number NOT IN(SELECT voucher_no FROM party_ledger WHERE type='SALE' and company_id=${company_id})`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/sale_missing_invoice_report.twig',{'sales':rows,'session':session_values});

    });

});

router.get('/simple_stock_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT DISTINCT pro.product_id,pro.COST_PRICE,category.CATEGORY_ID,category.NAME as cat_name,pro.NAME,pro.weight,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='OPENING' AND product_id=pro.product_id AND company_id=${company_id}) as OPEN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE' AND product_id=pro.product_id AND company_id=${company_id}) as SALE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as SALE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='DEMAGE' AND product_id=pro.product_id AND company_id=${company_id}) as DEMAGE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE  product_id=pro.product_id AND company_id=${company_id}) as BALANCE
from products as pro
LEFT JOIN category ON
category.CATEGORY_ID=pro.CATEGORY_ID
WHERE pro.company_id=${company_id} ORDER BY category.CATEGORY_ID`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/simple_stock_report.twig',{'stocks':rows,'session':session_values});

    });
});

//end of stock//




//for categorywise profit report//
//for categorywise profit report//
router.post('/categorywise_profit',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var category_id=request.body.category_id;

    var query=`SELECT products.NAME as product_name, sale_master.invoice_date,sale_master.invoice_number,sale_child.weight,sale_child.product_id,sale_child.quantity,sale_child.cost_price,price,IFNULL(sale_child.quantity,0)*IFNULL(sale_child.cost_price,0) as total_cost,IFNULL(sale_child.quantity,0)*IFNULL(sale_child.price,0) as total_sale_price,IFNULL(sale_child.quantity,0)*IFNULL(sale_child.price,0) - IFNULL(sale_child.quantity,0)*IFNULL(sale_child.cost_price,0) as profit FROM sale_child LEFT JOIN sale_master on sale_master.id = sale_child.sale_master_id LEFT JOIN products on products.PRODUCT_ID = sale_child.product_id WHERE sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}' AND sale_master.company_id=${company_id} AND sale_master.type='S' `;


    if(category_id!=0){
        var query2=` AND sale_child.category_id=${request.body.category_id}`
        query=query+query2;
    }



    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/categorywise_profit.twig',{'sales':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});

//end of categorywise profit report//



//for categorywise stock report//
router.post('/categorywise_stock',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT DISTINCT pro.product_id,pro.COST_PRICE,category.CATEGORY_ID,category.NAME as cat_name,pro.NAME,pro.weight,IFNULL(pro.OPENING_BALANCE,0) as OPEN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE' AND product_id=pro.product_id AND company_id=${company_id}) as SALE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='SALE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as SALE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='PURCHASE_RETURN' AND product_id=pro.product_id AND company_id=${company_id}) as PURCHASE_RETURN,
(SELECT IFNULL(SUM(quantity),0) FROM product_ledger WHERE type='DEMAGE' AND product_id=pro.product_id AND company_id=${company_id}) as DEMAGE,
(SELECT IFNULL(SUM(quantity),0)+OPEN FROM product_ledger WHERE  product_id=pro.product_id AND company_id=${company_id} AND type <> 'OPENING') as BALANCE
from products as pro
LEFT JOIN category ON
category.CATEGORY_ID=pro.CATEGORY_ID
WHERE pro.company_id=${company_id} and category.CATEGORY_ID=${request.body.category_id} ORDER BY category.CATEGORY_ID`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/categorywise_stock_report.twig',{'stocks':rows,'session':session_values});

    });
});
//end of categorywise stock//

//for payablae report//
router.get('/payable_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT IFNULL(OPENING_BALANCE,0)+IFNULL(view_rec2.debate,0)-IFNULL(view_rec2.credit,0) as BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=${company_id} AND parties.TYPE='V'`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/payable_report.twig',{'payable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });
});
//end of reveabile//



router.get('/receivable_report2',(request,response)=>{
    mysqlconnection.query(`SELECT OPENING_BALANCE-view_rec2.credit+view_rec2.debate AS BALANCE,NAME FROM parties LEFT JOIN view_rec2 ON view_rec2.party_id=parties.PARTY_ID WHERE parties.COMPANY_ID=33 AND parties.TYPE='C'`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
        response.send(rows)
//      response.render('./reports/receivable_report.twig',{'receivable_reports':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    })
});


//profit and loss report//
router.post('/profit_and_loss',(request,response)=>{
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;
    var company_id=request.session.COMPANY_ID;


    var sales=`select IFNULL(sum(net_amount),0) as sales from sale_master WHERE type='S' AND invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'  AND company_id=${company_id}`;
    var sale_returns=`select IFNULL(sum(net_amount),0) as returns from sale_master WHERE type='R' AND invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'  AND company_id=${company_id}`;
    var expense=`SELECT IFNULL(SUM(AMOUNT),0) as expense FROM vouchers WHERE TYPE='EX' AND VOUCHER_DATE BETWEEN '${request.body.from_date}' AND '${request.body.to_date}' AND company_id=${company_id}`;
    var salecostprice=`select IFNULL(SUM(IFNULL(cost_price,0)*IFNULL(quantity,0)),0) as salecostprice from sale_child LEFT JOIN sale_master ON sale_master.id = sale_child.sale_master_id WHERE sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'AND company_id=${company_id}   AND sale_master.type='S'`;
    var returncostprice=`select IFNULL(SUM(IFNULL(cost_price,0)*IFNULL(quantity,0)),0) as returncostprice from sale_child LEFT JOIN sale_master ON sale_master.id = sale_child.sale_master_id WHERE sale_master.invoice_date BETWEEN '${request.body.from_date}' AND '${request.body.to_date}'AND company_id=${company_id} AND sale_master.type='R'`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sales, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].sales;
                return_data.sales = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(salecostprice, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].salecostprice;
                return_data.salecostprice = x;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].expense;
                return_data.expense = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(returncostprice, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].returncostprice;
                return_data.returncostprice = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_returns, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].returns;
                return_data.returns = x;
                parallel_done();
            });
        }

    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

           console.log(return_data.expense);

        response.render('./reports/profit_and_loss.twig',{'sales':return_data.sales,'returns':return_data.returns,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'expense':return_data.expense,'salecostprice':return_data.salecostprice,'returncostprice':return_data.returncostprice});
    });

});
//end of profit and loss report//


//profit loss report//
router.post('/profit_loss',(request,response)=>{
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;
    var company_id=request.session.COMPANY_ID;


   var sales=`SELECT sum(net_amount) AS sales from sale_master WHERE company_id=${company_id}  AND invoice_date between '${request.body.from_date}' AND '${request.body.to_date}'`;
   var expense=`SELECT IFNULL(SUM(amount),0) as expense from vouchers WHERE company_id=${company_id} and type='EX' AND voucher_date between '${request.body.from_date}' AND '${request.body.to_date}'`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sales, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].sales;
                return_data.total_sale = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].expense;
                return_data.expense = x;
                parallel_done();
            });
        }

    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }



        response.render('./reports/profit_loss.twig',{'total_sale':return_data.total_sale,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'expense':return_data.expense,'opening_balance':return_data.opening_balance});
    });

});
//end of profit loss report//



//for party_leger_report//
router.post('/party_leger_report',(request,response)=>{
    var party_id=request.body.party_id;
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;



    let closing_balance = `SELECT F_CLOSING_BALANCE2(${party_id},'${to_date}') as closing`;
    let opening_balance=`SELECT F_OPENING_BALANCE2(${party_id},'${from_date}') as opening`;
    let complete_reports=`SELECT * FROM party_ledger WHERE voucher_date>='${from_date}' AND voucher_date<='${to_date}' AND party_id=${party_id}`;
    // let complete_reports=`SELECT * FROM party_ledger WHERE voucher_date>='2018-10-23' AND voucher_date<='2019-10-23' AND party_id=681`;
    
    let parties=`SELECT * FROM parties WHERE PARTY_ID=${party_id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(closing_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].closing;
                return_data.closing_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(opening_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].opening;
                return_data.opening_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(complete_reports, {}, function(err, results) {
                if (err) return parallel_done(err);

                return_data.complete_reports = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


        response.render('./reports/party_ledger_report.twig',{'complete_reports':return_data.complete_reports,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'closing_balance':return_data.closing_balance,'opening_balance':return_data.opening_balance,'parties':return_data.parties});
    });
});

//end of party_leger_report//




//for bank_leger_report//
router.post('/bank_leger_report',(request,response)=>{
    var account_id=request.body.account_id;
    var from_date=request.body.from_date;
    var to_date=request.body.to_date;



    let closing_balance = `SELECT F_BANK_CLOSING2(${account_id},'${to_date}') as closing`;
    let opening_balance=`SELECT F_BANK_OPENING2(${account_id},'${from_date}') as opening`;
    let complete_reports=`SELECT * FROM bank_ledger WHERE voucher_date>='${from_date}' AND voucher_date<='${to_date}' AND account_id=${account_id}`;
    let banks=`SELECT * FROM accounts WHERE ACCOUNT_ID=${account_id}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(closing_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].closing;
                return_data.closing_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(opening_balance, {}, function(err, results) {
                if (err) return parallel_done(err);
                var x=results[0].opening;
                return_data.opening_balance = x;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(complete_reports, {}, function(err, results) {
                if (err) return parallel_done(err);

                return_data.complete_reports = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }
    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }



        response.render('./reports/bank_ledger_report.twig',{'complete_reports':return_data.complete_reports,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date,'closing_balance':return_data.closing_balance,'opening_balance':return_data.opening_balance,'banks':return_data.banks});
    });
});

//end of bank_leger_report//

router.get('/try',(request,response)=>{
    var events = [{}];
    var cart_values={
        name:request.body.SHORT_NAME,
        email:request.session.EMAIL,
        contact:request.session.CONTACT,
        logo:request.session.LOGO_LARGE
    }
    events[0][1]=cart_values
    events[0][2]=cart_values

    response.send(events);


})


router.post('/hawala_send_report',(request,response)=>{

    var company_id=request.session.COMPANY_ID;

    var query=`SELECT hawala.*,p1.name as from_customer_name,p2.name as to_customer_name,p2.ADDRESS,currency.currency_name,currency_commision.currency_name as currency_commision,conversion_currency_commision.currency_name as conversion_currency_commision FROM hawala LEFT JOIN parties p1 on hawala.from_customer=p1.PARTY_ID LEFT JOIN parties p2 on hawala.to_customer=p2.PARTY_ID LEFT JOIN currency on hawala.currency_id=currency.currencyid LEFT JOIN currency currency_commision on hawala.commission_currency_id=currency_commision.currencyid LEFT JOIN currency conversion_currency_commision on hawala.conversion_currency_id=conversion_currency_commision.currencyid where hawala.TYPE='SEND' AND hawala.COMPANY_ID=${company_id} AND hawala.hawala_date >= '${request.body.from_date}' AND hawala.hawala_date <= '${request.body.to_date}' ORDER BY hawala.hawala_id DESC `;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


//        response.send(rows);
        response.render('./reports/hawala_send_report.twig',{'hawalas':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });

});

router.post('/hawala_cash_receive',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name,currency.currency_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID LEFT JOIN currency on currency.currencyid=vouchers.currency_id WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='CRV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/hawala_cash_receive.twig',{'cash_receives':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});

router.post('/hawala_cash_payment',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT vouchers.*,parties.NAME as party_name,accounts.Name as account_name,currency.currency_name FROM vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID LEFT JOIN currency on currency.currencyid=vouchers.currency_id WHERE vouchers.VOUCHER_DATE >= '${request.body.from_date}' AND vouchers.VOUCHER_DATE <= '${request.body.to_date}' AND vouchers.COMPANY_ID=${company_id}  AND vouchers.type='CPV'`,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./reports/hawala_cash_payment.twig',{'cash_payments':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });
});


router.post('/hawala_receive_report',(request,response)=>{

    var company_id=request.session.COMPANY_ID;

    var query=`SELECT hawala.*,p1.name as from_customer_name,p2.name as to_customer_name,p2.ADDRESS,currency.currency_name,currency_commision.currency_name as currency_commision,conversion_currency_commision.currency_name as conversion_currency_commision FROM hawala LEFT JOIN parties p1 on hawala.from_customer=p1.PARTY_ID LEFT JOIN parties p2 on hawala.to_customer=p2.PARTY_ID LEFT JOIN currency on hawala.currency_id=currency.currencyid LEFT JOIN currency currency_commision on hawala.commission_currency_id=currency_commision.currencyid LEFT JOIN currency conversion_currency_commision on hawala.conversion_currency_id=conversion_currency_commision.currencyid where hawala.TYPE='RECEIVE' AND hawala.COMPANY_ID=${company_id} AND hawala.hawala_date >= '${request.body.from_date}' AND hawala.hawala_date <= '${request.body.to_date}' ORDER BY hawala.hawala_id DESC `;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }


//        response.send(rows);
        response.render('./reports/hawala_receive_report.twig',{'hawalas':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });

});

router.post('/hawala_sale_report',(request,response)=>{

    var company_id=request.session.COMPANY_ID;

    var query=`SELECT currency_transaction.*,c1.currency_name as from_currency,c2.currency_name as to_currency FROM currency_transaction LEFT JOIN currency c1 on c1.currencyid=currency_transaction.from_currency_id LEFT JOIN currency c2 on c2.currencyid=currency_transaction.to_currency_id where currency_transaction.TYPE='CURRENCY_SALE' AND currency_transaction.COMPANY_ID=${company_id} AND currency_transaction.date >= '${request.body.from_date}' AND currency_transaction.date <= '${request.body.to_date}' ORDER BY currency_transaction.transaction_id DESC `;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);

        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

//        response.send(rows);
        response.render('./reports/hawala_sale_report.twig',{'hawalas':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });

});


router.post('/hawala_purchase_report',(request,response)=>{

    var company_id=request.session.COMPANY_ID;

    var query=`SELECT currency_transaction.*,c1.currency_name as from_currency,c2.currency_name as to_currency FROM currency_transaction LEFT JOIN currency c1 on c1.currencyid=currency_transaction.from_currency_id LEFT JOIN currency c2 on c2.currencyid=currency_transaction.to_currency_id where currency_transaction.TYPE='CURRENCY_PURCHASE' AND currency_transaction.COMPANY_ID=${company_id} AND currency_transaction.date >= '${request.body.from_date}' AND currency_transaction.date <= '${request.body.to_date}' ORDER BY currency_transaction.transaction_id DESC `;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);

        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

//        response.send(rows);
        response.render('./reports/hawala_purchase_report.twig',{'hawalas':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });

});



router.get('/floor_list_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select * from floor where COMPANY_ID=${company_id}`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/floor_list_report.twig',{'results':rows,'session':session_values});

    })
});

router.post('/floorwise_report',(request,response)=>{
    var floor_id=request.body.floor_id;
    var company_id=request.session.COMPANY_ID;

    var query=`SELECT plot_master.*,floor.floor_name FROM plot_master LEFT JOIN floor ON floor.floor_id=plot_master.floor_id WHERE plot_master.floor_id=${floor_id}`;

    mysqlconnection.query(query,(err,rows)=>{
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//        response.send(rows);
        response.render('./reports/floorwise_report.twig',{'results':rows,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});
    });

});

router.get('/floorwise_complete_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`select * from floor where COMPANY_ID=${company_id}`,(err,rows)=>{
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//      response.send(rows)
        response.render('./reports/floorwise_complete_report.twig',{'results':rows,'session':session_values});

    })
});

router.get('/get_shops/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`SELECT plot_master.*,projects.project_name,floor.floor_name,plotsizes.size_name FROM plot_master LEFT JOIN projects on projects.project_id=plot_master.project_id LEFT JOIN floor on floor.floor_id=plot_master.floor_id LEFT JOIN plotsizes on plotsizes.size_id=plot_master.size_id WHERE plot_master.company_id=${company_id} and plot_master.status='B' and plot_master.active_flag='Y' and plot_master.floor_id=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});



router.post('/detail_stock_report',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var product_details=`SELECT * FROM product_ledger WHERE product_id=${request.body.product_id} AND transection_date between '${request.body.from_date}' AND '${request.body.to_date}'`;
    var product=`SELECT * from products where PRODUCT_ID=${request.body.product_id}`;
    var quantity_in_hind=`SELECT IFNULL(SUM(quantity),0) as quantity_in_hand FROM product_ledger WHERE product_id=${request.body.product_id} AND transection_date < '${request.body.from_date}'`;

        var return_data = {};

        async.parallel([
            function(parallel_done) {
                mysqlconnection.query(product_details, {}, function(err, results) {
                    if (err) return parallel_done(err);
                    return_data.product_details = results;
                    parallel_done();
                });
            },
            function(parallel_done) {
                mysqlconnection.query(product, {}, function(err, results) {
                    if (err) return parallel_done(err);
                    return_data.product = results;
                    parallel_done();
                });
            },
            function(parallel_done) {
                mysqlconnection.query(quantity_in_hind, {}, function(err, results) {
                    if (err) return parallel_done(err);
                    return_data.quantity_in_hand = results;
                    parallel_done();
                });
            },

        ], function(err) {
            if(err) console.log(err);
            var session_values={
                name:request.session.SHORT_NAME,
                email:request.session.EMAIL,
                contact:request.session.CONTACT,
                logo:request.session.LOGO_LARGE
            }
            // response.send(return_data.product_details);
            response.render('./reports/detail_stock_report.twig',{'results':return_data.product_details,'quantity_in_hand':return_data.quantity_in_hand,'product':return_data.product,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

        });


});


router.get('/get_product_detail/:type?/:transection_id?/:product_id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var type=request.params.type;
    var product_id=request.params.product_id
    if(type=='SALE'){
        mysqlconnection.query(`SELECT product_ledger.*,sale_master.customer_id,parties.NAME FROM product_ledger LEFT JOIN sale_master on sale_master.invoice_number=product_ledger.transection_id LEFT JOIN parties ON parties.PARTY_ID=sale_master.customer_id WHERE product_ledger.product_id=${product_id} AND product_ledger.type='SALE' AND product_ledger.transection_id=${request.params.transection_id}`,(err,rows)=>{
            if(!err){
                response.json(rows);
            }
            else{
                response.send(err);
            }
        });
    }
    else if(type=='SALE_RETURN'){
        mysqlconnection.query(`SELECT product_ledger.*,sale_master.customer_id,parties.NAME FROM product_ledger LEFT JOIN sale_master on sale_master.invoice_number=product_ledger.transection_id LEFT JOIN parties ON parties.PARTY_ID=sale_master.customer_id WHERE product_ledger.product_id=${product_id} AND product_ledger.type='SALE_RETURN' AND product_ledger.transection_id=${request.params.transection_id}`,(err,rows)=>{
            if(!err){
                response.json(rows);
            }
            else{
                response.send(err);
            }
        });
    }
    else if(type=='PURCHASE'){
        mysqlconnection.query(`SELECT product_ledger.*,purchase_master.customer_id,parties.NAME FROM product_ledger LEFT JOIN purchase_master on purchase_master.invoice_number=product_ledger.transection_id LEFT JOIN parties ON parties.PARTY_ID=purchase_master.customer_id WHERE product_ledger.product_id=${product_id} AND product_ledger.type='PURCHASE' AND product_ledger.transection_id=${request.params.transection_id}`,(err,rows)=>{
            if(!err){
                response.json(rows);
            }
            else{
                response.send(err);
            }
        });
    }
    else if(type=='PURCHASE_RETURN'){
        mysqlconnection.query(`SELECT product_ledger.*,purchase_master.customer_id,parties.NAME FROM product_ledger LEFT JOIN purchase_master on purchase_master.invoice_number=product_ledger.transection_id LEFT JOIN parties ON parties.PARTY_ID=purchase_master.customer_id WHERE product_ledger.product_id=${product_id} AND product_ledger.type='PURCHASE_RETURN' AND product_ledger.transection_id=${request.params.transection_id}`,(err,rows)=>{
            if(!err){
                response.json(rows);
            }
            else{
                response.send(err);
            }
        });
    }

    else if(type=='ISSUE'){
        mysqlconnection.query(`SELECT product_ledger.* FROM product_ledger LEFT JOIN issues_master on issues_master.issue_number=product_ledger.transection_id WHERE product_ledger.product_id=${product_id} AND product_ledger.type='ISSUE' AND product_ledger.transection_id=${request.params.transection_id}`,(err,rows)=>{
            if(!err){
                response.json(rows);
            }
            else{
                response.send(err);
            }
        });
    }

    else if(type=='RECEIVE'){
        mysqlconnection.query(`SELECT product_ledger.* FROM product_ledger LEFT JOIN receives_master on receives_master.receive_number=product_ledger.transection_id WHERE product_ledger.product_id=${product_id} AND product_ledger.type='RECEIVE' AND product_ledger.transection_id=${request.params.transection_id}`,(err,rows)=>{
            if(!err){
                response.json(rows);
            }
            else{
                response.send(err);
            }
        });
    }
       
       else if(type=='DEMAGE'){
        mysqlconnection.query(`SELECT product_ledger.* FROM product_ledger LEFT JOIN damages_master on damages_master.invoice_number=product_ledger.transection_id WHERE product_ledger.product_id=${product_id} AND product_ledger.type='DEMAGE' AND product_ledger.transection_id=${request.params.transection_id}`,(err,rows)=>{
            if(!err){
                response.json(rows);
            }
            else{
                response.send(err);
            }
        });
    }




});


router.post('/detail_stock_report2',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var product_details=`SELECT product_ledger.*,sale_master.customer_id as s_customer_id,purchase_master.customer_id as p_customer_id,p1.NAME as s_name,p2.NAME as p_name FROM product_ledger LEFT JOIN sale_master on sale_master.invoice_number=product_ledger.transection_id AND sale_master.company_id=${company_id} AND sale_master.type='S' LEFT JOIN purchase_master ON purchase_master.invoice_number=product_ledger.transection_id AND purchase_master.company_id=${company_id} AND purchase_master.type='P' LEFT JOIN parties p1 ON p1.PARTY_ID=sale_master.customer_id LEFT JOIN parties p2 ON p2.PARTY_ID=purchase_master.customer_id WHERE product_id=${request.body.product_id}  AND product_ledger.transection_date between '${request.body.from_date}' AND '${request.body.to_date}' GROUP BY product_ledger.id ORDER BY product_ledger.transection_date ASC`;
    var product=`SELECT * from products where PRODUCT_ID=${request.body.product_id}`;
    var quantity_in_hind=`SELECT IFNULL(SUM(quantity),0) as quantity_in_hand FROM product_ledger WHERE product_id=${request.body.product_id} AND transection_date < '${request.body.from_date}'`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(product_details, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.product_details = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(product, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.product = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(quantity_in_hind, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.quantity_in_hand = results;
                parallel_done();
            });
        },

    ], function(err) {
        if(err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
            // response.send(return_data.product_details);
        response.render('./reports/detail_stock_report2.twig',{'results':return_data.product_details,'quantity_in_hand':return_data.quantity_in_hand,'product':return_data.product,'session':session_values,'from_date':request.body.from_date,'to_date':request.body.to_date});

    });


});



module.exports=router;