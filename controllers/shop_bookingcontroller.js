/**
 * Created by belawal on 11/26/20.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{

    var sale_id = request.session.sale_id;
    request.session.sale_id = null;

    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`SELECT shop_booking.*,projects.project_name,floor.floor_name,plot_master.name as shop_name,parties.NAME as customer_name FROM shop_booking LEFT JOIN projects on projects.project_id=shop_booking.project_id LEFT JOIN floor on floor.floor_id=shop_booking.floor_id LEFT JOIN plot_master on plot_master.plot_id=shop_booking.builder_master_id  LEFT JOIN parties on parties.PARTY_ID=shop_booking.customer_id and shop_booking.company_id=${company_id} ORDER BY shop_booking.booking_id DESC`,(err,rows)=>{
        if(err) response.send(err);
//      response.send(rows);
        response.render('./shop_booking/list.twig',{bookings:rows,'messages': request.flash('user')});
    });



});



router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y' and type='Developer'`;
    var floors = `select * from floor where COMPANY_ID=${company_id}`;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND status='Y' and registration_type='Developer'`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(floors, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.floors = results;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        }


    ], function(err) {
        if (err) console.log(err);
//        res.send(return_data.projects);
        res.render('./shop_booking/add.twig',{'projects':return_data.projects,'floors':return_data.floors,'customers':return_data.customers ,company_id:company_id});
    });
});



router.post('/store',(request,response)=>{
    customer_id=request.body.customer_id;

    var data={
        customer_id:request.body.customer_id,
        booking_number:request.body.booking_number,
        project_id:request.body.project_id,
        floor_id:request.body.floor_id,
        builder_master_id:request.body.builder_master_id,
        square_feet:request.body.square_feet,
        rate:request.body.rate,
        shop_total:request.body.shop_total,
        type:request.body.type,
        total:request.body.grand_total,
        down_payment:request.body.down_payment,
        no_of_installments:request.body.no_of_instalments,
        plan_date:request.body.plan_date,
        gap:request.body.gap,
        advance:request.body.advance,
        discount:request.body.discount,
        remaining:request.body.remaining,
        company_id:request.session.COMPANY_ID
    }

    mysqlconnection.query('insert into shop_booking set ?',data,(err,rows)=>{
     if(!err){
         var booking_id=rows.insertId;

         var monthly_instalment_no=request.body.monthly_instalment_no;
         var date=request.body.date;
         var amount=request.body.amount;

         for(var i=0;i<monthly_instalment_no.length;i++){
             var data={
                 shop_booking_id:booking_id,
                 instalment_no:monthly_instalment_no[i],
                 date:date[i],
                 amount:amount[i]

             }
             mysqlconnection.query('insert into shop_booking_detail set ?',data,(err,rows)=>{

             });
         }


         var data={
             voucher_no:request.body.booking_number,
             voucher_date:request.body.plan_date,
             type:"SHOP_BOOKING",
             party_id:customer_id,
             credit:0,
             COMPANY_ID:request.session.COMPANY_ID,
             debit:request.body.grand_total,
             remarks:'Shop/Appartment BOOKING ENTRY'
         }
         mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
             if(err) console.log(err)

         });

         if(request.body.advance==0){

         }else{
             var data={
                 voucher_no:request.body.booking_number,
                 voucher_date:request.body.plan_date,
                 type:"SHOP_BOOKING",
                 party_id:customer_id,
                 credit:request.body.advance,
                 COMPANY_ID:request.session.COMPANY_ID,
                 debit:0,
                 remarks:'BOOKING ADVANCE ENTRY'
             }
             mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                 if(err) console.log(err)

             });
         }

         request.flash('user', 'Data Successfully Inserted');
         response.redirect('/shop_booking/list')
     }
    });
   });


router.get('/get_shops/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select * from plot_master where  floor_id=${request.params.id}`,(err,rows)=>{
        if(!err){
            response.json(rows);
        }
        else{
            response.send(err);
        }
    });
});


router.get('/edit/:id?',(request,response)=>{
    var id=request.params.id;
    var company_id=request.session.COMPANY_ID;
    var shop_bookingmaster = `select shop_booking.*,plot_master.name as shop_name from shop_booking LEFT JOIN plot_master on plot_master.plot_id=shop_booking.builder_master_id where shop_booking.company_id=${company_id}  and shop_booking.booking_id=${id}`;
    var shop_bookingchild = `select shop_booking_detail.* from shop_booking_detail  where  shop_booking_detail.shop_booking_id=${id} Order By shop_booking_detail.date ASC`;

    var projects = `select * from projects where company_id=${company_id} AND active_flag='Y' and type='Developer'`;
    var floors = `select * from floor where COMPANY_ID=${company_id}`;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND status='Y' and registration_type='Developer'`;

    var return_data = {};


    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(projects, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.projects = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(shop_bookingmaster, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.bookingmaster = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(shop_bookingchild, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.bookingchild = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(floors, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.floors = results;
                parallel_done();
            });
        },

        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
//        response.send(return_data.bookingmaster);
        response.render('./shop_booking/edit.twig',{'projects':return_data.projects,'customers':return_data.customers , 'floors':return_data.floors, company_id:company_id,masters:return_data.bookingmaster,childs:return_data.bookingchild});
    });


});

router.post('/update',(request,response)=>{
    var booking_id=request.body.booking_id;
    var company_id=request.session.COMPANY_ID;
    customer_id=request.body.customer_id;

    var data={
        customer_id:request.body.customer_id,
        booking_number:request.body.booking_number,
        project_id:request.body.project_id,
        floor_id:request.body.floor_id,
        builder_master_id:request.body.builder_master_id,
        square_feet:request.body.square_feet,
        rate:request.body.rate,
        shop_total:request.body.shop_total,
        type:request.body.type,
        total:request.body.grand_total,
        down_payment:request.body.down_payment,
        no_of_installments:request.body.no_of_instalments,
        plan_date:request.body.plan_date,
        gap:request.body.gap,
        advance:request.body.advance,
        discount:request.body.discount,
        remaining:request.body.remaining,
        company_id:request.session.COMPANY_ID
    }

    mysqlconnection.query(`update shop_booking  set ? where booking_id=${booking_id}`,data,(err,rows)=>{

        mysqlconnection.query(`delete from shop_booking_detail where  shop_booking_id=${booking_id}`,()=>{
            var monthly_instalment_no=request.body.monthly_instalment_no;
            var date=request.body.date;
            var amount=request.body.amount;

            for(var i=0;i<monthly_instalment_no.length;i++){
                var data={
                    shop_booking_id:booking_id,
                    instalment_no:monthly_instalment_no[i],
                    date:date[i],
                    amount:amount[i]

                }
                mysqlconnection.query('insert into shop_booking_detail set ?',data,(err,rows)=>{

                });
            }
        });

        var voucher_no=request.body.booking_number;
        mysqlconnection.query(`delete from party_ledger where  voucher_no='${voucher_no}' AND type='SHOP_BOOKING'`,(err)=>{
            var data={
                voucher_no:request.body.booking_number,
                voucher_date:request.body.plan_date,
                type:"SHOP_BOOKING",
                party_id:customer_id,
                credit:0,
                COMPANY_ID:request.session.COMPANY_ID,
                debit:request.body.grand_total,
                remarks:'Shop/Appartment BOOKING ENTRY'
            }
            mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });

            if(request.body.advance==0){

            }else{
                var data={
                    voucher_no:request.body.booking_number,
                    voucher_date:request.body.plan_date,
                    type:"SHOP_BOOKING",
                    party_id:customer_id,
                    credit:request.body.advance,
                    COMPANY_ID:request.session.COMPANY_ID,
                    debit:0,
                    remarks:'BOOKING ADVANCE ENTRY'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }

        });
        request.flash('user', 'Data Successfully Updated');
        response.redirect('/shop_booking/list')
    });

});


router.get('/print/:id?',(request,response)=>{

    mysqlconnection.query(`SELECT shop_booking.*,floor.floor_name,plot_master.name as shop_name,plot_master.squarefeets as squarefeets,plot_master.rate as shop_rate,parties.*,projects.project_name,projects.project_logo FROM shop_booking LEFT JOIN floor ON floor.floor_id=shop_booking.floor_id LEFT JOIN parties ON parties.PARTY_ID=shop_booking.customer_id LEFT JOIN plot_master ON plot_master.plot_id=shop_booking.builder_master_id LEFT JOIN projects on projects.project_id=shop_booking.project_id WHERE shop_booking.booking_id=${request.params.id}`,(err,rows)=>{
        if(err) response.send(err);
//          response.send(rows);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//        response.send(rows);
        response.render('./shop_booking/print.twig',{bookings:rows,session:session_values});
    });




});

module.exports=router;