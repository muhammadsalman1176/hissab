/**
 * Created by belawal on 5/7/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select sale_master.*,f_d.name as from_destination_name,IFNULL(sum(vouchers.AMOUNT),0) as total_expense, t_d.name as to_destination_name,parties.name as p_name from sale_master LEFT JOIN parties on sale_master.customer_id=parties.PARTY_ID  LEFT JOIN destinations as t_d on sale_master.to_destination=t_d.id LEFT JOIN vouchers on sale_master.invoice_number=vouchers.trip_number  LEFT JOIN destinations as f_d on sale_master.from_destination=f_d.id where sale_master.type='T' AND sale_master.company_id=${company_id} GROUP BY sale_master.invoice_number ORDER BY sale_master.ID DESC`,(err,rows)=>{
        if(err) response.send(err);
//        response.send(rows);
        response.render('./trip/list.twig',{trips:rows});
    });

});


router.get('/add',(request,res)=>{
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var products = `select * from products where COMPANY_ID=${company_id}`;
    var sale_master=`select * from sale_master ORDER BY ID DESC limit 1`;
    var destinations=`select * from destinations where COMPANY_ID=${company_id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.invoice = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(destinations, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.destinations = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        var invoice_number=return_data.invoice[0].invoice_number+1;
        res.render('./trip/add.twig',{'customers':return_data.customers , 'categories':return_data.categories,'invoice_number':invoice_number,sale_id:sale_id, products:return_data.products, destinations:return_data.destinations});
    });
});



router.post('/store',(request,response,cb)=>{
    var company_id=request.session.COMPANY_ID;

    var data={
        invoice_number:request.body.invoice_number,
        invoice_date:request.body.invoice_date,
        payment_type:request.body.payment_type,
        customer_name:request.body.customer_name,
        invoice_time:request.body.time,
        customer_id:request.body.customer_id,
        gross_amount:request.body.gross_amount,
        discount:request.body.discount,
        from_destination:request.body.from_destination,
        to_destination:request.body.to_destination,
        pak_vehicle_number:request.body.pak_vehicle_number,
        net_amount:request.body.net_amount,
        internal_notes:request.body.internal_notes,
        external_notes:request.body.external_notes,
        type:'T',
        driver_name:request.body.driver_name,
        driver_contact:request.body.driver_contact,
        afghan_vehicle_number:request.body.afghan_vehicle_number,
        renter_name:request.body.renter_name,
        company_id:company_id
    }
    mysqlconnection.query('insert into sale_master set ?',data,(err,rows)=>{
        if(!err){
            var sale_master_id=rows.insertId;
            var categories=request.body.category_id;
            var product_id=request.body.product_id;
            var quantity=request.body.quantity;
            var price=request.body.price;
            var total=request.body.total;
            var cost_price=request.body.cost_price;
            for(var i=0;i<categories.length;i++){
                var data={
                    sale_master_id:sale_master_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:quantity[i],
                    price:price[i],
                    total:total[i],
                    cost_price:cost_price[i]

                }
                mysqlconnection.query('insert into sale_child set ?',data,(err,rows)=>{

                });
            }



            for(var i=0;i<categories.length;i++){
                var data={
                    transection_id:request.body.invoice_number,
                    transection_date:request.body.invoice_date,
                    company_id:company_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:-(quantity[i]),
                    cost_price:cost_price[i],
                    type:'SALE'
                }
                mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                    if(err) console.log(err);
                });
            }


            console.log("the master id is " +rows.insertId);
            request.session.sale_id=rows.insertId;


            if(request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment == "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            if (request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment != "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"CRV",
                    party_id:request.body.customer_id,
                    credit:request.body.payment,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'payment received on invoice number'+request.body.invoice_number
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            if (request.body.payment_type == "cash" && request.body.customer_id != "counter_sale") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:request.body.net_amount,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }

            if (request.body.payment_type == "cash") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'Cash Sale  on invoice number'+request.body.invoice_number + " on party id " + request.body.customer_id
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            response.redirect('/trip/list');
        }
        else{
            response.send(err)
        }
    });
});

router.get('/print/:id?/:trip?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var trip_number=request.params.trip;
    var sale_master=`select sale_master.*,f_d.name as from_destination_name, t_d.name as to_destination_name,parties.NAME as party_name,parties.CONTACT_NO,parties.EMAIL,parties.ADDRESS from sale_master  LEFT JOIN destinations as t_d on sale_master.to_destination=t_d.id  LEFT JOIN destinations as f_d on sale_master.from_destination=f_d.id LEFT JOIN parties on parties.PARTY_ID=sale_master.customer_id where sale_master.company_id=${company_id} And sale_master.id=${id}`;
    var sale_child=`select sale_child.*,category.NAME as category_name,products.NAME as product_name from sale_child LEFT JOIN category on category.CATEGORY_ID=sale_child.category_id LEFT JOIN products on products.PRODUCT_ID=sale_child.product_id   where  sale_child.sale_master_id=${id}`;
    var trip_expense=`SELECT  IFNULL(SUM(AMOUNT),0) as amount FROM vouchers WHERE trip_number=${trip_number}`;

    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.child = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(trip_expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.trip_expense = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
        response.render('./trip/print.twig',{'masters':return_data.master , 'childs':return_data.child,session:session_values,'trip_expense':return_data.trip_expense[0]['amount']});
    });


});



router.get('/expense/edit/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var expenses=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=3`;
    var record=`select * from vouchers where VOUCHER_ID=${req.params.id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(record, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.record = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(expenses, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.expenses = results;
                parallel_done();
            });
        }
    ], function(err) {
        res.render('./trip/expense_edit.twig',{record:return_data.record,expenses:return_data.expenses});
    });
});

router.post('/expense_update',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'EX',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        trip_number:request.body.trip_number,
        COMPANY_ID:company_id,
        ACCOUNT_ID:request.body.account_id,
        TIME:request.body.time
    }
    mysqlconnection.query(`update vouchers set ? Where VOUCHER_ID=${request.body.id}`, data,(err,rows)=>{
        if(err) console.log(err);

        var data={
            voucher_no:request.body.voucher_no,
            voucher_date:request.body.voucher_date,
            type:"EXPENSE",
            credit:request.body.amount,
            COMPANY_ID:company_id,
            debit:0,
            remarks:'EXPENSE  on VOUCHER number'+request.body.voucher_no
        }
        mysqlconnection.query(`update cash_ledger set ? where voucher_no='${request.body.voucher_no}' and COMPANY_ID=${company_id}`,data,(err,rows)=>{
            if(err) console.log(err)

        });

        response.redirect('/trip/list')
    });



});
router.get('/expense/:id?/:trip_number?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var id=request.params.id;
    var trip_number=request.params.trip_number;
    var sale_master=`select sale_master.*,f_d.name as from_destination_name, t_d.name as to_destination_name,parties.NAME as party_name,parties.CONTACT_NO,parties.EMAIL,parties.ADDRESS from sale_master  LEFT JOIN destinations as t_d on sale_master.to_destination=t_d.id  LEFT JOIN destinations as f_d on sale_master.from_destination=f_d.id LEFT JOIN parties on parties.PARTY_ID=sale_master.customer_id where sale_master.company_id=${company_id} And sale_master.id=${id}`;

    var vouchers=`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='EX' ORDER BY VOUCHER_ID DESC limit 1`;
    var expenses=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=3`
    var trip_expense=`SELECT sum(AMOUNT) as amount FROM vouchers WHERE trip_number=${trip_number}`;
    var trip_expense_detail=`select * from vouchers where trip_number=${trip_number}`;
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(vouchers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.vouchers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(expenses, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.expenses = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(trip_expense, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.trip_expense = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(trip_expense_detail, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.trip_expense_detail = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);

        if(return_data.vouchers.length>0){
            var the_string=return_data.vouchers[0].VOUCHER_NO;
            var parts = the_string.split('-', 2);
            var voucher_no  = parseInt(parts[1])+1;
            voucher_no='EXP-'+voucher_no;
        }
        else{
            voucher_no='EXP-'+1;
        }
//         console.log(return_data.trip_expense[0]['amount'])
//        response.send(return_data.trip_expense_detail);
        response.render('./trip/expense2.twig',{voucher_no:voucher_no,expenses:return_data.expenses,'masters':return_data.master,'trip_expense':return_data.trip_expense[0]['amount'] , 'trip_expense_details':return_data.trip_expense_detail});
    });



});


router.post('/expense/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var account_id=request.body.account_id;
    var amount=request.body.amount;
    var description=request.body.description;
    var voucher_no="";
    var vouchers=`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='EX' ORDER BY VOUCHER_ID DESC limit 1`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(vouchers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.vouchers = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        if(return_data.vouchers.length>0){
            var the_string=return_data.vouchers[0].VOUCHER_NO;
            var parts = the_string.split('-', 2);
            var voucher_no  = parseInt(parts[1])+1;
            voucher_no='EXP-'+voucher_no;
        }
        else{
            voucher_no='EXP-'+1;
        }

        for(var i=0;i<account_id.length;i++){
            var data={
                VOUCHER_NO:voucher_no,
                TYPE:'EX',
                AMOUNT:amount[i],
                VOUCHER_DATE:request.body.voucher_date,
                DESCRIPTION:description[i],
                MANUAL_NO:request.body.manual_no,
                trip_number:request.body.trip_number,
                COMPANY_ID:company_id,
                ACCOUNT_ID:account_id[i],
                TIME:request.body.time
            }
            mysqlconnection.query('insert into vouchers set ?', data,(err,rows)=>{
                if(!err) {
                    var data={
                        voucher_no:voucher_no,
                        voucher_date:request.body.voucher_date,
                        type:"EXPENSE",
                        credit:amount[i],
                        COMPANY_ID:company_id,
                        debit:0,
                        remarks:'EXPENSE  on VOUCHER number'+request.body.voucher_no
                    }
                    mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                    request.flash('cash_payment_success',rows.insertId);
                    response.redirect('/trip/list')
                }
                else{
                    console.log(err)
                }
            });

        }

    });




});

router.get('/edit/:id?',(request,response)=>{
    var id=request.params.id;
    var sale_id = request.session.sale_id;
    request.session.sale_id = null; // resets session variable

    var company_id=request.session.COMPANY_ID;
    var customers = `select * from parties where COMPANY_ID=${company_id} AND TYPE='C' AND status='Y'`;
    var categories = `select * from category where COMPANY_ID=${company_id}`;
    var products = `select * from products where COMPANY_ID=${company_id}`;
    var sale_master=`select * from sale_master where id=${id}`;
    var sale_child=`select * from sale_child where sale_master_id=${id}`;
    var destinations=`select * from destinations where COMPANY_ID=${company_id}`;
    var return_data = {};

    async.parallel([
        function(parallel_done) {
            mysqlconnection.query(customers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.customers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_child, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.sale_child = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(categories, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.categories = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(sale_master, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.sale_master = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(products, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.products = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(destinations, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.destinations = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
//     response.send(return_data.sale_master);
        response.render('./trip/edit.twig',{'customers':return_data.customers , 'categories':return_data.categories,'sale_master':return_data.sale_master,sale_id:sale_id, products:return_data.products, destinations:return_data.destinations,'sale_child':return_data.sale_child});
    });

});


router.post('/update',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    mysqlconnection.query(`delete from sale_child where sale_master_id=${request.body.id}`);
    mysqlconnection.query(`delete from product_ledger where transection_id=${request.body.invoice_number}`);
    mysqlconnection.query(`delete from party_ledger where voucher_no=${request.body.invoice_number}`);
    mysqlconnection.query(`delete from cash_ledger where voucher_no=${request.body.invoice_number}`);

    var data={
        invoice_number:request.body.invoice_number,
        invoice_date:request.body.invoice_date,
        payment_type:request.body.payment_type,
        customer_name:request.body.customer_name,
        invoice_time:request.body.time,
        customer_id:request.body.customer_id,
        gross_amount:request.body.gross_amount,
        discount:request.body.discount,
        from_destination:request.body.from_destination,
        to_destination:request.body.to_destination,
        pak_vehicle_number:request.body.pak_vehicle_number,
        net_amount:request.body.net_amount,
        internal_notes:request.body.internal_notes,
        external_notes:request.body.external_notes,
        type:'T',
        driver_name:request.body.driver_name,
        driver_contact:request.body.driver_contact,
        afghan_vehicle_number:request.body.afghan_vehicle_number,
        renter_name:request.body.renter_name,
        company_id:company_id
    }
    mysqlconnection.query(`update sale_master set ? where id=${request.body.id}`,data,(err,rows)=>{

            var sale_master_id=request.body.id;
            var categories=request.body.category_id;
            var product_id=request.body.product_id;
            var quantity=request.body.quantity;
            var price=request.body.price;
            var total=request.body.total;
            var cost_price=request.body.cost_price;
            for(var i=0;i<categories.length;i++){

                var data={
                    sale_master_id:sale_master_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:quantity[i],
                    price:price[i],
                    total:total[i],
                    cost_price:cost_price[i]

                }
                mysqlconnection.query('insert into sale_child set ?',data,(err,rows)=>{

                });
            }



            for(var i=0;i<categories.length;i++){
                var data={
                    transection_id:request.body.invoice_number,
                    transection_date:request.body.invoice_date,
                    company_id:company_id,
                    category_id:categories[i],
                    product_id:product_id[i],
                    quantity:-(quantity[i]),
                    cost_price:cost_price[i],
                    type:'SALE'
                }
                mysqlconnection.query('insert into product_ledger set ?',data,(err,rows)=>{
                    if(err) console.log(err);
                });
            }


            console.log("the master id is " +rows.insertId);
            request.session.sale_id=rows.insertId;


            if(request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment == "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            if (request.body.payment_type == "credit" && request.body.customer_id != "counter_sale" && request.body.payment != "") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'credit sale'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"CRV",
                    party_id:request.body.customer_id,
                    credit:request.body.payment,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'payment received on invoice number'+request.body.invoice_number
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            if (request.body.payment_type == "cash" && request.body.customer_id != "counter_sale") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });

                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    party_id:request.body.customer_id,
                    credit:request.body.net_amount,
                    debit:0,
                    COMPANY_ID:company_id,
                    remarks:'CASH SALE'
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }

            if (request.body.payment_type == "cash") {
                var data={
                    voucher_no:request.body.invoice_number,
                    voucher_date:request.body.invoice_date,
                    type:"SALE",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.net_amount,
                    remarks:'Cash Sale  on invoice number'+request.body.invoice_number + " on party id " + request.body.customer_id
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }


            response.redirect('/trip/list');



    });

});

module.exports=router;