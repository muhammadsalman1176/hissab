/**
 * Created by belawal on 8/29/19.
 */


var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var http = require("http");

var multer  = require('multer');
var upload = multer({ dest: 'public/img/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');


var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};
router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get("/list",(request,res)=>{
    mysqlconnection.query(`select * from types`  ,(err,rows)=>{
        if(!err){
//            res.send('oksdlfjsd')
            res.render('./types/list.twig',{types:rows})
        }
        else{
            response.send(err);
        }
    })

});


router.get('/edit/:id?',(req,res)=>{
    var id=req.params.id;
    mysqlconnection.query(`SELECT * From types where id=${id}`,(err,rows)=>{
        if(!err){
            res.render('./types/edit.twig',{types:rows})
        }
        else{
            console.log('error'+err)
        }
    });
});

router.get('/add',(request,response)=>{
    response.render('./types/add.twig');
});

router.post('/store',(request,response)=>{
    var data={
        NAME:request.body.name,
        AMOUNT:request.body.amount,
        EXPIRY:request.body.expiry,
        PER_MONTH:request.body.per_month,
        DESCRIPTION:request.body.description

    }
    mysqlconnection.query('insert into types set ?' , data,(err,rows)=>{
        if(!err){
            response.redirect('/types/list');
        }
        else{

        }
    });
});


router.post('/update',(request,response)=>{


    var company_id=request.session.COMPANY_ID;
    var id=request.body.id;
    var data={
        NAME:request.body.name,
        AMOUNT:request.body.amount,
        EXPIRY:request.body.expiry,
        PER_MONTH:request.body.per_month,
        DESCRIPTION:request.body.description

    }
    mysqlconnection.query(`UPDATE types set ? where id=${id}`,data,(err,rows)=>{
        if(!err){
            response.redirect('/types/list');
        }
        else{
            response.send(err);
        }
    });
});

router.get('/delete/:id?',(request,response)=>{
    mysqlconnection.query(`delete from types where id=${request.params.id}` ,(err,rows)=>{
        if(!err){
            response.redirect('/types/list');
        }
        else{

        }
    });
});



module.exports=router;