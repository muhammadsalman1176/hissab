/**
 * Created by belawal on 7/4/20.
 */

var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash');
app.use(flash());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))


var cookieParser = require('cookie-parser');

var flash = require('express-flash-messages')
app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};

router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select vouchers.*,parties.name as p_name,parties.TYPE as p_type from vouchers  LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID where vouchers.TYPE='CRV' AND vouchers.COMPANY_ID=${company_id} ORDER BY vouchers.VOUCHER_ID DESC`,(err,rows)=>{
        if(!err){
            var successmessage = request.flash('cash_payment_success');
            console.log('success message: ' + successmessage);
            response.render('./voucher/hawalacashreceive/list.twig',{receives:rows,success : successmessage});
//            response.render('./voucher/cashpayment/list.twig',{payments:rows});
        }
        else{
            response.send(err)
        }
    });
});

router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var parties=`select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V') AND status='Y'`
    var vouchers=`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='CRV' ORDER BY VOUCHER_ID DESC limit 1`;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(vouchers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.vouchers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },  function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        if(return_data.vouchers.length>0){
            var the_string=return_data.vouchers[0].VOUCHER_NO;
            var parts = the_string.split('-', 2);
            var voucher_no  = parseInt(parts[1])+1;
            voucher_no='CRV-'+voucher_no;
        }
        else{
            voucher_no='CRV-'+1;
        }

        response.render('./voucher/hawalacashreceive/add.twig',{voucher_no:voucher_no,parties:return_data.parties,currencies:return_data.currencies});
    });

});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'CRV',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        COMPANY_ID:company_id,
        PARTY_ID:request.body.party_id,
        currency_id:request.body.currency_id,
        exchange_rate:request.body.exchange_rate,
        conversion_currency_id:request.body.conversion_currency_id,
        conversion_exchange_rate:request.body.conversion_exchange_rate,
        conversion_total_amount:request.body.conversion_total_amount,
        TIME:request.body.time
    }
    mysqlconnection.query('insert into vouchers set ?', data,(err,rows)=>{
        if(!err) {

            var remarks;
            if(request.body.description==""){
                mysqlconnection.query(`select * from parties where PARTY_ID=${request.body.party_id}`,(err,rows)=>{
                    var remarks='Cash Received to '+ rows[0].NAME;

                    if(request.body.conversion_exchange_rate==0){
                        var data={
                            voucher_no:request.body.voucher_no,
                            voucher_date:request.body.voucher_date,
                            type:"CASH_RECEIVED",
                            COMPANY_ID:company_id,
                            party_id:request.body.party_id,
                            credit:request.body.amount,
                            currency_id:request.body.currency_id,
                            debit:0,
                            remarks:remarks
                        }
                        mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });

                    }
                    else{
                        var data={
                            voucher_no:request.body.voucher_no,
                            voucher_date:request.body.voucher_date,
                            type:"CASH_RECEIVED",
                            COMPANY_ID:company_id,
                            party_id:request.body.party_id,
                            credit:request.body.conversion_total_amount,
                            currency_id:request.body.conversion_currency_id,
                            debit:0,
                            exchange_rate:request.body.conversion_exchange_rate,
                            remarks:remarks
                        }
                        mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                            if(err) console.log(err)

                        });
                    }


                });
            }
            else{
                var remarks=request.body.description;
                if(request.body.conversion_exchange_rate==0){
                    var data={
                        voucher_no:request.body.voucher_no,
                        voucher_date:request.body.voucher_date,
                        type:"CASH_RECEIVED",
                        COMPANY_ID:company_id,
                        party_id:request.body.party_id,
                        credit:request.body.amount,
                        currency_id:request.body.currency_id,
                        debit:0,
                        remarks:remarks
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });

                }
                else{
                    var data={
                        voucher_no:request.body.voucher_no,
                        voucher_date:request.body.voucher_date,
                        type:"CASH_RECEIVED",
                        COMPANY_ID:company_id,
                        party_id:request.body.party_id,
                        credit:request.body.conversion_total_amount,
                        currency_id:request.body.conversion_currency_id,
                        debit:0,
                        exchange_rate:request.body.conversion_exchange_rate,
                        remarks:remarks
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                }
            }


            var data={
                voucher_no:request.body.voucher_no,
                voucher_date:request.body.voucher_date,
                type:"CASH_RECEIVED",
                credit:0,
                COMPANY_ID:company_id,
                debit:request.body.amount,
                currency_id:request.body.currency_id,
                remarks:'CASH_RECEIVED on VOUCHER number '+request.body.voucher_no
            }
            mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                if(err) console.log(err)

            });

            request.flash('cash_payment_success',rows.insertId);
            response.redirect('/hawala_cashreceive/list');
        }
        else{
            console.log(err)
        }
    });
});


router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var parties=`select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V')`;
    var record=`select * from vouchers where VOUCHER_ID=${request.params.id} And COMPANY_ID=${company_id}`;
    var currencies=`select * from currency where status='Y' AND COMPANY_ID = ${company_id}`;

    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(record, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.record = results;
                parallel_done();
            });
        }, function(parallel_done) {
            mysqlconnection.query(currencies, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.currencies = results;
                parallel_done();
            });
        }
    ], function(err) {
//        response.send(return_data.record);
        response.render('./voucher/hawalacashreceive/edit.twig',{record:return_data.record,parties:return_data.parties,currencies:return_data.currencies});
    });

});


router.post('/update',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'CRV',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        COMPANY_ID:company_id,
        PARTY_ID:request.body.party_id,
        currency_id:request.body.currency_id,
        conversion_currency_id:request.body.conversion_currency_id,
        conversion_exchange_rate:request.body.conversion_exchange_rate,
        conversion_total_amount:request.body.conversion_total_amount,
        exchange_rate:request.body.exchange_rate,
        TIME:request.body.time
    }
    mysqlconnection.query(`update vouchers set ? where VOUCHER_ID=${request.body.id}`,data,(err,rows)=>{
        if(err) console.log(err);

        var remarks=request.body.description;


        if(request.body.conversion_exchange_rate==0){
            var data={
                voucher_no:request.body.voucher_no,
                voucher_date:request.body.voucher_date,
                type:"CASH_RECEIVED",
                COMPANY_ID:company_id,
                party_id:request.body.party_id,
                credit:request.body.amount,
                currency_id:request.body.currency_id,
                debit:0,
                remarks:remarks
            }
            mysqlconnection.query(`update party_ledger set ? where voucher_no='${request.body.voucher_no}' AND COMPANY_ID=${company_id}`,data,(err,rows)=>{
                if(err) console.log(err);
            });
        }
        else{
            var data={
                voucher_no:request.body.voucher_no,
                voucher_date:request.body.voucher_date,
                type:"CASH_RECEIVED",
                COMPANY_ID:company_id,
                party_id:request.body.party_id,
                credit:request.body.conversion_total_amount,
                currency_id:request.body.conversion_currency_id,
                debit:0,
                exchange_rate:request.body.conversion_exchange_rate,
                remarks:remarks
            }
            mysqlconnection.query(`update party_ledger set ? where voucher_no='${request.body.voucher_no}' AND COMPANY_ID=${company_id}`,data,(err,rows)=>{
                if(err) console.log(err);
            });
        }


        var data={
            voucher_no:request.body.voucher_no,
            voucher_date:request.body.voucher_date,
            type:"CASH_RECEIVED",
            credit:0,
            COMPANY_ID:company_id,
            currency_id:request.body.currency_id,
            debit:request.body.amount,
            remarks:'CASH_RECEIVED on VOUCHER number '+request.body.voucher_no
        }
        mysqlconnection.query(`update cash_ledger set ? where voucher_no='${request.body.voucher_no}' AND COMPANY_ID=${company_id}`,data,(err,rows)=>{
        });

        response.redirect('/hawala_cashreceive/list');

    })
});


router.get('/delete/:id?',(req,res)=>{
    var company_id=req.session.COMPANY_ID;
    var record=`select * from vouchers where VOUCHER_ID=${req.params.id}`;
    mysqlconnection.query(record,(err,rows)=>{
        if(rows.length>0){
            var voucher_no=rows[0].VOUCHER_NO;
            var type='EXPENSE';
            mysqlconnection.query(`delete from vouchers where VOUCHER_ID=${req.params.id}`);
            mysqlconnection.query(`delete from cash_ledger where voucher_no='${voucher_no}' and type='CASH_RECEIVED' and COMPANY_ID=${company_id}`,(err,rows)=>{
                if(err) console.log(err);
            });

            mysqlconnection.query(`delete from party_ledger where voucher_no='${voucher_no}' and type='CASH_RECEIVED' and COMPANY_ID=${company_id}`,(err,rows)=>{
                if(err) console.log(err);
            });
            res.redirect('/hawala_cashreceive/list')
        }

    });
});


module.exports=router;