/**
 * Created by belawal on 10/23/20.
 */
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
fs = require('fs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var multer  = require('multer');
var upload = multer({ dest: '/tmp/'});


const fileUpload = require('express-fileupload');

app.use(fileUpload({ safeFileNames: true, preserveExtension: true }))

var flash = require('connect-flash-plus');
app.use(flash());

var cookieParser = require('cookie-parser');

//var flash = require('express-flash-messages')
//app.use(flash())
var router=express.Router();

var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));


var async = require('async');

sessionvalues = function(req, res, next) {
    res.locals.session = req.session;
    next();
};


router.use(sessionvalues);

var mysqlconnection=require('../connection.js');

app.set('views','./views');
app.set('view engine','twig');

router.get('/list',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    mysqlconnection.query(`select vouchers.*,accounts.NAME as b_name ,parties.name as p_name,parties.TYPE as p_type from vouchers  LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID  LEFT JOIN accounts on vouchers.ACCOUNT_ID=accounts.ACCOUNT_ID  where vouchers.TYPE='CRV' AND vouchers.COMPANY_ID=${company_id} ORDER BY vouchers.VOUCHER_ID DESC`,(err,rows)=>{
        if(!err){
            var successmessage = request.flash('cash_payment_success');
            console.log('success message: ' + successmessage);
            response.render('./voucher/realestatecashreceive/list.twig',{receives:rows,success : successmessage});
//            response.render('./voucher/cashpayment/list.twig',{payments:rows});
        }
        else{
            response.send(err)
        }
    });


});

router.get('/add',(request,response)=>{

    var company_id=request.session.COMPANY_ID;
    var parties=`select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V') AND status='Y'`
    var vouchers=`select * from vouchers where COMPANY_ID=${company_id} AND TYPE='CRV' ORDER BY VOUCHER_ID DESC limit 1`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`
    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(vouchers, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.vouchers = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },  function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        }
    ], function(err) {
        if (err) console.log(err);
        if(return_data.vouchers.length>0){
            var the_string=return_data.vouchers[0].VOUCHER_NO;
            var parts = the_string.split('-', 2);
            var voucher_no  = parseInt(parts[1])+1;
            voucher_no='CRV-'+voucher_no;
        }
        else{
            voucher_no='CRV-'+1;
        }

        response.render('./voucher/realestatecashreceive/add.twig',{voucher_no:voucher_no,parties:return_data.parties,banks:return_data.banks});
    });

});

router.post('/store',(request,response)=>{
    var company_id=request.session.COMPANY_ID;
    var data={
        VOUCHER_NO:request.body.voucher_no,
        TYPE:'CRV',
        AMOUNT:request.body.amount,
        VOUCHER_DATE:request.body.voucher_date,
        DESCRIPTION:request.body.description,
        MANUAL_NO:request.body.manual_no,
        COMPANY_ID:company_id,
        PARTY_ID:request.body.party_id,
        ACCOUNT_ID:request.body.account_id,
        cheque_number:request.body.cheque_number,
        registration_number:request.body.registration_number,
        TIME:request.body.time
    }
    mysqlconnection.query('insert into vouchers set ?', data,(err,rows)=>{
        if(!err) {

            var remarks;
            if(request.body.description==""){
                mysqlconnection.query(`select * from parties where PARTY_ID=${request.body.party_id}`,(err,rows)=>{
                    var remarks='Cash Payment to '+ rows[0].NAME;
                    var data={
                        voucher_no:request.body.voucher_no,
                        voucher_date:request.body.voucher_date,
                        type:"CASH_RECEIVED",
                        COMPANY_ID:company_id,
                        party_id:request.body.party_id,
                        credit:request.body.amount,
                        debit:0,
                        remarks:remarks
                    }
                    mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                        if(err) console.log(err)

                    });
                });
            }
            else{
                var remarks=request.body.description;
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    type:"CASH_RECEIVED",
                    COMPANY_ID:company_id,
                    party_id:request.body.party_id,
                    credit:request.body.amount,
                    debit:0,
                    remarks:remarks
                }
                mysqlconnection.query(`insert into party_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            if(request.body.payment_type=='cash'){
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    type:"CASH_RECEIVED",
                    credit:0,
                    COMPANY_ID:company_id,
                    debit:request.body.amount,
                    remarks:'CASH_RECEIVED on VOUCHER number '+request.body.voucher_no
                }
                mysqlconnection.query(`insert into cash_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }
            else{
                var data={
                    voucher_no:request.body.voucher_no,
                    voucher_date:request.body.voucher_date,
                    deposit:request.body.amount,
                    account_id:request.body.account_id,
                    withdraw:0,
                    COMPANY_ID:company_id,
                    remarks:request.body.description
                }

                mysqlconnection.query(`insert into bank_ledger set ?`,data,(err,rows)=>{
                    if(err) console.log(err)

                });
            }



            request.flash('cash_payment_success',rows.insertId);
            response.redirect('/realestate_cashreceive/list');
        }
        else{
            console.log(err)
        }
    });
});


router.get('/print/:id?',(request,response)=>{
    mysqlconnection.query(`select vouchers.*,parties.NAME AS party_name,parties.father_name,accounts.Name as b_name,currency.currency_name,booking.booking_id,booking.plot_id,plot_master.name as plot_number,plot_master.street_no,plot_master.block,plot_master.length_width from vouchers LEFT JOIN parties on parties.PARTY_ID=vouchers.PARTY_ID LEFT JOIN accounts on accounts.ACCOUNT_ID=vouchers.ACCOUNT_ID LEFT JOIN currency on currency.currencyid=vouchers.currency_id LEFT JOIN booking on booking.booking_number=vouchers.registration_number LEFT JOIN plot_master on plot_master.plot_id=booking.plot_id where vouchers.VOUCHER_ID=${request.params.id}`,(err,rows)=>{
        if(err) console.log(err)
        console.log(rows);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }
//       response.send(rows);
        response.render('./voucher/realestatecashreceive/print.twig',{vouchers:rows,session:session_values})
    })
})

router.get('/edit/:id?',(request,response)=>{
    var company_id=request.session.COMPANY_ID;

    var parties=`select * from parties where COMPANY_ID=${company_id} AND (type='C' OR type='V')`;
    var record=`select * from vouchers where VOUCHER_ID=${request.params.id} And COMPANY_ID=${company_id}`;
    var banks=`select * from accounts where COMPANY_ID=${company_id} AND HEAD_ID=2`

    var return_data = {};

    async.parallel([

        function(parallel_done) {
            mysqlconnection.query(parties, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.parties = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(banks, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.banks = results;
                parallel_done();
            });
        },
        function(parallel_done) {
            mysqlconnection.query(record, {}, function(err, results) {
                if (err) return parallel_done(err);
                return_data.record = results;
                parallel_done();
            });
        }
    ], function(err) {
//        response.send(return_data.record);
        response.render('./voucher/realestatecashreceive/edit.twig',{record:return_data.record,parties:return_data.parties,banks:return_data.banks});
    });

});


router.get('/get_party_id/:reg_number?',(request,response)=>{
    mysqlconnection.query(`select booking.*,parties.NAME as party_name from booking Left join parties on parties.PARTY_ID=booking.customer_id where booking.booking_number='${request.params.reg_number}'`,(err,rows)=>{
        if(err) console.log(err)
        console.log(rows);
        var session_values={
            name:request.session.SHORT_NAME,
            email:request.session.EMAIL,
            contact:request.session.CONTACT,
            logo:request.session.LOGO_LARGE
        }

        response.json(rows);
//        response.render('./voucher/realestatecashreceive/print.twig',{vouchers:rows,session:session_values})
    })
});


module.exports=router;
