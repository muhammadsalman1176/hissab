/**
 * Created by belawal on 1/15/19.
 */
const express=require('express');
const app=express();
const path=require('path');


var cookieSession = require('cookie-session');
app.use(cookieSession({
    name: 'session',
    keys: ['key1', 'key2']
}));

const mysql=require('mysql');
var mysqlconnection = mysql.createConnection({
      host:'localhost',
       user:'root',
    password:'',
    database:'ice'
});

app.locals.site = 'Exampdfsdle';



app.use(function (req, res, next) {
    var session_values={
        name:req.session.SHORT_NAME,
        email:req.session.EMAIL,
        contact:req.session.CONTACT,
        logo:req.session.LOGO_LARGE
    }
    var company_id=req.session.COMPANY_ID;

    if(company_id=='' || company_id==undefined){
        next();
    }
    else{
        mysqlconnection.query(`select * from assigned_menus where COMPANY_ID=${company_id}`,(err,rows)=>{
            if(err) console.log(err)
            res.locals = {
                siteTitle:req.session.COMPANY_ID ,
                role: req.session.role,
                online_email: req.session.EMAIL,
                description: "My app's description",
                all_menus:rows
            };
            next();
        });
    }

});

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var flash = require('connect-flash-plus');
app.use(flash());

app.use('/static',express.static(__dirname+'/public'));



const login=require('./controllers/logincontroller')
app.use('/',login);

const company=require('./controllers/companycontroller')
app.use('/company',company);


const types=require('./controllers/typescontroller')
app.use('/types',types);

const category=require('./controllers/categorycontroller')
app.use('/category',category);

const product=require('./controllers/productcontroller')
app.use('/product',product);

const employees=require('./controllers/employeescontroller')
app.use('/employees',employees);

const customers=require('./controllers/customercontroller')
app.use('/customers',customers);

const vendors=require('./controllers/vendorcontroller')
app.use('/vendors',vendors);

const bookers=require('./controllers/bookercontroller')
app.use('/bookers',bookers);


const currencies=require('./controllers/currencycontroller')
app.use('/currencies',currencies);


const fakhar_customers=require('./controllers/fakhar_customerscontroller')
app.use('/fakhar_customers',fakhar_customers);

const realestate_customers=require('./controllers/realestate_customerscontroller')
app.use('/realestate_customers',realestate_customers);


const acount_head=require('./controllers/account_headcontroller')
app.use('/account_head',acount_head);

//sale invoice//
const sale_invoice=require('./controllers/sale_invoicecontroller')
app.use('/sale_invoice',sale_invoice);


const sale_return=require('./controllers/sale_returncontroller')
app.use('/sale_return',sale_return);

const yy_sale_invoice=require('./controllers/yy_sale_invoicecontroller')
app.use('/yy_sale_invoice',yy_sale_invoice);


const hawala=require('./controllers/hawalacontroller')
app.use('/hawala',hawala);

const hawala_receive=require('./controllers/hawala_receivecontroller')
app.use('/hawala_receive',hawala_receive);

const hawala_sale=require('./controllers/hawala_salecontroller')
app.use('/hawala_sale',hawala_sale);


const hawala_purchase=require('./controllers/hawala_purchasecontroller')
app.use('/hawala_purchase',hawala_purchase);


const union_sale_invoice=require('./controllers/union_sale_invoicecontroller')
app.use('/union_sale_invoice',union_sale_invoice);
//end of sale invoice//

//purchase invoice//
const purchase_invoice=require('./controllers/purchase_invoicecontroller')
app.use('/purchase_invoice',purchase_invoice);


const floor = require('./controllers/floorController')
app.use('/floor',floor);

const issues = require('./controllers/issuescontroller')
app.use('/issues',issues);

const receives = require('./controllers/receivescontroller')
app.use('/receives',receives);

const yy_purchase_invoice=require('./controllers/yy_purchase_invoicecontroller')
app.use('/yy_purchase_invoice',yy_purchase_invoice);

const purchase_return=require('./controllers/purchase_returncontroller')
app.use('/purchase_return',purchase_return);


const boring_purchase_invoice=require('./controllers/boring_purchase_invoicecontroller')
app.use('/boring_purchase_invoice',boring_purchase_invoice);

const union_purchase_invoice=require('./controllers/union_purchase_invoicecontroller')
app.use('/union_purchase_invoice',union_purchase_invoice);
//end of purchase invoice//


// remainder //
const remainder=require('./controllers/remaindercontroller')
app.use('/remainder',remainder);
//end of remainder//

//for demages//
const damages=require('./controllers/damagescontroller')
app.use('/damages',damages);
//end of damages//

//for vouchers//
const voucher_expense=require('./controllers/voucher_expensecontroller')
app.use('/voucher_expense',voucher_expense);

const hawala_voucher_expense=require('./controllers/hawala_voucher_expensecontroller')
app.use('/hawala_voucher_expense',hawala_voucher_expense);

const voucher_income=require('./controllers/voucher_incomecontroller')
app.use('/voucher_income',voucher_income);

const voucher_cashpayment=require('./controllers/voucher_cashpaymentcontroller')
app.use('/voucher_cashpayment',voucher_cashpayment);

const voucher_cashreceive=require('./controllers/voucher_cashreceivecontroller')
app.use('/voucher_cashreceive',voucher_cashreceive);

const hawala_cashreceive=require('./controllers/voucher_hawalacashreceivecontroller')
app.use('/hawala_cashreceive',hawala_cashreceive);

const realestate_cashreceive=require('./controllers/voucher_realestatecashreceivecontroller')
app.use('/realestate_cashreceive',realestate_cashreceive);


const hawala_cashpayment=require('./controllers/voucher_hawalacashpaymentcontroller')
app.use('/hawala_cashpayment',hawala_cashpayment);

const yy_voucher_bankdeposit=require('./controllers/yy_voucher_bankdepositcontroller')
app.use('/yy_voucher_bankdeposit',yy_voucher_bankdeposit);


const voucher_bankdeposit=require('./controllers/voucher_bankdepositcontroller')
app.use('/voucher_bankdeposit',voucher_bankdeposit);

const voucher_bankwithdraw=require('./controllers/voucher_bankwithdrawcontroller')
app.use('/voucher_bankwithdraw',voucher_bankwithdraw);

//end of vouchers//

//for reports//
const reports=require('./controllers/reportscontroller')
app.use('/reports',reports);

const hawala_reports=require('./controllers/hawala_reportscontroller')
app.use('/hawala_reports',hawala_reports);



const hawala_cash_closing=require('./controllers/hawala_cash_closingcontroller')
app.use('/hawala_cash_closing',hawala_cash_closing);


const real_estate_report=require('./controllers/realestate_reportscontroller')
app.use('/real_estate_report',real_estate_report);
//end of reports//

//for users//
const users=require('./controllers/usercontroller')
app.use('/users',users);
//end of users//


//for trash//
const trash=require('./controllers/trashcontroller')
app.use('/trash',trash);
//end of users//


const project=require('./controllers/projectcontroller')
app.use('/projects',project);

const plotsizes=require('./controllers/plotsizecontroller')
app.use('/plotsizes',plotsizes);

const plotfeatures=require('./controllers/plotfeaturecontroller')
app.use('/plotfeatures',plotfeatures);

const plotmaster=require('./controllers/plotmastercontroller')
app.use('/plotmaster',plotmaster);

const buildermaster=require('./controllers/buildermastercontroller')
app.use('/buildermaster',buildermaster);


const payment_modes=require('./controllers/paymentmodecontroller')
app.use('/payment_modes',payment_modes);

const payment_schedule=require('./controllers/paymentschedulecontroller')
app.use('/payment_schedule',payment_schedule);

const booking=require('./controllers/bookingcontroller')
app.use('/booking',booking);


const shop_booking=require('./controllers/shop_bookingcontroller')
app.use('/shop_booking',shop_booking);
//for destinations//
const destination=require('./controllers/destinationcontroller')
app.use('/destination',destination);
//end of destinations//


//sale trip//
const trip=require('./controllers/tripcontroller')
app.use('/trip',trip);
//end of trip

//purchase dollars//
const purchase_dollar=require('./controllers/purchase_dollarcontroller')
app.use('/purchase_dollars',purchase_dollar);
//end of purchase dollars


//purchase Form I //
const purchase_form_i=require('./controllers/purchase_form_icontroller')
app.use('/purchase_form_i',purchase_form_i);
//end of Form I//


//purchase Form I //
const sale_form_i=require('./controllers/sale_form_icontroller')
app.use('/sale_form_i',sale_form_i);
//end of Form I//



//sale purchase dollars//
const sale_dollar=require('./controllers/sale_dollarcontroller')
app.use('/sale_dollars',sale_dollar);
//end of purchase dollars

const city=require('./controllers/CityController')
app.use('/city',city);

//demand note//
const demand_note=require('./controllers/demand_notecontroller')
app.use('/demand_note',demand_note);
//end of demand note//


const test=require('./controllers/testcontroller')
app.use('/test',test);

// app.listen(3000,()=>{console.log('server running on 3000')})
app.listen(3003,()=>{console.log('server running on 3003')})

